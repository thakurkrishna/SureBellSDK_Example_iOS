//
//  Device.swift
//  sureBellFramework-iOS
//
//  Created by PiOctave on 16/04/18.
//

//    Copyright (C) 2016-2018 PiOctave Solutions (Pvt) Ltd. (http://www.pioctave.com/)

//    All information contained herein is, and remains
//    the property of PiOctave Solutions (Pvt) Ltd and its suppliers
//    if any.  The intellectual and technical concepts contained
//    herein are proprietary to PiOctave Solutions (Pvt) Ltd
//    and are protected by trade secret or copyright law.
//    Dissemination of this information or reproduction of this material
//    is strictly forbidden unless prior written permission is obtained
//    from PiOctave Solutions (Pvt) Ltd.

//    THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
//    ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
//    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
//    ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
//    FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
//    DAMAGES. HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
//    STRICT LIABILITY, OR TORT ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
//    EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


import Foundation
import ObjectMapper
import Alamofire

/// This is a Singleton class.
/// Where this Singleton pattern makes sure that only one instance of a class is instantiated with in the application.
/// Only one instance of this object can be shared and used by other object globally.
///
/// Simple example for Singleton pattern
///
/// **Listing 1**
///
///     class Singleton {
///         static let sharedInstance: Singleton = {
///             let instance = Singleton()
///
///             //setup code
///
///             return instance
///         }()
///     }
///
public class Device:Mappable {
    public var _id : String?
    public var updatedAt : String?
    public var createdAt : String?
    public var name : String?
    public var mac : String?
    public var firmwareVersion : String?
    public var model : String?
    public var adminToken : Int?
    public var vendor : String?
    public var createdBy : String?
    public var __v : Int?
    public var deviceUser : String?
    public var lastImage : String?
    public var settings : Settings?
    public var upgradeAttempts : Int?
    public var versionOnDevice : Int?
    public var upgradeStatus : String?
    public var location : [Double]?
    public var invites : [String]?
    public var users : [Users]?
    public var admins : [Admins]?
    public var tag : String?
    public var isLocalAvailable : Bool?
    public var isRemoteAvailable : Bool?
    public var statusUpdatedAt : String?
    
    public static var instance = Device()
    
    required convenience public init?(map: Map) {
        self.init()
    }
    
    public func mapping(map: Map) {
        self._id <- map["_id"]
        self.updatedAt <- map["updatedAt"]
        self.createdAt <- map["createdAt"]
        self.name <- map["name"]
        self.mac <- map["mac"]
        self.firmwareVersion <- map["firmwareVersion"]
        self.model <- map["model"]
        self.adminToken <- map["adminToken"]
        self.vendor <- map["vendor"]
        self.createdBy <- map["createdBy"]
        self.__v <- map["__v"]
        self.deviceUser <- map["deviceUser"]
        self.lastImage <- map["lastImage"]
        self.settings <- map["settings"]
        self.upgradeAttempts <- map["upgradeAttempts"]
        self.versionOnDevice <- map["versionOnDevice"]
        self.upgradeStatus <- map["upgradeStatus"]
        self.location <- map["location"]
        self.invites <- map["invites"]
        self.users <- map["users"]
        self.admins <- map["admins"]
        self.tag <- map["tag"]
        self.isLocalAvailable <- map["isLocalAvailable"]
        self.isRemoteAvailable <- map["isRemoteAvailable"]
        self.statusUpdatedAt <- map["statusUpdatedAt"]
    }
    
    
    /// Calls back getDevice() function to check whether the specififed device is present on server or not with the registered user.
    ///
    /// **Listing 1**
    ///
    ///     let id:5a61fc09f300270010f9bcd1 (device id as a *param*)
    ///
    /// When call function becomes successful server gives response as like below.
    ///
    /// **Listing 2**
    ///
    ///     {
    ///     "_id":"5a61fc09f300270010f9bcd1",
    ///     "updatedAt":"2018-04-20T10:10:11.185Z",
    ///     "createdAt":"2018-01-19T14:09:13.500Z",
    ///     "name":"D364",
    ///     "mac":"1C:49:7B:9E:D3:64",
    ///     "firmwareVersion":"2018.04.19",
    ///     "model":"SureBell-Rev-2",
    ///     "adminToken":"30956",
    ///     "vendor":"577264f020006ee2e0c4b043",
    ///     "createdBy":"5a61e9c79af05a0010c62884",
    ///     "__v":866,
    ///     "deviceUser":"5a61fc09f300270010f9bcd2",
    ///     "lastImage":"https:storage.googleapis.com/pioctave/     5a61fc09f300270010f9bcd1%2FlastImage-1517208643113?GoogleAccessId=cloudstorage@masterserver-1332.iam.gserviceaccount.com&Expires=",
    ///     "pairedWith":[
    ///     ],
    ///     "settings":{
    ///     "chime":true,
    ///     "motionSensitivity":1,
    ///     "cloudUpload":false,
    ///     "timeZone":"+5:30",
    ///     "cloudUploadSetting":{
    ///     "ring":{
    ///     "settings":[
    ///     ],
    ///     "status":true
    ///     },
    ///     "motion":{
    ///     "settings":[
    ///     ],
    ///     "status":true
    ///     }
    ///     },
    ///     "video":{
    ///     "recording":{
    ///     "quality":1
    ///     },
    ///     "live":{
    ///     "quality":1
    ///     }
    ///     },
    ///     "audio":{
    ///     "speaker_vol":2,
    ///     "mic_gain":2
    ///     },
    ///     "motionSettings":[
    ///     ]
    ///     },
    ///     "upgradeAttempts":0,
    ///     "versionOnDevice":0,
    ///     "upgradeStatus":"UPGRADED",
    ///     "location":[
    ///     12.9741833,
    ///     77.65403609999997
    ///     ],
    ///     "invites":[
    ///     "+919586679526"
    ///     ],
    ///
    ///     "users":[
    ///     {
    ///     "_id":"5a61b91c9af05a0010c62225",
    ///     "firstname":"Smitha",
    ///     "lastname":"NS",
    ///     "email":"smitha@pioctave.com",
    ///     "phone":"+919481382762"
    ///     }
    ///     ],    "admins":[
    ///     {
    ///     "_id":"5a60b9dbe4329f0010afaa28",
    ///     "email":"niteesh@pioctave.com",
    ///     "firstname":"Niteesh",
    ///     "lastname":"Kumar",
    ///     "phone":"+919916644441"
    ///     }],
    ///     "tag":"ALPHA"
    ///     }
    ///
    /// When call function fails server gives the error as like below.
    ///
    /// **Listing 3**
    ///
    ///        {
    ///        "code": 400,
    ///        "error": "invalid_params",
    ///        "error_description": "missing necessary params{\"query\":[\"vendor\"],\"body\":[],\"params\":[]}"
    ///        }
    ///
    /// - Parameters:
    ///   - id: 5a61fc09f300270010f9bcd1 (Device id from server)
    ///   - callback:@escaping (Calls back getDevice() function)
    ///   - newDevice:Device (Device found on server)
    ///   - failure:@escaping (If the device is not found on server or if the user didn't get properly fetched data from serever then call back fails)
    ///   - error:Any (Device not found on server)
    ///
    public func getDevice(id:String, callback:@escaping (_ newDevice:Device)->Void, failure:@escaping (_ error:Any)->Void) -> Void {
        DeviceHttps.instance.getDevice(id: id, callback: callback, failure: failure)
    }
    
    /// It calls inviteUser() function, then it checks whether the searched user is registered and is available on server or not. If present then it sends invitation to the specified user or the called function fails.
    ///
    /// **Listing 1**
    ///
    ///     let id:5a61fc09f300270010f9bcd1 (Device id) user:abc@gmail.com (Reg. user mail ID)
    ///
    /// When call function becomes successful server gives response as like below.
    ///
    /// **Listing 2**
    ///
    ///     {
    ///     "_id":"5a61fc09f300270010f9bcd1",
    ///     "updatedAt":"2018-04-23T12:03:46.574Z",
    ///     "createdAt":"2018-01-19T14:09:13.500Z",
    ///     "name":"D364",
    ///     "mac":"1C:49:7B:9E:D3:64",
    ///     "firmwareVersion":"2018.04.20",
    ///     "model":"SureBell-Rev-2",
    ///     "adminToken":"11616",
    ///     "vendor":"577264f020006ee2e0c4b043",
    ///     "createdBy":"5a61e9c79af05a0010c62884",
    ///     "__v":874,
    ///     "deviceUser":"5a61fc09f300270010f9bcd2",
    ///     "lastImage":"https://storage.googleapis.com/pioctave/5a61fc09f300270010f9bcd1%2FlastImage-1517208643113?GoogleAccessId=cloudstorage@masterserver-1332.iam.gserviceaccount.com&Expires=1524488627&Signature=P4k5M2HjT18c21ZbrTDLSVL96qihXzlkXxy3%2BrMmXQszznWt38I8LvewyTVdqFTIEsMQWi%",
    ///     "pairedWith":[
    ///
    ///     ],
    ///     "settings":{
    ///     "chime":true,
    ///     "motionSensitivity":1,
    ///     "cloudUpload":false,
    ///     "timeZone":"+5:30",
    ///     "cloudUploadSetting":{
    ///     "ring":{
    ///     "settings":[
    ///
    ///     ],
    ///     "status":true
    ///     },
    ///     "motion":{
    ///     "settings":[
    ///
    ///     ],
    ///     "status":true
    ///     }
    ///     },
    ///     "video":{
    ///     "recording":{
    ///     "quality":1
    ///     },
    ///     "live":{
    ///     "quality":1
    ///     }
    ///     },
    ///     "audio":{
    ///     "speaker_vol":2,
    ///     "mic_gain":2
    ///     },
    ///     "motionSettings":[
    ///
    ///     ]
    ///     },
    ///     "upgradeAttempts":0,
    ///     "versionOnDevice":0,
    ///     "upgradeStatus":"UPGRADED",
    ///     "location":[
    ///     12.9741833,
    ///     77.65403609999997
    ///     ],
    ///     "invites":[
    ///     "+919586679526",
    ///     "5a61fc09f300270010f9bcd1",
    ///     "krishna@pioctave.com",
    ///     "sample@gmail.com"
    ///     ],
    ///     ],
    ///     "tag":"ALPHA"
    ///     }
    ///
    /// When call function fails server gives the error as like below.
    ///
    /// **Listing 3**
    ///
    ///        {
    ///        "code": 400,
    ///        "error": "invalid_params",
    ///        "error_description": "missing necessary params{\"query\":[\"vendor\"],\"body\":[],\"params\":[]}"
    ///        }
    ///
    /// - Parameters:
    ///   - id: 5a61fc09f300270010f9bcd1 (Device id of inviting user registered on server)
    ///   - user: Registered user on sure server to whom we want to send invitation.
    ///   - callback:@escaping (it calls back inviteUser() function until it finds the user on sure server to whom we want to send invitation)
    ///   - newDevice:Device (Device found on server in the name of Reg. user to whom we need to send invitation)
    ///   - failure: If the specified user is not available then it gives user not found.
    ///   - error:Any (User ID and Device ID not found on server)
    ///
    public func inviteUser(id:String,user:String, callback:@escaping (_ newDevice:Device)->Void, failure:@escaping (_ error:Any)->Void) -> Void {
        DeviceHttps.instance.inviteUser(id: id, user: user, callback: callback, failure: failure)
    }
    
    
    /// This getEvents() function helps to get list of events of a device from the server.
    ///
    /// **Listing 1**
    ///
    ///     Let id:5a61fc09f300270010f9bcd1 (Device ID from server)
    ///
    /// When called function becomes successful it gives list of events of a device and the json response for that function will be as like below.
    ///
    /// **Listing 2**
    ///
    ///     [
    ///     {
    ///     "_id":"5ae2ac4c60178d001a527bb6",
    ///     "name":"motion",
    ///     "type":"motion",
    ///     "createdAt":"2018-04-27T04:49:20.000Z",
    ///     "createdBy":{
    ///     "_id":"585326897542157909654321",
    ///     "name":"test",
    ///     "lastImage":"1517208643113"
    ///     },     "image":"https://storage.googleapis.com/pioctave/585326897542157909654321%2F5ae2ac4c60178d001a527bb6-image%2F1524804560.jpg?GoogleAccessId=cloudstorage@masterserver-1332.iam.gserviceaccount.com&Expires=1524816843&Signature=CuWbO3uUq1niHhNxd6Aqf1XniKaaS24oOpwSvsdCr1Bca36QytUCyEGQPo5j2B4ojGvUJYZ4c3T6knmG%",
    ///     "allowedUsers":[
    ///     "5682hbn2873hjvvo98879372",
    ///     "543209876543210987654321"
    ///     ],
    ///     "comments":[
    ///     ],
    ///     "like":[
    ///     ],
    ///     "status":"missed",
    ///     "videoSize":[
    ///     ],   "video":"https://storage.googleapis.com/pioctave/585326897542157909654321%2F5ae2ac4c60178d001a527bb6-video%2F1524804684565.mp4?GoogleAccessId=cloudstorage@masterserver-1332.iam.gserviceaccount.com&Expires=1524816843&Signature=EtK284QRlMv7Mpl48h0inmgMWwQ6jxjQHk%",
    ///     "__v":0,
    ///     "imageSize":7803,      "urlSummary":"https://storage.googleapis.com/summary-service/585326897542157909654321%2F5ae2ac4c60178d001a527bb6-video%2F1524804684565.mp4?GoogleAccessId=cloudstorage@masterserver-1332.iam.gserviceaccount.com&Expires=1524816843&Signature=Dj9Z5DWQQ9omi9ghsrH23VFT73st%2"
    ///     }
    ///     ]
    ///
    /// When call function fails, it won't push events to the user and it gives error as like below with error code.
    ///
    /// **Listing 3**
    ///
    ///     {
    ///        "code": 400,
    ///        "error": "invalid_params",
    ///        "error_description": "missing necessary params{\"query\":[\"vendor\"],\"body\":[],\"params\":[]}"
    ///     }
    ///
    /// - Parameters:
    ///   - id:String (Device id from server as a String)
    ///   - callback:@escaping (It calls back getEvents() function, To make user to get events from the particular device)
    ///   - newDevice:Event (Events will be pushed to the specified device id)
    ///   - failure: It fails to push events to the specified device as per mentioned id.
    ///   - error:Any (Device fails to get list of events, either APIError or general error)
    ///
    public func getEvents(id:String, callback:@escaping (_ newDevice:[Event])->Void, failure:@escaping (_ error:Any)->Void) -> Void {
        DeviceHttps.instance.getEvents(id: id, callback: callback, failure: failure)
    }
    
    
   /// This getDevices() function gives the list of devices which are successfully registered on sure server and the json response will be as like below.
   ///
   /// **Listing 1**
   ///
   ///      [
   ///      {
   ///      "_id":"5a0d5545c2e850001ae2c710",
   ///      "updatedAt":"2018-04-23T05:13:16.157Z",
   ///      "createdAt":"2018-01-19T00:43:29.232Z",
   ///      "vendor":"5682hbn2873hjvvo98879373",
   ///      "createdBy":"543209876543210987654321",
   ///      "mac":"1C:49:7B:C3:B2:F4",
   ///      "name":"hsr pasolite",
   ///      "firmwareVersion":"2.0.3",
   ///      "adminToken":"12587",
   ///      "model":"SureBell-Rev-3",
   ///      "__v":7,
   ///      "deviceUser":"5a613f319af05a0010c608a0",      "lastImage":"https://storage.googleapis.com/pioctave/5a0d5545c2e850001ae2c710%2FlastImage-1520415450621?GoogleAccessId=cloudstorage@masterserver-1332.iam.gserviceaccount.com&Expires=1524493845&Signature=BdEjQcNsTc9F2Pz5W7poCOHUDbaVnHdxhZT%2BY%",
   ///      "pairedWith":[
   ///      ],
   ///      "settings":{
   ///      "chime":true,
   ///      "motionSensitivity":2,
   ///      "cloudUploadSetting":{
   ///      "ring":{
   ///      "settings":[
   ///      ],
   ///      "status":true
   ///      },
   ///      "motion":{
   ///      "settings":[
   ///      ],
   ///      "status":true
   ///      }
   ///      },
   ///      "motionSettings":[
   ///      {
   ///      "resolution":{
   ///      "y":720,
   ///      "x":1280
   ///      },
   ///      "bounds":[
   ///      {
   ///      "y":292.73233,
   ///      "x":668.7335,
   ///      "_id":"5a9fb2efa484cc001ad1bb66"
   ///      },
   ///      {
   ///      "y":714.0289,
   ///      "x":930.66345,
   ///      "_id":"5a9fb2efa484cc001ad1bb65"
   ///      }
   ///      ],
   ///      "anchors":[
   ///      {
   ///      "type":0,
   ///      "y":322.47626,
   ///      "x":668.7335,
   ///      "_id":"5a9fb2efa484cc001ad1bb64"
   ///      },
   ///      {
   ///      "type":0,
   ///      "y":292.73233,
   ///      "x":923.66895,
   ///      "_id":"5a9fb2efa484cc001ad1bb63"
   ///      },
   ///      {
   ///      "type":0,
   ///      "y":706.7129,
   ///      "x":930.66345,
   ///      "_id":"5a9fb2efa484cc001ad1bb62"
   ///      },
   ///      {
   ///      "type":0,
   ///      "y":714.0289,
   ///      "x":671.5534,
   ///      "_id":"5a9fb2efa484cc001ad1bb61"
   ///      }
   ///      ]
   ///      }
   ///      ]
   ///      },
   ///      "upgradeAttempts":0,
   ///      "versionOnDevice":0,
   ///      "upgradeStatus":"UPGRADED",
   ///      "location":[
   ///      ],
   ///      "invites":[
   ///      ],
   ///      "users":[
   ///      {
   ///      "_id":"543209876543210987654321",
   ///      "email":"sample@gmail.com",
   ///      "firstname":"sample",
   ///      "lastname":"test",
   ///      "phone":"+919876598765"
   ///      }
   ///      ],
   ///      "admins":[
   ///      {
   ///      "_id":"543209876543210987654321",
   ///      "email":"sample@gmail.com",
   ///      "firstname":"sample",
   ///      "lastname":"test",
   ///      "phone":"+919876598765"
   ///      }
   ///      ],
   ///      "tag":"ALPHA"
   ///      }
   ///      ]
   ///
   /// When called function fails it gives either APIError or general error with the error description code and the devices will not be listed.
   ///
   /// **Listing 2**
   ///
   ///      {
   ///        "code": 400,
   ///        "error": "invalid_params",
   ///        "error_description": "missing necessary params{\"query\":[\"vendor\"],\"body\":[],\"params\":[]}"
   ///     }
   ///
   /// - Parameters:
   ///   - callback:@escaping (It calls back getDevices() function, to get list of registered devices)
   ///   - newDevice:[Device] (Here [Device] it means as per funtion is list of devices)
   ///   - failure: It fails to list down the devices which are registered successfully on server.
   ///   - error:Any (List of registered device won't come, either APIError or general error)
   ///
   public func getDevices(callback:@escaping (_ newDevice:[Device])->Void, failure:@escaping (_ error:Any)->Void) -> Void {
        DeviceHttps.instance.getDevices(callback: callback, failure: failure)
    }
    
    
    /// It calls getEventsByDate() function, to list down the events on users specfied dates.
    ///
    /// **Listing 1**
    ///
    ///     Let id:"5a9fb2efa484cc001ad1bb61", from: Date(), to:tomorrow!, offset: 0 (Parameters will be passed like this to the function)
    ///
    /// when call function become successful it lists events as per the dates mentioned and the json response will be as like below.
    ///
    /// **Listing 2**
    ///
    ///     [
    ///     {
    ///     "_id":"5ae0564e26ea29001a0a7e18",
    ///     "name":"motion",
    ///     "type":"motion",
    ///     "createdAt":"2018-04-25T10:19:57.000Z",
    ///     "createdBy":{
    ///     "_id":"585326897542157909654321",
    ///     "name":"test",
    ///     "lastImage":"1517208643113"
    ///     },      "image":"https://storage.googleapis.com/pioctave/585326897542157909654321%2F5ae0564e26ea29001a0a7e18-image%2F1524651597.jpg?GoogleAccessId=cloudstorage@masterserver-1332.iam.gserviceaccount.com&Expires=1524659421&Signature=FVwGII03sJ55O%2FNIOcTMm4CH7zf1edMfozG2h5CA1jh6P5nojydGF%",
    ///     "allowedUsers":[
    ///     "5682hbn2873hjvvo98879372"
    ///     ],
    ///     "comments":[
    ///     ],
    ///     "like":[
    ///     ],
    ///     "status":"missed",
    ///     "videoSize":[
    ///     ],     "video":"https://storage.googleapis.com/pioctave/585326897542157909654321%2F5ae0564e26ea29001a0a7e18-video%2F1524651598620.mp4?GoogleAccessId=cloudstorage@masterserver-1332.iam.gserviceaccount.com&Expires=1524659421&Signature=iM8v4gLsTzQdTMFPyjGJEE3DJMeadETe0JbQK78mQRYtlpzgi2%",
    ///     "__v":0,
    ///     "imageSize":8027,     "urlSummary":"https://storage.googleapis.com/summary-service/585326897542157909654321%2F5ae0564e26ea29001a0a7e18-video%2F1524651598620.mp4?GoogleAccessId=cloudstorage@masterserver-1332.iam.gserviceaccount.com&Expires=1524659421&Signature=o%2F3gcFTGHgIvn93j33vVRxTFtCyThoee3qLtnJ3mp679DYQdHR7kzFCR05eAOXRzENHztXLQ"
    ///     }
    ///     ]
    ///
    /// When called function returns error, it fails ti list down events as per the dates specified.
    ///
    /// **Listing 3**
    ///
    ///     {
    ///        "code": 400,
    ///        "error": "invalid_params",
    ///        "error_description": "missing necessary params{\"query\":[\"vendor\"],\"body\":[],\"params\":[]}"
    ///     }
    ///
    /// - Parameters:
    ///   - id:String (Device id as String from server)
    ///   - from:String (User specifying from date as a parameter)
    ///   - to:String (User specifying to date as a parameter)
    ///   - offset:0 (By default offset value will be "0" which helps to lists down finite number of events)
    ///   - callback:@escaping (Calls back getEventsByDate function to get events as per the specified period of time)
    ///   - newDevice:[Event] (Here [Event] means list of events as per the specified period of time)
    ///   - failure:@escaping (Failure results in empty event list on that specified period of time)
    ///   - error:Any (Here error can be APIError or general error and returns error with error decription code)
    ///
    public func getEventsByDate(_ id:String, from:Date, to:Date, offset:Int,callback:@escaping (_ newDevice:[Event])->Void, failure:@escaping (_ error:Any)->Void) -> Void {
        DeviceHttps.instance.getEventsByDate(id, from: from, to: to, offset: offset, callback: callback, failure: failure)
    }
    
    
    /// It calls getDataUseByDevice() function to show the amount of data taken by device to upload events to the cloud.
    ///
    /// **Listing 1**
    ///
    ///     Let id: "5ae0564e26ea29001a0a7e18", "2018-04-01", "2018-05-01", 0 (Parameters to be passed to the function)
    ///
    /// When called function become successful, it displays the amount of data used by device and the json response will be as like below.
    ///
    /// **Listing 2**
    ///
    ///     {
    ///     "_id":"585326897542157909654321",
    ///     "totalImageSize":15330459,
    ///     "totalVideoSize":12714784413
    ///     }
    ///
    /// When called function fails, it returns error with the error description code and the response will be as like below.
    ///
    /// **Listing 3**
    ///
    ///     {
    ///        "code": 400,
    ///        "error": "invalid_params",
    ///        "error_description": "missing necessary params{\"query\":[\"vendor\"],\"body\":[],\"params\":[]}"
    ///     }
    ///
    /// - Parameters:
    ///   - id:String (Device id as string from server)
    ///   - from:String (From date as a parameter)
    ///   - to:String (To date as a parameter)
    ///   - callback:@escaping (calls back getDataUseByDevice function to get the amount of data used by device)
    ///   - dataUsage:DataUsage (Gives the amount of data used by device)
    ///   - failure:Fails to calculate the amount of data used by device.
    ///   - error:Any (Returns error which is either APIError or general error)
    ///
    public func getDataUseByDevice(id:String, from:String, to:String, callback:@escaping (_ dataUsage:DataUsage)->Void, failure:@escaping (_ error:Any)->Void) -> Void {
        DeviceHttps.instance.getDataUseByDevice(id: id, from:from, to: to, callback: callback, failure: failure)
    }
    
    
    /// It calls removeUser() funtion to remove mentioned user id from the specific device id. If an admi wants to remove any of the user from thier device they can use this function.
    ///
    /// **Listing 1**
    ///
    ///     Let id: "585326897542157909654321", user: "543209876543210987654322" (Device id and User id as parameters to be passed)
    ///
    /// When called function becomes successfully removes user from the device and the successful response from json will be as like below.
    ///
    /// **Listing 2**
    ///
    ///     {
    ///     "_id":"585326897542157909654321",
    ///     "updatedAt":"2018-04-23T12:47:36.417Z",
    ///     "createdAt":"2018-01-19T14:09:13.500Z",
    ///     "name":"test",
    ///     "mac":"1C:50:8C:9F:TE:ST",
    ///     "firmwareVersion":"2018.04.20",
    ///     "model":"SureBell-Rev-2",
    ///     "adminToken":"11616",
    ///     "vendor":"5682hbn2873hjvvo98879373",
    ///     "createdBy":"5682hbn2873hjvvo98879372",
    ///     "__v":880,
    ///     "deviceUser":"5682hbn2873hjvvo98879373",
    ///     "lastImage":"https://storage.googleapis.com/pioctave/585326897542157909654321%2FlastImage-1517208643113?GoogleAccessId=cloudstorage@masterserver-1332.iam.gserviceaccount.com&Expires=1524491257&Signature=Vw%2FOyzjc106AQUt3WWs99pzyxBwTxDViyaNh9mnY3fXDC%2",
    ///     "pairedWith":[
    ///     ],
    ///     "settings":{
    ///     "chime":true,
    ///     "motionSensitivity":1,
    ///     "cloudUpload":false,
    ///     "timeZone":"+5:30",
    ///     "cloudUploadSetting":{
    ///     "ring":{
    ///     "settings":[
    ///     ],
    ///     "status":true
    ///     },
    ///     "motion":{
    ///     "settings":[
    ///     ],
    ///     "status":true
    ///     }
    ///     },
    ///     "video":{
    ///     "recording":{
    ///     "quality":1
    ///     },
    ///     "live":{
    ///     "quality":1
    ///     }
    ///     },
    ///     "audio":{
    ///     "speaker_vol":2,
    ///     "mic_gain":2
    ///     },
    ///     "motionSettings":[
    ///     ]
    ///     },
    ///     "upgradeAttempts":0,
    ///     "versionOnDevice":0,
    ///     "upgradeStatus":"UPGRADED",
    ///     "location":[
    ///     12.9741833,
    ///     77.65403609999997
    ///     ],
    ///     "invites":[
    ///     ],
    ///     "users":[
    ///     {
    ///     "_id":"543209876543210987654321",
    ///     "email":"sample@gmail.com",
    ///     "firstname":"sample",
    ///     "lastname":"test",
    ///     "phone":"+919876598765"
    ///     }
    ///     ],
    ///     "admins":[
    ///     {
    ///     "_id":"543209876543210987654321",
    ///     "email":"sample@gmail.com",
    ///     "firstname":"sample",
    ///     "lastname":"test",
    ///     "phone":"+919876598765"
    ///     }
    ///     ],
    ///     "tag":"ALPHA"
    ///     }
    ///
    /// When called function fails it gives error with the error description code as like below.
    ///
    /// **Listing 3**
    ///
    ///     {
    ///        "code": 400,
    ///        "error": "invalid_params",
    ///        "error_description": "missing necessary params{\"query\":[\"vendor\"],\"body\":[],\"params\":[]}"
    ///     }
    ///
    /// - Parameters:
    ///   - id:String (Device id as a String from server)
    ///   - user:String (User id as a String from server)
    ///   - callback:@escaping (Calls back registered user id with device id to remove that user from that device)
    ///   - newDevice:Device (Takes device id as parameter to remove specified user id user from that device)
    ///   - failure:@escaping (Fails to remove mentioned user from the device)
    ///   - error:Any (Error can be APIError or general error, which may be because of bad network or nonexist user)
    ///
    public func removeUser(id:String, user:String, callback:@escaping (_ newDevice:Device)->Void, failure:@escaping (_ error:Any)->Void) -> Void {
        DeviceHttps.instance.removeUser(id: id, user:user, callback: callback, failure: failure)
    }
    
    
    /// It calls back removeAdmin() function to remove Admin from the specified Device id. Here user of the device is a admin then he can remove other admins from the same device.
    ///
    /// **Listing 1**
    ///
    ///     Let id: "585326897542157909654321", user: "543209876543210987654322"
    ///     (Device id and User id as parameters to be passed)
    ///
    /// When called function successfully removes Admin from the specified device, server returns json response as like below.
    ///
    /// **Listing 2**
    ///
    ///     {
    ///     "_id":"585326897542157909654321",
    ///     "updatedAt":"2018-04-23T12:47:36.417Z",
    ///     "createdAt":"2018-01-19T14:09:13.500Z",
    ///     "name":"test",
    ///     "mac":"1C:50:8C:9F:TE:ST",
    ///     "firmwareVersion":"2018.04.20",
    ///     "model":"SureBell-Rev-2",
    ///     "adminToken":"11616",
    ///     "vendor":"5682hbn2873hjvvo98879373",
    ///     "createdBy":"5682hbn2873hjvvo98879372",
    ///     "__v":880,
    ///     "deviceUser":"5682hbn2873hjvvo98879373",
    ///     "lastImage":"https://storage.googleapis.com/pioctave/585326897542157909654321%2FlastImage-1517208643113?GoogleAccessId=cloudstorage@masterserver-1332.iam.gserviceaccount.com&Expires=1524491257&Signature=Vw%2FOyzjc106AQUt3WWs99pzyxBwTxDViyaNh9mnY3fXDC%2",
    ///     "pairedWith":[
    ///     ],
    ///     "settings":{
    ///     "chime":true,
    ///     "motionSensitivity":1,
    ///     "cloudUpload":false,
    ///     "timeZone":"+5:30",
    ///     "cloudUploadSetting":{
    ///     "ring":{
    ///     "settings":[
    ///     ],
    ///     "status":true
    ///     },
    ///     "motion":{
    ///     "settings":[
    ///     ],
    ///     "status":true
    ///     }
    ///     },
    ///     "video":{
    ///     "recording":{
    ///     "quality":1
    ///     },
    ///     "live":{
    ///     "quality":1
    ///     }
    ///     },
    ///     "audio":{
    ///     "speaker_vol":2,
    ///     "mic_gain":2
    ///     },
    ///     "motionSettings":[
    ///     ]
    ///     },
    ///     "upgradeAttempts":0,
    ///     "versionOnDevice":0,
    ///     "upgradeStatus":"UPGRADED",
    ///     "location":[
    ///     12.9741833,
    ///     77.65403609999997
    ///     ],
    ///     "invites":[
    ///     ],
    ///     "users":[
    ///     {
    ///     "_id":"543209876543210987654321",
    ///     "email":"sample@gmail.com",
    ///     "firstname":"sample",
    ///     "lastname":"test",
    ///     "phone":"+919876598765"
    ///     }
    ///     ],
    ///     "admins":[
    ///     {
    ///     "_id":"543209876543210987654321",
    ///     "email":"sample@gmail.com",
    ///     "firstname":"sample",
    ///     "lastname":"test",
    ///     "phone":"+919876598765"
    ///     }
    ///     ],
    ///     "tag":"ALPHA"
    ///     }
    ///
    /// When called function fails to remove Admin, it gives error with the error description and the json error response will be as like below.
    ///
    /// **Listing 3**
    ///
    ///     {
    ///        "code": 400,
    ///        "error": "invalid_params",
    ///        "error_description": "missing necessary params{\"query\":[\"vendor\"],\"body\":[],\"params\":[]}"
    ///     }
    ///
    /// - Parameters:
    ///   - id:String (Device as String from server)
    ///   - user:String (User id as String from server)
    ///   - callback:@escaping (Calls back removeAdmin function to remove particular Admin from the specific device)
    ///   - newDevice:Device (Here it removes Admin by using User id from the specified Device id)
    ///   - failure:@escaping (It fails to remove Admin from specified device)
    ///   - error:Any (Error can be APIError or general error, which can be because of bad network, non exist user or missing parameters)
    ///
    public func removeAdmin(id:String, user:String, callback:@escaping (_ newDevice:Device)->Void, failure:@escaping (_ error:Any)->Void) -> Void {
        DeviceHttps.instance.removeAdmin(id: id, user: user, callback: callback, failure: failure)
    }
    
    
    
    /// If user a device is a Admin and if they wants to make another user as Admin then they cam use this function to give admin rights to the user.
    ///
    /// **Listing 1**
    ///
    ///      Let id: "585326897542157909654321", user: "543209876543210987654322"
    ///     (Device id and User id as parameters to be passed)
    ///
    /// When called function successfully gives Admin rights to the user as per mentioned device and user id, server returns json success response as like below.
    ///
    /// **Listing 2**
    ///
    ///     {
    ///     "_id":"585326897542157909654321",
    ///     "updatedAt":"2018-04-23T12:32:51.053Z",
    ///     "createdAt":"2018-01-19T14:09:13.500Z",
    ///     "name":"test",
    ///     "mac":"1C:50:8C:9F:TE:ST",
    ///     "firmwareVersion":"2018.04.20",
    ///     "model":"SureBell-Rev-2",
    ///     "adminToken":"11616",
    ///     "vendor":"5682hbn2873hjvvo98879373",
    ///     "createdBy":"5682hbn2873hjvvo98879372",
    ///     "__v":878,
    ///     "deviceUser":"5682hbn2873hjvvo98879373",   "lastImage":"https://storage.googleapis.com/pioctave/585326897542157909654321%2FlastImage-1517208643113?GoogleAccessId=cloudstorage@masterserver-1332.iam.gserviceaccount.com&Expires=1524490371&Signature=WSicEA6%2BZTpWeNIXWAMFuzG0BuX10z7TFEQOSEdlKkKGs1%2",
    ///     "pairedWith":[
    ///     ],
    ///     "settings":{
    ///     "chime":true,
    ///     "motionSensitivity":1,
    ///     "cloudUpload":false,
    ///     "timeZone":"+5:30",
    ///     "cloudUploadSetting":{
    ///     "ring":{
    ///     "settings":[
    ///     ],
    ///     "status":true
    ///     },
    ///     "motion":{
    ///     "settings":[
    ///     ],
    ///     "status":true
    ///     }
    ///     },
    ///     "video":{
    ///     "recording":{
    ///     "quality":1
    ///     },
    ///     "live":{
    ///     "quality":1
    ///     }
    ///     },
    ///     "audio":{
    ///     "speaker_vol":2,
    ///     "mic_gain":2
    ///     },
    ///     "motionSettings":[
    ///     ]
    ///     },
    ///     "upgradeAttempts":0,
    ///     "versionOnDevice":0,
    ///     "upgradeStatus":"UPGRADED",
    ///     "location":[
    ///     12.9741833,
    ///     77.65403609999997
    ///     ],
    ///     "invites":[
    ///     ],
    ///     "users":[
    ///     {
    ///     "_id":"543209876543210987654321",
    ///     "email":"sample@gmail.com",
    ///     "firstname":"sample",
    ///     "lastname":"test",
    ///     "phone":"+919876543210"
    ///     }
    ///     ],
    ///     "admins":[
    ///     {
    ///     "_id":"543209876543210987654321",
    ///     "email":"sample@gmail.com",
    ///     "firstname":"sample",
    ///     "lastname":"test",
    ///     "phone":"+919876543210"
    ///     }
    ///     ],
    ///     "tag":"ALPHA"
    ///     }
    ///
    /// When called funtion fails to give Admin rights to user, the server returns error response with the error description code as like below.
    ///
    /// **Listing 3**
    ///
    ///     {
    ///        "code": 400,
    ///        "error": "invalid_params",
    ///        "error_description": "missing necessary params{\"query\":[\"vendor\"],\"body\":[],\"params\":[]}"
    ///     }
    ///
    /// - Parameters:
    ///   - id:String (Device id as String from server)
    ///   - user:String (User id as String from server)
    ///   - callback:@escaping (Calls back inviteAdmin fucntion to give admin rights to the sprecified user of the device)
    ///   - newDevice:Device (Here it takes user id and gives admin rights to the mention device id)
    ///   - failure:@escaping (Fails to give admin rights to the specified user)
    ///   - error:Any (Error can be APIError or general error, Which can be because of missing necessary parameters)
    ///
    public func inviteAdmin(id:String, user:String, callback:@escaping (_ newDevice:Device)->Void, failure:@escaping (_ error:Any)->Void) -> Void {
        DeviceHttps.instance.inviteAdmin(id: id, user:user, callback: callback, failure: failure)
    }
    
    
    /// If user wants to delete already sent invitation to a user, then they can use this function to delete it.
    ///
    /// **Listing 1**
    ///
    ///      Let id: "585326897542157909654321", user: "543209876543210987654322"
    ///     (Device id and User id as parameters to be passed)
    ///
    /// When called function successfully removes sent invitation then server returns json success response as like below.
    ///
    /// **Listing 2**
    ///
    ///     {
    ///     "_id":"585326897542157909654321",
    ///     "updatedAt":"2018-04-23T12:47:36.417Z",
    ///     "createdAt":"2018-01-19T14:09:13.500Z",
    ///     "name":"test",
    ///     "mac":"1C:50:8C:9F:TE:ST",
    ///     "firmwareVersion":"2018.04.20",
    ///     "model":"SureBell-Rev-2",
    ///     "adminToken":"11616",
    ///     "vendor":"5682hbn2873hjvvo98879373",
    ///     "createdBy":"5682hbn2873hjvvo98879372",
    ///     "__v":880,
    ///     "deviceUser":"5682hbn2873hjvvo98879373",
    ///     "lastImage":"https://storage.googleapis.com/pioctave/585326897542157909654321%2FlastImage-1517208643113?GoogleAccessId=cloudstorage@masterserver-1332.iam.gserviceaccount.com&Expires=1524491257&Signature=Vw%2FOyzjc106AQUt3WWs99pzyxBwTxDViyaNh9mnY3fXDC%2",
    ///     "pairedWith":[
    ///     ],
    ///     "settings":{
    ///     "chime":true,
    ///     "motionSensitivity":1,
    ///     "cloudUpload":false,
    ///     "timeZone":"+5:30",
    ///     "cloudUploadSetting":{
    ///     "ring":{
    ///     "settings":[
    ///     ],
    ///     "status":true
    ///     },
    ///     "motion":{
    ///     "settings":[
    ///     ],
    ///     "status":true
    ///     }
    ///     },
    ///     "video":{
    ///     "recording":{
    ///     "quality":1
    ///     },
    ///     "live":{
    ///     "quality":1
    ///     }
    ///     },
    ///     "audio":{
    ///     "speaker_vol":2,
    ///     "mic_gain":2
    ///     },
    ///     "motionSettings":[
    ///     ]
    ///     },
    ///     "upgradeAttempts":0,
    ///     "versionOnDevice":0,
    ///     "upgradeStatus":"UPGRADED",
    ///     "location":[
    ///     12.9741833,
    ///     77.65403609999997
    ///     ],
    ///     "invites":[
    ///     ],
    ///     "users":[
    ///     {
    ///     "_id":"543209876543210987654321",
    ///     "email":"sample@gmail.com",
    ///     "firstname":"sample",
    ///     "lastname":"test",
    ///     "phone":"+919876598765"
    ///     }
    ///     ],
    ///     "admins":[
    ///     {
    ///     "_id":"543209876543210987654321",
    ///     "email":"sample@gmail.com",
    ///     "firstname":"sample",
    ///     "lastname":"test",
    ///     "phone":"+919876598765"
    ///     }
    ///     ],
    ///     "tag":"ALPHA"
    ///     }
    ///
    /// When called function fails to remove sent invitation, then server resturns json error response with the error descripton code as like below.
    ///
    /// **Listing 3**
    ///
    ///     {
    ///        "code": 400,
    ///        "error": "invalid_params",
    ///        "error_description": "missing necessary params{\"query\":[\"vendor\"],\"body\":[],\"params\":[]}"
    ///     }
    ///
    /// - Parameters:
    ///   - id:String (Device id as String from server)
    ///   - user:String (User id as String from server)
    ///   - callback:@escaping (Calls back removeInvite function to delete sent invitation to the user)
    ///   - newDevice:Device (Here it takes device to which the particular user id is registered to delete the sent invitation)
    ///   - failure:@escaping (Fails to delete teh invitation which has already sent)
    ///   - error:Any (Error can be APIError or general error, which can be because of missing necessary parameters)
    ///
    public func removeInvite(id:String, user:String, callback:@escaping (_ newDevice:Device)->Void, failure:@escaping (_ error:Any)->Void) -> Void {
        DeviceHttps.instance.removeInvite(id: id, user:user, callback: callback, failure: failure)
    }
    
    
    /// It calls back deviceAvailable() funtion to list down all registered devices on sure server.
    /// If called function becomes successful the it lists down all devices registered on sure server and server returns json success response as like below.
    ///
    /// **Listing 1**
    ///
    ///     [
    ///     {
    ///     "_id":"5a0d5545c2e850001ae2c710",
    ///     "isAvailable":true,
    ///     "updatedAt":"2018-04-27T07:09:53.385Z"
    ///     },
    ///     {
    ///     "_id":"585326897542157909654321",
    ///     "isAvailable":true,
    ///     "updatedAt":"2018-04-27T07:09:53.385Z"
    ///     }
    ///     ]
    ///
    /// If called function fails to lists down all devices resgistered on sure server then server returns json error response as like below.
    ///
    /// **Listing 2**
    ///
    ///     {
    ///        "code": 400,
    ///        "error": "invalid_params",
    ///        "error_description": "missing necessary params{\"query\":[\"vendor\"],\"body\":[],\"params\":[]}"
    ///     }
    ///
    /// - Parameters:
    ///   - callback:@escaping (Calls back deviceAvailable function to list all devices available on sure server)
    ///   - newDevice:DeviceAvailable (Here it lists all registered devices as per available device id's on sure server)
    ///   - failure:@escaping (Fails to list down all devices available on sure server)
    ///   - error:Any (Error can be APIError or general error, can be because of bad network or unregistered devices)
    ///
    public func deviceAvailable(callback:@escaping (_ newDevice:[DeviceAvailable])->Void, failure:@escaping (_ error:Any)->Void) -> Void {
        DeviceHttps.instance.deviceAvailable(callback: callback, failure: failure)
    }
    
    
    /// It calls deviceAvailable() function to display whether the device is there on server or removed as per specified device id.
    ///
    /// **Listing 1**
    ///
    ///     Let id: "585326897542157909654321"
    ///     (Device id as parameter to be passed)
    ///
    /// When called function successfully displays registered device on sure server as per specified device id, server returns json response as like below.
    ///
    /// **Listing 2**
    ///
    ///     {
    ///     "status":"success",
    ///     "message":"AVAILABLE"
    ///     }
    ///
    /// When called function fails to display device availability on sure server as per specified device id, server returns json error response with the error description code as like below.
    ///
    /// **Listing 3**
    ///
    ///     {
    ///        "code": 400,
    ///        "error": "invalid_params",
    ///        "error_description": "missing necessary params{\"query\":[\"vendor\"],\"body\":[],\"params\":[]}"
    ///     }
    ///
    /// - Parameters:
    ///   - id:String (Device id as String from server)
    ///   - callback:@escaping (Calls back deviceAvailable function to check whether the specified device id is there on sure server or not)
    ///   - newDevice:DeviceAvailbale (Here it takes device id and check on sure serever whether it is registered or removed)
    ///   - failure:@escaping (Fails to display the specified device id's device availability)
    ///   - error:Any (Error can be APIError or general error, because of unregistered device or missing necessary parameter)
    ///
    public func deviceAvailable(_ id:String, callback:@escaping (_ newDevice:DeviceAvailable)->Void, failure:@escaping (_ error:Any)->Void) -> Void {
        DeviceHttps.instance.deviceAvailable(id, callback: callback, failure: failure)
    }
    
    
    /// It calls back removeMe() fucntion, when user wants to remove himself from his device by specifying device and user id.
    ///
    /// **Listing 1**
    ///
    ///     Let id: "585326897542157909654321", user: "543209876543210987654322"
    ///     (Device id and User id as parameters to be passed)
    ///
    /// When called function removes user himself from his device successfully, server returns json success response as like below.
    ///
    /// **Listing 2**
    ///
    ///     {
    ///     "_id":"585326897542157909654321",
    ///     "updatedAt":"2018-04-23T12:47:36.417Z",
    ///     "createdAt":"2018-01-19T14:09:13.500Z",
    ///     "name":"test",
    ///     "mac":"1C:50:8C:9F:TE:ST",
    ///     "firmwareVersion":"2018.04.20",
    ///     "model":"SureBell-Rev-2",
    ///     "adminToken":"11616",
    ///     "vendor":"5682hbn2873hjvvo98879373",
    ///     "createdBy":"5682hbn2873hjvvo98879372",
    ///     "__v":880,
    ///     "deviceUser":"5682hbn2873hjvvo98879373",
    ///     "lastImage":"https://storage.googleapis.com/pioctave/585326897542157909654321%2FlastImage-1517208643113?GoogleAccessId=cloudstorage@masterserver-1332.iam.gserviceaccount.com&Expires=1524491257&Signature=Vw%2FOyzjc106AQUt3WWs99pzyxBwTxDViyaNh9mnY3fXDC%2",
    ///     "pairedWith":[
    ///     ],
    ///     "settings":{
    ///     "chime":true,
    ///     "motionSensitivity":1,
    ///     "cloudUpload":false,
    ///     "timeZone":"+5:30",
    ///     "cloudUploadSetting":{
    ///     "ring":{
    ///     "settings":[
    ///     ],
    ///     "status":true
    ///     },
    ///     "motion":{
    ///     "settings":[
    ///     ],
    ///     "status":true
    ///     }
    ///     },
    ///     "video":{
    ///     "recording":{
    ///     "quality":1
    ///     },
    ///     "live":{
    ///     "quality":1
    ///     }
    ///     },
    ///     "audio":{
    ///     "speaker_vol":2,
    ///     "mic_gain":2
    ///     },
    ///     "motionSettings":[
    ///     ]
    ///     },
    ///     "upgradeAttempts":0,
    ///     "versionOnDevice":0,
    ///     "upgradeStatus":"UPGRADED",
    ///     "location":[
    ///     12.9741833,
    ///     77.65403609999997
    ///     ],
    ///     "invites":[
    ///     ],
    ///     "users":[
    ///     {
    ///     "_id":"543209876543210987654321",
    ///     "email":"sample@gmail.com",
    ///     "firstname":"sample",
    ///     "lastname":"test",
    ///     "phone":"+919876598765"
    ///     }
    ///     ],
    ///     "admins":[
    ///     {
    ///     "_id":"543209876543210987654321",
    ///     "email":"sample@gmail.com",
    ///     "firstname":"sample",
    ///     "lastname":"test",
    ///     "phone":"+919876598765"
    ///     }
    ///     ],
    ///     "tag":"ALPHA"
    ///     }
    ///
    /// When called function fails to remove user himself from their device, server returns json error response with error code description as like below.
    ///
    /// **Listing 3**
    ///
    ///     {
    ///        "code": 400,
    ///        "error": "invalid_params",
    ///        "error_description": "missing necessary params{\"query\":[\"vendor\"],\"body\":[],\"params\":[]}"
    ///     }
    ///
    /// - Parameters:
    ///   - id:String (Device id as String from server)
    ///   - user:String (User id as String from server)
    ///   - callback:@escaping (Calls back removeMe function to remove user himself from his device)
    ///   - newDevice:Device (Here it takes device id and user id and remmoves user himself from specified device)
    ///   - failure:@escaping (Fails to remove user himself from his device as per mentioned device and user id)
    ///   - error:Any (Error can be APIError or general error,can be because of missing necessary parameters)
    ///
    public func removeMe(id:String, user:String, callback:@escaping (_ newDevice:Device)->Void, failure:@escaping (_ error:Any)->Void) -> Void {
        DeviceHttps.instance.removeMe(id: id, user: user, callback: callback, failure: failure)
    }
    
    
    /// It calls back acceptInvite() function to make invitation accepted person as a user of the specified device.
    ///
    /// **Listing 1**
    ///
    ///     Let id: "543209876543210987654321"
    ///     (Device id as parameter to be passed)
    ///
    /// When called function successfully makes the invitation accepted person as a user of the specified device, server returns successful json response as like below.
    ///
    /// **Listing 2**
    ///
    ///     {
    ///     "_id":"585326897542157909654322",
    ///     "updatedAt":"2018-04-23T12:20:49.230Z",
    ///     "createdAt":"2018-01-19T14:09:13.500Z",
    ///     "name":"test",
    ///     "mac":"1C:50:8C:9F:TE:ST",
    ///     "firmwareVersion":"2018.04.20",
    ///     "model":"SureBell-Rev-2",
    ///     "adminToken":"11616",
    ///     "vendor":"5682hbn2873hjvvo98879373",
    ///     "createdBy":"5682hbn2873hjvvo98879372",
    ///     "__v":877,
    ///     "deviceUser":"5682hbn2873hjvvo98879373", "lastImage":"https://storage.googleapis.com/pioctave/585326897542157909654321%2FlastImage-1517208643113?GoogleAccessId=cloudstorage@masterserver-1332.iam.gserviceaccount.com&Expires=1524489649&Signature=jQp9SbUDtX1%2BQYOCSlWwelnSYcmmcKIbvNbYARxhj%",
    ///     "pairedWith":[
    ///     ],
    ///     "settings":{
    ///     "chime":true,
    ///     "motionSensitivity":1,
    ///     "cloudUpload":false,
    ///     "timeZone":"+5:30",
    ///     "cloudUploadSetting":{
    ///     "ring":{
    ///     "settings":[
    ///     ],
    ///     "status":true
    ///     },
    ///     "motion":{
    ///     "settings":[
    ///     ],
    ///     "status":true
    ///     }
    ///     },
    ///     "video":{
    ///     "recording":{
    ///     "quality":1
    ///     },
    ///     "live":{
    ///     "quality":1
    ///     }
    ///     },
    ///     "audio":{
    ///     "speaker_vol":2,
    ///     "mic_gain":2
    ///     },
    ///     "motionSettings":[
    ///     ]
    ///     },
    ///     "upgradeAttempts":0,
    ///     "versionOnDevice":0,
    ///     "upgradeStatus":"UPGRADED",
    ///     "location":[
    ///     12.9741833,
    ///     77.65403609999997
    ///     ],
    ///     "invites":[
    ///     ],
    ///     "users":[
    ///     {
    ///     "_id":"543209876543210987654321",
    ///     "email":"sample@gmail.com",
    ///     "firstname":"sample",
    ///     "lastname":"test",
    ///     "phone":"+919876598765"
    ///     }
    ///     ],
    ///     "admins":[
    ///     {
    ///     "_id":"543209876543210987654321",
    ///     "email":"sample@gmail.com",
    ///     "firstname":"sample",
    ///     "lastname":"test",
    ///     "phone":"+919876598765"
    ///     }
    ///     ],
    ///     "tag":"ALPHA"
    ///     }
    ///
    /// when called function fails to makes invitation accepted person as user of specified device, server returns json error response as like below.
    ///
    /// **Listing 3**
    ///
    ///     {
    ///        "code": 400,
    ///        "error": "invalid_params",
    ///        "error_description": "missing necessary params{\"query\":[\"vendor\"],\"body\":[],\"params\":[]}"
    ///     }
    ///
    /// - Parameters:
    ///   - id:String  (Device id as String from server)
    ///   - callback:@escaping (calls back acceptInvite function to accept recieved invitation from other device user)
    ///   - newDevice:Device (Here it takes device id as parameter to make invitation accepted person as user of that particular device)
    ///   - failure:@escaping (Fails to make that person as user of the specified device)
    ///   - error:Any (Erro can be APIError or general error, which can be because of unregitered user or missing necessary parameters)
    ///
    public func acceptInvite(id:String, callback:@escaping (_ newDevice:Device)->Void, failure:@escaping (_ error:Any)->Void) -> Void {
        DeviceHttps.instance.acceptInvite(id: id, callback: callback, failure: failure)
    }
    
    
    /// It calls rejectInvite() function to help user to reject invitation they have received.
    ///
    /// **Listing 1**
    ///
    ///      Let id: "543209876543210987654321"
    ///     (Device id as parameter to be passed)
    ///
    /// When called function successfully rejects received invitation, server returns json successful response as like below.
    ///
    /// **Listing 2**
    ///
    ///     {
    ///     "status":"success",
    ///     "message":"Done...!"
    ///     }
    ///
    /// When called function fails to reject received invitation, server returns json error response with the error decription code as like below.
    ///
    /// **Listing 3**
    ///
    ///     {
    ///        "code": 400,
    ///        "error": "invalid_params",
    ///        "error_description": "missing necessary params{\"query\":[\"vendor\"],\"body\":[],\"params\":[]}"
    ///     }
    ///
    /// - Parameters:
    ///   - id:String (Device id as a String from server)
    ///   - callback:@escaping (Calls back rejectInvite function to make user reject the received invitation)
    ///   - newDevice:Device (Here it takes device id as parameter and helps user to reject the invitation)
    ///   - failure:@escaping (Fails to make user reject received invivtation)
    ///   - error:Any (Error can be APIError or general error, which is because of missing necessary parameters)
    ///
    public func rejectInvite(id:String, callback:@escaping (_ newDevice:Device)->Void, failure:@escaping (_ error:Any)->Void) -> Void {
        DeviceHttps.instance.rejectInvite(id: id, callback: callback, failure: failure)
    }
    
    
    /// It calls getEvent() function to get latest notification on the event list.
    ///
    /// **Listing 1**
    ///
    ///     Let device_id: "5a61fc09f300270010f9bcd1", event_id: "5af267240122040010b0b025"
    ///     (Device id and Event id as parameters to be passed)
    ///
    /// When called function successfully gets latest notification on the event list, server returns successful json response as like below.
    ///
    /// **Listing 2**
    ///
    ///     {
    ///     "_id":"5ae2ac4c60178d001a527bb6",
    ///     "name":"motion",
    ///     "type":"motion",
    ///     "createdAt":"2018-04-27T04:49:20.000Z",
    ///     "createdBy":{
    ///     "_id":"585326897542157909654321",
    ///     "name":"test",
    ///     "lastImage":"1517208643113"
    ///     },
    ///     "image":"https://storage.googleapis.com/pioctave/585326897542157909654321%2F5ae2ac4c60178d001a527bb6-image%2F1524804560.jpg?GoogleAccessId=cloudstorage@masterserver-1332.iam.gserviceaccount.com&Expires=1524816843&Signature=CuWbO3uUq1niHhNxd6Aqf1XniKaaS24oOpwSvsdCr1Bca36QytUCyEGQPo5j2B4ojGvUJYZ4c3T6knmG%",
    ///     "allowedUsers":[
    ///     "5682hbn2873hjvvo98879372",
    ///     "543209876543210987654321"
    ///     ],
    ///     "comments":[
    ///     ],
    ///     "like":[
    ///     ],
    ///     "status":"missed",
    ///     "videoSize":[
    ///     ],   "video":"https://storage.googleapis.com/pioctave/585326897542157909654321%2F5ae2ac4c60178d001a527bb6-video%2F1524804684565.mp4?GoogleAccessId=cloudstorage@masterserver-1332.iam.gserviceaccount.com&Expires=1524816843&Signature=EtK284QRlMv7Mpl48h0inmgMWwQ6jxjQHk%",
    ///     "__v":0,
    ///     "imageSize":7803,
    ///     "urlSummary":"https://storage.googleapis.com/summary-service/585326897542157909654321%2F5ae2ac4c60178d001a527bb6-video%2F1524804684565.mp4?GoogleAccessId=cloudstorage@masterserver-1332.iam.gserviceaccount.com&Expires=1524816843&Signature=Dj9Z5DWQQ9omi9ghsrH23VFT73st%2"
    ///     }
    ///
    /// When called function fails to get latest notification on the event list, server returns json error response with the error description code as like below.
    ///
    /// **Listing 3**
    ///
    ///     {
    ///        "code": 400,
    ///        "error": "invalid_params",
    ///        "error_description": "missing necessary params{\"query\":[\"vendor\"],\"body\":[],\"params\":[]}"
    ///     }
    ///
    /// - Parameters:
    ///   - device_id:String (device_id as a String from server)
    ///   - event_id:
    ///   - callback:@escaping (Calls back getEvent function to get latest notification on the event list)
    ///   - newDevice:Event (By using event id it gives latest event to the specified device)
    ///   - failure:@escaping (Fails to push latest notification to teh specified device)
    ///   - error:Any (Error can be APIError or general error, because of missing necessary parameters)
    ///
    public func getEvent(device_id:String, event_id:String, callback:@escaping (_ newDevice:Event)->Void, failure:@escaping (_ error:Any)->Void) -> Void {
        DeviceHttps.instance.getEvent(device_id: device_id, event_id: event_id, callback: callback, failure: failure)
    }
    
    
    /// It calls shareEvent() function to share a notification with other user as per user request. If the user of the device is a admin the only they can share event.
    ///
    /// **Listing 1**
    ///
    ///     Let event_id: "5adef3addda123001a123d31", shared: true
    ///     (event_id and share status as parameters to be passed)
    ///
    /// When called function successfully shares a notification as per user request, server returns successful json response as like below.
    ///
    /// **Listing 2**
    ///
    ///     {
    ///     "_id":"5adef3addda123001a123d31",
    ///     "name":"motion",
    ///     "type":"motion",
    ///     "createdAt":"2018-04-24T09:06:52.000Z",
    ///     "createdBy":"585326897542157909654321",
    ///     "image":"1524560812.jpg",
    ///     "allowedUsers":[
    ///     "5682hbn2873hjvvo98879372",
    ///     "543209876543210987654321"
    ///     ],
    ///     "comments":[
    ///     ],
    ///     "like":[
    ///     ],
    ///     "status":"missed",
    ///     "videoSize":[
    ///     1640451,
    ///     1640451
    ///     ],
    ///     "video":"https://goo.gl/cc2ujs",
    ///     "__v":0,
    ///     "imageSize":8049,
    ///     "shared":true
    ///     }
    ///
    /// When called function fails to share a notification as per user request, server returns json error response with the error description code as like below.
    ///
    /// **Listing 3**
    ///
    ///     {
    ///        "code": 400,
    ///        "error": "invalid_params",
    ///        "error_description": "missing necessary params{\"query\":[\"vendor\"],\"body\":[],\"params\":[]}"
    ///     }
    ///
    /// - Parameters:
    ///   - event_id:String (Event id as a String from server)
    ///   - shared:Bool (Shared as a boolean value provided by user)
    ///   - callback:@escaping (Calls back shareEvent to share a motion notification to others)
    ///   - newDevice:Event (Here it receives boolean value as parameter from user and as per specified event id it shares that particular notification with the other user)
    ///   - failure:@escaping (Fails to share notification to other user as per user request)
    ///   - error:Any (Error can be APIError or general error, because of missing necessary parameters)
    ///
    public func shareEvent(event_id:String, shared:Bool, callback:@escaping (_ newDevice:Event)->Void, failure:@escaping (_ error:Any)->Void) -> Void {
        DeviceHttps.instance.shareEvent(event_id: event_id, shared: shared, callback: callback, failure: failure)
    }
    
    
    /// It calls changeEventStatus() function to change the notification status as either missed or attended notification as per user action.
    /// The user viewed notifications are as attended and the notifications which are not yet viewed are as missed notifications.
    ///
    /// **Listing 1**
    ///
    ///     Let event_id: "5adef3addda123001a123d31", status:"missed"
    ///     (Event id and status as parameters to be passed)
    ///
    /// When the called function successfully changes the status of the notifications as per the user action, server returns successful json response as like below.
    ///
    /// **Listing 2**
    ///
    ///     {
    ///     "_id":"5adef3addda123001a123d31",
    ///     "name":"motion",
    ///     "type":"motion",
    ///     "createdAt":"2018-04-24T09:06:52.000Z",
    ///     "createdBy":{
    ///     "_id":"585326897542157909654321",
    ///     "name":"test",
    ///     "lastImage":"1517208643113"
    ///     },
    ///     "image":"1524560812.jpg",
    ///     "__v":0,
    ///     "imageSize":8049,
    ///     "shared":true,
    ///     "allowedUsers":[
    ///     "5682hbn2873hjvvo98879372",
    ///     "543209876543210987654321"
    ///     ],
    ///     "comments":[
    ///     ],
    ///     "like":[
    ///     ],
    ///     "status":"missed",
    ///     "videoSize":[
    ///     1640451,
    ///     1640451
    ///     ],
    ///     "video":[
    ///     "1524560813511.mp4",
    ///     "1524560812.mp4"
    ///     ]
    ///     }
    ///
    /// When called function fails to change event status, server returns error json response with the error description code as like below.
    ///
    /// **Listing 3**
    ///
    ///     {
    ///        "code": 400,
    ///        "error": "invalid_params",
    ///        "error_description": "missing necessary params{\"query\":[\"vendor\"],\"body\":[],\"params\":[]}"
    ///     }
    ///
    /// - Parameters:
    ///   - event_id:String (Event id as a String from server)
    ///   - status:String (Status of notification as String)
    ///   - callback:@escaping (Calls back changeEventStatus function to change notification status as per user activities)
    ///   - newDevice:Event (Here it takes event id and changed the status of it as per user action)
    ///   - failure:@escaping (Fails to change the status of the event as per user action)
    ///   - error:Any (Error can be APIError or general error, because of missing necessary parameters)
    ///
    public func changeEventStatus(event_id:String, status:String, callback:@escaping (_ newDevice:Event)->Void, failure:@escaping (_ error:Any)->Void) -> Void {
        DeviceHttps.instance.changeEventStatus(event_id: event_id, status: status, callback: callback, failure: failure)
    }
    
    
    /// To get the latest captured image as the image to be displayed, when user gets call, user needs setDeviceLatestImage() function.
    ///
    /// **Listing 1**
    ///
    ///     Let id: "585326897542157909654321", imageData:UIImagePNGRepresentation(im.image!)!, timeinms: "\(Utils.getCurrentMillis())"
    ///     (Device id, user image converted into png form and time in milliseconds are as parameters to be passed)
    ///
    /// When called function sets latest captured image successfully for the specified device, server returns successful json response as like below.
    ///
    /// **Listing 2**
    ///
    ///     {
    ///     "message":"https://storage.googleapis.com/pioctave/5a61fc09f300270010f9bcd%2FlastImage-1524830568600?GoogleAccessId=cloudstorage@masterserver-1332.iam.gserviceaccount.com&Expires=1524834170&Signature=b0ODghoIDNbydxcr8825fyfsy4Czg%2",
    ///     "status":"success"
    ///     }
    ///
    /// When called function fails to set latest captured image for the specified device, server returns json error with the error description code as like below.
    ///
    /// **Listing 3**
    ///
    ///     {
    ///        "code": 400,
    ///        "error": "invalid_params",
    ///        "error_description": "missing necessary params{\"query\":[\"vendor\"],\"body\":[],\"params\":[]}"
    ///     }
    ///
    /// - Parameters:
    ///   - id:String (Device id as a String from server)
    ///   - imageData:Data (User image converted into PNG form)
    ///   - timeinms:String (Time in milliseconds as String)
    ///   - callback:@escaping (Calls back setDeviceLatestImage function to set latest captured image for device)
    ///   - newDevice:Bool (Here as per prefered boolean value for device it sets latest captured image for that particular device)
    ///   - failure:@escaping (Fails to set latest captured image for the specified device)
    ///   - error:Any (Error can be APIError or general error, because of missing necessary parameters)
    ///
    public func setDeviceLatestImage(id:String, imageData: Data, timeinms:String, callback:@escaping (_ newDevice:Bool)->Void, failure:@escaping (_ error:Any)->Void) -> Void {
        DeviceHttps.instance.setDeviceLatestImage(id: id, imageData: imageData, timeinms: timeinms, callback: callback, failure: failure)
    }
    
    
    /// If the user is a admin of particular device, then he can delete unwanted notifications from his event list by using deleteEvent() function.
    ///
    /// **Listing 1**
    ///
    ///     Let device_id: "585326897542157909654321", event_id:"5adebb6edda13b001a1337a0"
    ///     (Device id and Event id are as parameters to be passed)
    ///
    /// When called function successfully deletes specified notification from the event list, server returns successful json response as like below.
    ///
    /// **Listing 2**
    ///
    ///     {
    ///     "status":"success",
    ///     "message":"removed"
    ///     }
    ///
    /// When called function fails to delete specified notification as per user request, server returns json error response with the error description code as like below.
    ///
    /// **Listing 3**
    ///
    ///     {
    ///        "code": 400,
    ///        "error": "invalid_params",
    ///        "error_description": "missing necessary params{\"query\":[\"vendor\"],\"body\":[],\"params\":[]}"
    ///     }
    ///
    /// - Parameters:
    ///   - device_id:String (Device id as a String from server)
    ///   - event_id:String (Event id as a String from server)
    ///   - callback:@escaping (Calls back deleteEvent function to delete specified notification in the event list as per user request)
    ///   - newDevice:StatusResponse (Here it takes device and event id to delete specified notification and gives back StatusResponse to update event status locally)
    ///   - failure:@escaping (Fails to delete specified notification from the event list)
    ///   - error:Any (Error can be APIError or general error, because of missing necessary parameters)
    ///
    public func deleteEvent(device_id:String, event_id:String, callback:@escaping (_ newDevice:StatusResponse)->Void, failure:@escaping (_ error:Any)->Void) -> Void {
        DeviceHttps.instance.deleteEvent(device_id: device_id, event_id: event_id, callback: callback, failure: failure)
    }
    
    
    /// It calls getTurnServerValues() function to get list of values from turn server to make video or live call.
    /// Successful called function connects call or it fails to connect call and server returns json error response with the error description code as like below.
    ///
    /// **Listing 1**
    ///
    ///     {
    ///        "code": 400,
    ///        "error": "invalid_params",
    ///        "error_description": "missing necessary params{\"query\":[\"vendor\"],\"body\":[],\"params\":[]}"
    ///     }
    ///
    /// - Parameters:
    ///   - callback:@escaping (Calls back getTurnServerValues function to get values from it to perform video call)
    ///   - turnList:[Turn] (Here turn server returns list of values to perform video call)
    ///   - failure:@escaping (Fails to get list of values from turn server)
    ///   - error:Any (Error can be APIError or general error, because of bad network or any other general issues)
    ///
    public func getTurnServerValues(callback:@escaping (_ turnList:[Turn])->Void, failure:@escaping (_ error:Any)->Void) -> Void {
        DeviceHttps.instance.getTurnServerValues(callback: callback, failure: failure)
    }

    //    case sendDeviceDiagnosis(id:String)
    
    
    /// It calls changeCloudUploadStatus() function to change cloud upload status(Enable/Disable) for specified device which avoids unnecessary usage of data.
    ///
    /// **Listing 1**
    ///
    ///     Let id: "585326897542157909654321", type: true
    ///     (Device id and type as boolean value are parameters to be passed)
    ///
    /// When called functio successfully changes cloud upload status for specified device, server returns successful json response as like below.
    ///
    /// **Listing 2**
    ///
    ///     {
    ///     "_id":"585326897542157909654321",
    ///     "updatedAt":"2018-04-23T13:42:19.386Z",
    ///     "createdAt":"2018-01-19T14:09:13.500Z",
    ///     "name":"test",
    ///     "mac":"1C:50:8C:9F:TE:ST",
    ///     "firmwareVersion":"2018.04.20",
    ///     "model":"SureBell-Rev-2",
    ///     "adminToken":"11616",
    ///     "vendor":"5682hbn2873hjvvo98879373",
    ///     "createdBy":"5682hbn2873hjvvo98879372",
    ///     "__v":888,
    ///     "deviceUser":"5682hbn2873hjvvo98879373",
    ///     "lastImage":"https://storage.googleapis.com/pioctave/585326897542157909654321%2FlastImage-1517208643113?GoogleAccessId=cloudstorage@masterserver-1332.iam.gserviceaccount.com&Expires=1524494539&Signature=lr1Gmn2LvkkO8leuo5XWRgB%2",
    ///     "pairedWith":[
    ///     ],
    ///     "settings":{
    ///     "chime":true,
    ///     "motionSensitivity":1,
    ///     "cloudUpload":true,
    ///     "timeZone":"+5:30",
    ///     "cloudUploadSetting":{
    ///     "ring":{
    ///     "settings":[
    ///     ],
    ///     "status":true
    ///     },
    ///     "motion":{
    ///     "settings":[
    ///     ],
    ///     "status":true
    ///     }
    ///     },
    ///     "video":{
    ///     "recording":{
    ///     "quality":1
    ///     },
    ///     "live":{
    ///     "quality":1
    ///     }
    ///     },
    ///     "audio":{
    ///     "speaker_vol":2,
    ///     "mic_gain":2
    ///     },
    ///     "motionSettings":[
    ///     ]
    ///     },
    ///     "upgradeAttempts":0,
    ///     "versionOnDevice":0,
    ///     "upgradeStatus":"UPGRADED",
    ///     "location":[
    ///     12.9741833,
    ///     77.65403609999997
    ///     ],
    ///     "invites":[
    ///     ],
    ///     "users":[
    ///     {
    ///     "_id":"543209876543210987654321",
    ///     "email":"sample@gmail.com",
    ///     "firstname":"sample",
    ///     "lastname":"test",
    ///     "phone":"+919876598765"
    ///     }
    ///     ],
    ///     "admins":[
    ///     {
    ///     "_id":"543209876543210987654321",
    ///     "email":"sample@gmail.com",
    ///     "firstname":"sample",
    ///     "lastname":"test",
    ///     "phone":"+919876598765"
    ///     }
    ///     ],
    ///     "tag":"ALPHA"
    ///     }
    ///
    /// When called function fails to change cloud upload status for the specified device, server returns json error response with the error description code as like below.
    ///
    /// **Listing 3**
    ///
    ///     {
    ///        "code": 400,
    ///        "error": "invalid_params",
    ///        "error_description": "missing necessary params{\"query\":[\"vendor\"],\"body\":[],\"params\":[]}"
    ///     }
    ///
    /// - Parameters:
    ///   - id:String (Device id as String from server)
    ///   - type:Bool (Type as a Boolean value provided by user)
    ///   - callback:@escaping (Calls back changeCloudUploadStatus function to enable and disable cloud upload function)
    ///   - newDevice:Device (Here as per specified device id it changes cloud upload functionality status)
    ///   - failure:@escaping (Fails to change cloud upload status for the specified device id)
    ///   - error:Any (Error can be APIError or general error, because of missing necessary parameters)
    ///
    public func changeCloudUploadStatus(id:String, type:Bool, callback:@escaping (_ newDevice:Device)->Void, failure:@escaping (_ error:Any)->Void) -> Void {
        DeviceHttps.instance.changeCloudUploadStatus(id: id, type:type, callback: callback, failure: failure)
    }
    
    
    public func changeCloudUploadSettings(id:String, type:String, callback:@escaping (_ newDevice:Device)->Void, failure:@escaping (_ error:Any)->Void) -> Void {
        DeviceHttps.instance.changeCloudUploadSettings(id: id, type: type, callback: callback, failure: failure)
    }
    
    
    /// It calls changeChimeStatus() function to enable or disable chime for the specified device.
    /// If the user of the device is a admin then only they can change chime status.
    ///
    /// **Listing 1**
    ///
    ///     Let Let id: "585326897542157909654321", type: true
    ///     (Device id and type as boolean value are parameters to be passed)
    ///
    /// When called function successfully changes chime status for specified device, server returns successful json response as like below.
    ///
    /// **Listing 2**
    ///
    ///      {
    ///     "_id":"585326897542157909654321",
    ///     "updatedAt":"2018-04-23T13:42:19.386Z",
    ///     "createdAt":"2018-01-19T14:09:13.500Z",
    ///     "name":"test",
    ///     "mac":"1C:50:8C:9F:TE:ST",
    ///     "firmwareVersion":"2018.04.20",
    ///     "model":"SureBell-Rev-2",
    ///     "adminToken":"11616",
    ///     "vendor":"5682hbn2873hjvvo98879373",
    ///     "createdBy":"5682hbn2873hjvvo98879372",
    ///     "__v":888,
    ///     "deviceUser":"5682hbn2873hjvvo98879373",
    ///     "lastImage":"https://storage.googleapis.com/pioctave/585326897542157909654321%2FlastImage-1517208643113?GoogleAccessId=cloudstorage@masterserver-1332.iam.gserviceaccount.com&Expires=1524494539&Signature=lr1Gmn2LvkkO8leuo5XWRgB%2",
    ///     "pairedWith":[
    ///     ],
    ///     "settings":{
    ///     "chime":true,
    ///     "motionSensitivity":1,
    ///     "cloudUpload":true,
    ///     "timeZone":"+5:30",
    ///     "cloudUploadSetting":{
    ///     "ring":{
    ///     "settings":[
    ///     ],
    ///     "status":true
    ///     },
    ///     "motion":{
    ///     "settings":[
    ///     ],
    ///     "status":true
    ///     }
    ///     },
    ///     "video":{
    ///     "recording":{
    ///     "quality":1
    ///     },
    ///     "live":{
    ///     "quality":1
    ///     }
    ///     },
    ///     "audio":{
    ///     "speaker_vol":2,
    ///     "mic_gain":2
    ///     },
    ///     "motionSettings":[
    ///     ]
    ///     },
    ///     "upgradeAttempts":0,
    ///     "versionOnDevice":0,
    ///     "upgradeStatus":"UPGRADED",
    ///     "location":[
    ///     12.9741833,
    ///     77.65403609999997
    ///     ],
    ///     "invites":[
    ///     ],
    ///     "users":[
    ///     {
    ///     "_id":"543209876543210987654321",
    ///     "email":"sample@gmail.com",
    ///     "firstname":"sample",
    ///     "lastname":"test",
    ///     "phone":"+919876598765"
    ///     }
    ///     ],
    ///     "admins":[
    ///     {
    ///     "_id":"543209876543210987654321",
    ///     "email":"sample@gmail.com",
    ///     "firstname":"sample",
    ///     "lastname":"test",
    ///     "phone":"+919876598765"
    ///     }
    ///     ],
    ///     "tag":"ALPHA"
    ///     }
    ///
    /// When called function fails to change chime status as per user request, server returns json error response with the error description code as like below.
    ///
    /// **Listing 3**
    ///
    ///     {
    ///        "code": 400,
    ///        "error": "invalid_params",
    ///        "error_description": "missing necessary params{\"query\":[\"vendor\"],\"body\":[],\"params\":[]}"
    ///     }
    ///
    /// - Parameters:
    ///   - id:String (Device id as String from server)
    ///   - type:Bool (Type as Boolean value provided by user)
    ///   - callback:@escaping (Calls back changeChimeStatus function to enable or disable chime of the specified device)
    ///   - newDevice:Device (Here it takes device id to change  chime status)
    ///   - failure:@escaping (Fails to change chime status as per user request)
    ///   - error:Any (Error can be APIError or general error, because of missing necessary parameters)
    ///
    public func changeChimeStatus(id:String, type:Bool, callback:@escaping (_ newDevice:Device)->Void, failure:@escaping (_ error:Any)->Void) -> Void {
        DeviceHttps.instance.changeChimeStatus(id: id, type: type, callback: callback, failure: failure)
    }
    
    
    /// It calls motionSensitivity() function to set sensitivity level as low(0), medium(1) or high(2) as per user request for the specified device.
    /// If the user of the device is a admin then only he can change sensitivity level for his device.
    ///
    /// **Listing 1**
    ///
    ///     Let id: "585326897542157909654321", motionSensitivity:1
    ///     (Device id and motion sensitivity level as integer value are as parameters to be passed)
    ///
    /// When called function successfully sets sensitivity level for the specified device, server returns json success response as like below.
    ///
    /// **Listing 2**
    ///
    ///     {
    ///     "_id":"585326897542157909654321",
    ///     "updatedAt":"2018-04-23T16:49:14.409Z",
    ///     "createdAt":"2018-01-19T14:09:13.500Z",
    ///     "name":"test",
    ///     "mac":"1C:50:8C:9F:TE:ST",
    ///     "firmwareVersion":"2018.04.23",
    ///     "model":"SureBell-Rev-2",
    ///     "adminToken":"48821",
    ///     "vendor":"5682hbn2873hjvvo98879373",
    ///     "createdBy":"5682hbn2873hjvvo98879372",
    ///     "__v":888,
    ///     "deviceUser":"5682hbn2873hjvvo98879373",   "lastImage":"https://storage.googleapis.com/pioctave/585326897542157909654321%2FlastImage-1517208643113?GoogleAccessId=cloudstorage@masterserver-1332.iam.gserviceaccount.com&Expires=1524554337&Signature=t%2B4I3BNC1bB3kZDoZoZSJaSWPkPdK2jCB6ShbyuUuNSWDqO4bsl8R6Ii6wUG5gwS38C%2ByuyWXNDLMU9U7aLQmSt%",
    ///     "pairedWith":[
    ///     ],
    ///     "settings":{
    ///     "chime":true,
    ///     "motionSensitivity":1,
    ///     "cloudUpload":true,
    ///     "timeZone":"+5:30",
    ///     "cloudUploadSetting":{
    ///     "ring":{
    ///     "settings":[
    ///     ],
    ///     "status":true
    ///     },
    ///     "motion":{
    ///     "settings":[
    ///     ],
    ///     "status":true
    ///     }
    ///     },
    ///     "video":{
    ///     "recording":{
    ///     "quality":1
    ///     },
    ///     "live":{
    ///     "quality":1
    ///     }
    ///     },
    ///     "audio":{
    ///     "speaker_vol":2,
    ///     "mic_gain":2
    ///     },
    ///     "motionSettings":[
    ///     ]
    ///     },
    ///     "upgradeAttempts":0,
    ///     "versionOnDevice":0,
    ///     "upgradeStatus":"UPGRADED",
    ///     "location":[
    ///     12.9741833,
    ///     77.65403609999997
    ///     ],
    ///     "invites":[
    ///     ],
    ///     "users":[
    ///     {
    ///     "_id":"543209876543210987654321",
    ///     "email":"sample@gmail.com",
    ///     "firstname":"sample",
    ///     "lastname":"test",
    ///     "phone":"+919876598765"
    ///     }
    ///     ],
    ///     "admins":[
    ///     {
    ///     "_id":"543209876543210987654321",
    ///     "email":"sample@gmail.com",
    ///     "firstname":"sample",
    ///     "lastname":"test",
    ///     "phone":"+919876598765"
    ///     }
    ///     ],
    ///     "tag":"ALPHA"
    ///     }
    ///
    /// When called function fails to set sensitivity level for the specififed device, server returns json error response with the error description code as like below.
    ///
    /// **Listing 3**
    ///
    ///     {
    ///        "code": 400,
    ///        "error": "invalid_params",
    ///        "error_description": "missing necessary params{\"query\":[\"vendor\"],\"body\":[],\"params\":[]}"
    ///     }
    ///
    /// - Parameters:
    ///   - id:String (Device id as String from server)
    ///   - motionSensitivity:Int (Motion Sensitivity level as a Integer value provided by user)
    ///   - callback:@escaping (Calls back motionSensitivity function to set sensitivity level for specified device to capture the motions as per the specification)
    ///   - newDevice:Device (Here it takes device id as parameter to set sensitivity level)
    ///   - failure:@escaping (Fails to set sensitivity level for the specified device id)
    ///   - error:Any (Error can be APIError or general error, which is because of missing necessary parameters)
    ///
    public func motionSensitivity(id:String, motionSensitivity:Int, callback:@escaping (_ newDevice:Device)->Void, failure:@escaping (_ error:Any)->Void) -> Void {
        DeviceHttps.instance.motionSenstivity(id: id, motionSensitivity: motionSensitivity, callback: callback, failure: failure)
    }
    
    
    /// It calls audioSettings() function, to set user prefered volume to either mic or speaker as low(0), medium(1) and high(2).
    /// If the user of the device is a admin then only he can set volume for mic and speaker.
    ///
    /// **Listing 1**
    ///
    ///     Let id: "585326897542157909654321", micGain: 2, speakerVol: 2
    ///     (Device id, micGain and speaker volume are as parameters to be passed)
    ///
    /// When called function successfully updates audioSettings, server returns successful json response as like below.
    ///
    /// **Listing 2**
    ///
    ///     {
    ///     "_id":"585326897542157909654321",
    ///     "updatedAt":"2018-04-24T06:23:22.398Z",
    ///     "createdAt":"2018-01-19T14:09:13.500Z",
    ///     "name":"test",
    ///     "mac":"1C:50:8C:9F:TE:ST",
    ///     "firmwareVersion":"2018.04.23",
    ///     "model":"SureBell-Rev-2",
    ///     "adminToken":"48821",
    ///     "vendor":"5682hbn2873hjvvo98879373",
    ///     "createdBy":"5682hbn2873hjvvo98879372",
    ///     "__v":888,
    ///     "deviceUser":"5682hbn2873hjvvo98879373",   "lastImage":"https://storage.googleapis.com/pioctave/585326897542157909654321%2FlastImage-1517208643113?GoogleAccessId=cloudstorage@masterserver-1332.iam.gserviceaccount.com&Expires=1524554602&Signature=WXoVE3hpWhz9VtlZ6Q5%2B7%2FdGJ7MZl%2BrXIMJu2ZHOJY%2BYEpcAo2UCYhOqsiqRice9%2F%2Fy3hqNarYDjc1cHBaJ%",
    ///     "pairedWith":[
    ///     ],
    ///     "settings":{
    ///     "chime":true,
    ///     "motionSensitivity":1,
    ///     "cloudUpload":true,
    ///     "timeZone":"+5:30",
    ///     "cloudUploadSetting":{
    ///     "ring":{
    ///     "settings":[
    ///     ],
    ///     "status":true
    ///     },
    ///     "motion":{
    ///     "settings":[
    ///     ],
    ///     "status":true
    ///     }
    ///     },
    ///     "video":{
    ///     "recording":{
    ///     "quality":1
    ///     },
    ///     "live":{
    ///     "quality":1
    ///     }
    ///     },
    ///     "audio":{
    ///     "mic_gain":2,
    ///     "speaker_vol":2
    ///     },
    ///     "motionSettings":[
    ///     ]
    ///     },
    ///     "upgradeAttempts":0,
    ///     "versionOnDevice":0,
    ///     "upgradeStatus":"UPGRADED",
    ///     "location":[
    ///     12.9741833,
    ///     77.65403609999997
    ///     ],
    ///     "invites":[
    ///     ],
    ///     "users":[
    ///     {
    ///     "_id":"543209876543210987654321",
    ///     "email":"sample@gmail.com",
    ///     "firstname":"sample",
    ///     "lastname":"test",
    ///     "phone":"+919876598765"
    ///     }
    ///     ],
    ///     "admins":[
    ///     {
    ///     "_id":"543209876543210987654321",
    ///     "email":"sample@gmail.com",
    ///     "firstname":"sample",
    ///     "lastname":"test",
    ///     "phone":"+919876598765"
    ///     }
    ///     ],
    ///     "tag":"ALPHA"
    ///     }
    ///
    /// When called function fails to update audioSettings, server returns json error response with the error description code as like below.
    ///
    /// **Listing 3**
    ///
    ///     {
    ///        "code": 400,
    ///        "error": "invalid_params",
    ///        "error_description": "missing necessary params{\"query\":[\"vendor\"],\"body\":[],\"params\":[]}"
    ///     }
    ///
    /// - Parameters:
    ///   - id:String (Device id as String from server)
    ///   - audio:Int (Audio(Mic/Speaker) as Integer value provided by user)
    ///   - callback:@escaping (Calls back audioSettings function to set mic and speaker volume for the specified device)
    ///   - newDevice:Device (Here it takes device id to set mic or speaker volume for the specified device)
    ///   - failure:@escaping (Fails to set mic or speaker volume to the specified device)
    ///   - error:Any (Error can be APIError or general error, which is because of missing necessary parameters)
    ///
    public func audioSettings(id:String, audio: Audio, callback:@escaping (_ newDevice:Device)->Void, failure:@escaping (_ error:Any)->Void) -> Void {
        DeviceHttps.instance.audioSettings(id: id, audio: audio, callback: callback, failure: failure)
    }
    
    
    /// It calls videoSettings() function to set quality of the live or recording video as per user request as low(0), medium(1) or high(2).
    /// If the user of the device is a admin then only they can set quality of the recording or live video.
    ///
    /// **Listing 1**
    ///
    ///     Let id: "585326897542157909654321", recording: 2,live: 2
    ///     (Device id, recording and live integer values are as parameters to be passed)
    ///
    /// When called function successfully sets recording or live video quality, server returns successful json response as like below.
    ///
    /// **Listing 2**
    ///
    ///     {
    ///     "_id":"585326897542157909654321",
    ///     "updatedAt":"2018-04-26T12:08:59.577Z",
    ///     "createdAt":"2018-01-19T14:09:13.500Z",
    ///     "name":"test",
    ///     "mac":"1C:50:8C:9F:TE:ST",
    ///     "firmwareVersion":"2018.04.25",
    ///     "model":"SureBell-Rev-2",
    ///     "adminToken":"72049",
    ///     "vendor":"5682hbn2873hjvvo98879373",
    ///     "createdBy":"5682hbn2873hjvvo98879372",
    ///     "__v":891,
    ///     "deviceUser":"5682hbn2873hjvvo98879373",   "lastImage":"https://storage.googleapis.com/pioctave/585326897542157909654321%2FlastImage-1517208643113?GoogleAccessId=cloudstorage@masterserver-1332.iam.gserviceaccount.com&Expires=1524748140&Signature=mQCKc96AMuZu6eorclp5snQO%2",
    ///     "pairedWith":[
    ///     ],
    ///     "settings":{
    ///     "chime":true,
    ///     "motionSensitivity":1,
    ///     "cloudUpload":true,
    ///     "timeZone":"+5.30",
    ///     "cloudUploadSetting":{
    ///     "ring":{
    ///     "settings":[
    ///     ],
    ///     "status":true
    ///     },
    ///     "motion":{
    ///     "settings":[
    ///     ],
    ///     "status":true
    ///     }
    ///     },
    ///     "video":{
    ///     "live":{
    ///     "quality":2
    ///     },
    ///     "recording":{
    ///     "quality":2
    ///     }
    ///     },
    ///     "audio":{
    ///     "speaker_vol":2,
    ///     "mic_gain":2
    ///     },
    ///     "motionSettings":[
    ///     {
    ///     "resolution":{
    ///     "x":1920,
    ///     "y":1080
    ///     },
    ///     "bounds":[
    ///     {
    ///     "x":2561,
    ///     "y":650,
    ///     "_id":"5ae071d026ea29001a0a8203"
    ///     },
    ///     {
    ///     "x":5236,
    ///     "y":3191,
    ///     "_id":"5ae071d026ea29001a0a8202"
    ///     }
    ///     ],
    ///     "anchors":[
    ///     {
    ///     "x":2999,
    ///     "y":1872,
    ///     "type":1,
    ///     "_id":"5ae071d026ea29001a0a8211"
    ///     },
    ///     {
    ///     "x":3431,
    ///     "y":1540,
    ///     "type":0,
    ///     "_id":"5ae071d026ea29001a0a8210"
    ///     },
    ///     {
    ///     "x":3739,
    ///     "y":1097,
    ///     "type":1,
    ///     "_id":"5ae071d026ea29001a0a820f"
    ///     },
    ///     {
    ///     "x":4044,
    ///     "y":650,
    ///     "type":0,
    ///     "_id":"5ae071d026ea29001a0a820e"
    ///     },
    ///     {
    ///     "x":4588,
    ///     "y":915,
    ///     "type":1,
    ///     "_id":"5ae071d026ea29001a0a820d"
    ///     },
    ///     {
    ///     "x":5126,
    ///     "y":1180,
    ///     "type":0,
    ///     "_id":"5ae071d026ea29001a0a820c"
    ///     },
    ///     {
    ///     "x":5175,
    ///     "y":1635,
    ///     "type":1,
    ///     "_id":"5ae071d026ea29001a0a820b"
    ///     },
    ///     {
    ///     "x":5236,
    ///     "y":2090,
    ///     "type":0,
    ///     "_id":"5ae071d026ea29001a0a820a"
    ///     },
    ///     {
    ///     "x":5135,
    ///     "y":2520,
    ///     "type":1,
    ///     "_id":"5ae071d026ea29001a0a8209"
    ///     },
    ///     {
    ///     "x":5028,
    ///     "y":2937,
    ///     "type":0,
    ///     "_id":"5ae071d026ea29001a0a8208"
    ///     },
    ///     {
    ///     "x":4467,
    ///     "y":3064,
    ///     "type":1,
    ///     "_id":"5ae071d026ea29001a0a8207"
    ///     },
    ///     {
    ///     "x":3909,
    ///     "y":3191,
    ///     "type":0,
    ///     "_id":"5ae071d026ea29001a0a8206"
    ///     },
    ///     {
    ///     "x":3172,
    ///     "y":2888,
    ///     "type":1,
    ///     "_id":"5ae071d026ea29001a0a8205"
    ///     },
    ///     {
    ///     "x":2561,
    ///     "y":2191,
    ///     "type":0,
    ///     "_id":"5ae071d026ea29001a0a8204"
    ///     }
    ///     ]
    ///     },
    ///     {
    ///     "resolution":{
    ///     "x":1920,
    ///     "y":1080
    ///     },
    ///     "bounds":[
    ///     {
    ///     "x":546,
    ///     "y":745,
    ///     "_id":"5ae071d026ea29001a0a81f9"
    ///     },
    ///     {
    ///     "x":690,
    ///     "y":889,
    ///     "_id":"5ae071d026ea29001a0a81f8"
    ///     }
    ///     ],
    ///     "anchors":[
    ///     {
    ///     "x":546,
    ///     "y":745,
    ///     "type":0,
    ///     "_id":"5ae071d026ea29001a0a8201"
    ///     },
    ///     {
    ///     "x":618,
    ///     "y":745,
    ///     "type":1,
    ///     "_id":"5ae071d026ea29001a0a8200"
    ///     },
    ///     {
    ///     "x":690,
    ///     "y":745,
    ///     "type":0,
    ///     "_id":"5ae071d026ea29001a0a81ff"
    ///     },
    ///     {
    ///     "x":690,
    ///     "y":817,
    ///     "type":1,
    ///     "_id":"5ae071d026ea29001a0a81fe"
    ///     },
    ///     {
    ///     "x":690,
    ///     "y":889,
    ///     "type":0,
    ///     "_id":"5ae071d026ea29001a0a81fd"
    ///     },
    ///     {
    ///     "x":618,
    ///     "y":889,
    ///     "type":1,
    ///     "_id":"5ae071d026ea29001a0a81fc"
    ///     },
    ///     {
    ///     "x":546,
    ///     "y":889,
    ///     "type":0,
    ///     "_id":"5ae071d026ea29001a0a81fb"
    ///     },
    ///     {
    ///     "x":546,
    ///     "y":817,
    ///     "type":1,
    ///     "_id":"5ae071d026ea29001a0a81fa"
    ///     }
    ///     ]
    ///     },
    ///     {
    ///     "resolution":{
    ///     "x":1920,
    ///     "y":1080
    ///     },
    ///     "bounds":[
    ///     {
    ///     "x":1070,
    ///     "y":734,
    ///     "_id":"5ae071d026ea29001a0a81ef"
    ///     },
    ///     {
    ///     "x":1214,
    ///     "y":878,
    ///     "_id":"5ae071d026ea29001a0a81ee"
    ///     }
    ///     ],
    ///     "anchors":[
    ///     {
    ///     "x":1070,
    ///     "y":734,
    ///     "type":0,
    ///     "_id":"5ae071d026ea29001a0a81f7"
    ///     },
    ///     {
    ///     "x":1142,
    ///     "y":734,
    ///     "type":1,
    ///     "_id":"5ae071d026ea29001a0a81f6"
    ///     },
    ///     {
    ///     "x":1214,
    ///     "y":734,
    ///     "type":0,
    ///     "_id":"5ae071d026ea29001a0a81f5"
    ///     },
    ///     {
    ///     "x":1214,
    ///     "y":806,
    ///     "type":1,
    ///     "_id":"5ae071d026ea29001a0a81f4"
    ///     },
    ///     {
    ///     "x":1214,
    ///     "y":878,
    ///     "type":0,
    ///     "_id":"5ae071d026ea29001a0a81f3"
    ///     },
    ///     {
    ///     "x":1142,
    ///     "y":878,
    ///     "type":1,
    ///     "_id":"5ae071d026ea29001a0a81f2"
    ///     },
    ///     {
    ///     "x":1070,
    ///     "y":878,
    ///     "type":0,
    ///     "_id":"5ae071d026ea29001a0a81f1"
    ///     },
    ///     {
    ///     "x":1070,
    ///     "y":806,
    ///     "type":1,
    ///     "_id":"5ae071d026ea29001a0a81f0"
    ///     }
    ///     ]
    ///     },
    ///     {
    ///     "resolution":{
    ///     "x":1920,
    ///     "y":1080
    ///     },
    ///     "bounds":[
    ///     {
    ///     "x":807,
    ///     "y":365,
    ///     "_id":"5ae071d026ea29001a0a81e5"
    ///     },
    ///     {
    ///     "x":1026,
    ///     "y":568,
    ///     "_id":"5ae071d026ea29001a0a81e4"
    ///     }
    ///     ],
    ///     "anchors":[
    ///     {
    ///     "x":807,
    ///     "y":365,
    ///     "type":0,
    ///     "_id":"5ae071d026ea29001a0a81ed"
    ///     },
    ///     {
    ///     "x":917,
    ///     "y":373,
    ///     "type":1,
    ///     "_id":"5ae071d026ea29001a0a81ec"
    ///     },
    ///     {
    ///     "x":1026,
    ///     "y":382,
    ///     "type":0,
    ///     "_id":"5ae071d026ea29001a0a81eb"
    ///     },
    ///     {
    ///     "x":1021,
    ///     "y":475,
    ///     "type":1,
    ///     "_id":"5ae071d026ea29001a0a81ea"
    ///     },
    ///     {
    ///     "x":1015,
    ///     "y":567,
    ///     "type":0,
    ///     "_id":"5ae071d026ea29001a0a81e9"
    ///     },
    ///     {
    ///     "x":917,
    ///     "y":568,
    ///     "type":1,
    ///     "_id":"5ae071d026ea29001a0a81e8"
    ///     },
    ///     {
    ///     "x":820,
    ///     "y":568,
    ///     "type":0,
    ///     "_id":"5ae071d026ea29001a0a81e7"
    ///     },
    ///     {
    ///     "x":813,
    ///     "y":466,
    ///     "type":1,
    ///     "_id":"5ae071d026ea29001a0a81e6"
    ///     }
    ///     ]
    ///     },
    ///     {
    ///     "bounds":[
    ///     {
    ///     "x":47,
    ///     "y":387,
    ///     "_id":"5ae071d026ea29001a0a81db"
    ///     },
    ///     {
    ///     "x":447,
    ///     "y":787,
    ///     "_id":"5ae071d026ea29001a0a81da"
    ///     }
    ///     ],
    ///     "anchors":[
    ///     {
    ///     "x":47,
    ///     "y":387,
    ///     "type":0,
    ///     "_id":"5ae071d026ea29001a0a81e3"
    ///     },
    ///     {
    ///     "x":247,
    ///     "y":387,
    ///     "type":1,
    ///     "_id":"5ae071d026ea29001a0a81e2"
    ///     },
    ///     {
    ///     "x":447,
    ///     "y":387,
    ///     "type":0,
    ///     "_id":"5ae071d026ea29001a0a81e1"
    ///     },
    ///     {
    ///     "x":447,
    ///     "y":587,
    ///     "type":1,
    ///     "_id":"5ae071d026ea29001a0a81e0"
    ///     },
    ///     {
    ///     "x":447,
    ///     "y":787,
    ///     "type":0,
    ///     "_id":"5ae071d026ea29001a0a81df"
    ///     },
    ///     {
    ///     "x":247,
    ///     "y":787,
    ///     "type":1,
    ///     "_id":"5ae071d026ea29001a0a81de"
    ///     },
    ///     {
    ///     "x":47,
    ///     "y":787,
    ///     "type":0,
    ///     "_id":"5ae071d026ea29001a0a81dd"
    ///     },
    ///     {
    ///     "x":47,
    ///     "y":587,
    ///     "type":1,
    ///     "_id":"5ae071d026ea29001a0a81dc"
    ///     }
    ///     ]
    ///     }
    ///     ]
    ///     },
    ///     "upgradeAttempts":0,
    ///     "versionOnDevice":0,
    ///     "upgradeStatus":"UPGRADED",
    ///     "location":[
    ///     12.9741833,
    ///     77.65403609999997
    ///     ],
    ///     "invites":[
    ///     ],
    ///     "users":[
    ///     {
    ///     "_id":"543209876543210987654321",
    ///     "email":"sample@gmail.com",
    ///     "firstname":"sample",
    ///     "lastname":"test",
    ///     "phone":"+919876598765"
    ///     }
    ///     ],
    ///     "admins":[
    ///     {
    ///     "_id":"543209876543210987654321",
    ///     "email":"sample@gmail.com",
    ///     "firstname":"sample",
    ///     "lastname":"test",
    ///     "phone":"+919876598765"
    ///     }
    ///     ],
    ///     "tag":"ALPHA"
    ///     }
    ///
    /// When called function fails to update videoSettings for the specified device, server returns json error response with the error description code as like below.
    ///
    /// **Listing 3**
    ///
    ///     {
    ///        "code": 400,
    ///        "error": "invalid_params",
    ///        "error_description": "missing necessary params{\"query\":[\"vendor\"],\"body\":[],\"params\":[]}"
    ///     }
    ///
    /// - Parameters:
    ///   - id:String (Device id as a String from server)
    ///   - recording:Int (Recording as a Integer Value provided by user)
    ///   - live:Int (Live as a Integer Value provided by user)
    ///   - callback:@escaping (Calls back videoSettings function to set quality of the recording/viewing video)
    ///   - newDevice:Device (Here it takes device id to set recording or live video quality)
    ///   - failure:@escaping (Fails to set recording or live video quality)
    ///   - error:Any (Error can be APIError or general error, which is because of missing necessary parameters)
    ///
    public func videoSettings(id:String, recording: Recording, live: Live, callback:@escaping (_ newDevice:Device)->Void, failure:@escaping (_ error:Any)->Void) -> Void {
        DeviceHttps.instance.videoSettings(id: id, recording: recording, live: live, callback: callback, failure: failure)
    }
    
    
    /// User can also set region of interest for the particular device to avoid unnecessary generation of events by drawing a region in motion settings using motionSetting() function.
    /// If the user of the device is a admin then only they can set regoin of intereset for the specified device.
    ///
    /// **Listing 1**
    ///
    ///     Let id: "543209876543210987654321", motionSettings: json
    ///     (Device id and motionSettings as a json object asre the parameters to be passed)
    ///
    /// When called function successfully sets regoin of interest as per user request, server returns successful json object as like below.
    ///
    /// **Listing 2**
    ///
    ///     {
    ///     "_id":"59f73899e128e7001abc2811",
    ///     "updatedAt":"2018-04-27T07:12:36.368Z",
    ///     "createdAt":"2018-01-19T00:43:17.688Z",
    ///     "vendor":"5682hbn2873hjvvo98879373",
    ///     "createdBy":"5a61151bb3bb660010dfc3f5",
    ///     "mac":"1C:50:7C:C2:TE:ST",
    ///     "name":"Test2",
    ///     "firmwareVersion":"1.2.21",
    ///     "adminToken":"86113",
    ///     "model":"SureBell-Rev-3",
    ///     "__v":6,
    ///     "deviceUser":"5a613f259af05a0010c6087b",
    ///     "lastImage":"https://storage.googleapis.com/pioctave/59f73899e128e7001abc2811%2FlastImage-1513772639130?GoogleAccessId=cloudstorage@masterserver-1332.iam.gserviceaccount.com&Expires=1524818426&Signature=yWeKJ%2BnuLVvbJvfc01hx5Wj3pmVj7Uk2v%2",
    ///     "pairedWith":[
    ///     ],
    ///     "settings":{
    ///     "chime":false,
    ///     "motionSensitivity":1,
    ///     "cloudUploadSetting":{
    ///     "ring":{
    ///     "settings":[
    ///     ],
    ///     "status":true
    ///     },
    ///     "motion":{
    ///     "settings":[
    ///     ],
    ///     "status":true
    ///     }
    ///     },
    ///     "video":{
    ///     "recording":{
    ///     "quality":3
    ///     },
    ///     "live":{
    ///     "quality":3
    ///     }
    ///     },
    ///     "audio":{
    ///     "speaker_vol":3,
    ///     "mic_gain":3
    ///     },
    ///     "motionSettings":[
    ///     {
    ///     "resolution":{
    ///     "y":375,
    ///     "x":812
    ///     },
    ///     "bounds":[
    ///     {
    ///     "y":150,
    ///     "x":150,
    ///     "_id":"5ae2cd641fa110001a870935"
    ///     },
    ///     {
    ///     "y":300,
    ///     "x":300,
    ///     "_id":"5ae2cd641fa110001a870934"
    ///     }
    ///     ],
    ///     "anchors":[
    ///     {
    ///     "type":0,
    ///     "y":150,
    ///     "x":150,
    ///     "_id":"5ae2cd641fa110001a870939"
    ///     },
    ///     {
    ///     "type":0,
    ///     "y":150,
    ///     "x":300,
    ///     "_id":"5ae2cd641fa110001a870938"
    ///     },
    ///     {
    ///     "type":0,
    ///     "y":300,
    ///     "x":300,
    ///     "_id":"5ae2cd641fa110001a870937"
    ///     },
    ///     {
    ///     "type":0,
    ///     "y":300,
    ///     "x":150,
    ///     "_id":"5ae2cd641fa110001a870936"
    ///     }
    ///     ]
    ///     }
    ///     ]
    ///     },
    ///     "upgradeAttempts":4,
    ///     "versionOnDevice":0,
    ///     "upgradeStatus":"UPGRADED",
    ///     "location":[
    ///     12.9620946,
    ///     77.6723834
    ///     ],
    ///     "invites":[
    ///     ],
    ///     "users":[
    ///     {
    ///     "_id":"5a61151bb3bb660010dfc3f5",
    ///     "firstname":"sample1",
    ///     "lastname":"test1",
    ///     "email":"sample1@gmail.com",
    ///     "phone":"+919876598765"
    ///     }
    ///     ],
    ///     "admins":[
    ///     {
    ///     "_id":"5a61151bb3bb660010dfc3f5",
    ///     "firstname":"sample1",
    ///     "lastname":"test1",
    ///     "email":"sample1@gmail.com",
    ///     "phone":"+919876598765"
    ///     }
    ///     ],
    ///     "tag":"ALPHA"
    ///     }
    ///
    /// When called function fails to updates region of interest for the specified device, server returns error json response with the error description code as like below.
    ///
    /// **Listing 3**
    ///
    ///     {
    ///        "code": 400,
    ///        "error": "invalid_params",
    ///        "error_description": "missing necessary params{\"query\":[\"vendor\"],\"body\":[],\"params\":[]}"
    ///     }
    ///
    /// - Parameters:
    ///   - id:String (Device id as String from server)
    ///   - motionSettings:Object (MotionSettings is a json object as per user prefered region of ineterest)
    ///   - callback:@escaping (Calls back motionSettings function to set region of interest as per user request)
    ///   - newDevice:Device (Here it takes device id to set region of interest by using json object as per user preference)
    ///   - failure:@esacping (Fails to set region of interest for the specified device as per user request)
    ///   - error:Any (Error can be APIError or general error, because of missing necessary parameters)
    ///
    public func motionSettings(id:String, motionSettings: Parameters, callback:@escaping (_ newDevice:Device)->Void, failure:@escaping (_ error:Any)->Void) -> Void {
        DeviceHttps.instance.motionSettings(id: id, motionSettings: motionSettings, callback: callback, failure: failure)
    }
    
    
    /// It calls changeTimeZone() function to set timings for the specified device as per user and device installed location.
    /// If the user of the particular device is a admin then only they can set timins for their device.
    ///
    /// **Listing 1**
    ///
    ///     Let id: "585326897542157909654321", timeZone: "+5.30"
    ///     (Device id and country standard timings are as the parameters to be passed)
    ///
    /// When called function successfully updates timings for the specified device as per the location, server returns successful json response as like below.
    ///
    /// **Listing 2**
    ///
    ///     {
    ///     "_id":"585326897542157909654321",
    ///     "updatedAt":"2018-04-24T08:02:44.782Z",
    ///     "createdAt":"2018-01-19T14:09:13.500Z",
    ///     "name":"test",
    ///     "mac":"1C:50:8C:9F:TE:ST",
    ///     "firmwareVersion":"2018.04.23",
    ///     "model":"SureBell-Rev-2",
    ///     "adminToken":"48821",
    ///     "vendor":"5682hbn2873hjvvo98879373",
    ///     "createdBy":"5682hbn2873hjvvo98879372",
    ///     "__v":888,
    ///     "deviceUser":"5682hbn2873hjvvo98879373",   "lastImage":"https://storage.googleapis.com/pioctave/585326897542157909654321%2FlastImage-1517208643113?GoogleAccessId=cloudstorage@masterserver-1332.iam.gserviceaccount.com&Expires=1524560565&Signature=bs%2B7hA5hYcqpHQnfL9BQMe4NOpUQ0gKQEdwCTeMsmfjSIq142gW0UsJYH9PBI7l6vA%2ByL7vBitpPcqbypv8JG5xL1TG94cd1itqRrNajuL3VnAma6JOawwB63%",
    ///     "pairedWith":[
    ///     ],
    ///     "settings":{
    ///     "chime":true,
    ///     "motionSensitivity":1,
    ///     "cloudUpload":false,
    ///     "timeZone":"+5.30",
    ///     "cloudUploadSetting":{
    ///     "ring":{
    ///     "settings":[
    ///     ],
    ///     "status":true
    ///     },
    ///     "motion":{
    ///     "settings":[
    ///     ],
    ///     "status":true
    ///     }
    ///     },
    ///     "video":{
    ///     "recording":{
    ///     "quality":1
    ///     },
    ///     "live":{
    ///     "quality":1
    ///     }
    ///     },
    ///     "audio":{
    ///     "speaker_vol":2,
    ///     "mic_gain":2
    ///     },
    ///     "motionSettings":[
    ///     ]
    ///     },
    ///     "upgradeAttempts":0,
    ///     "versionOnDevice":0,
    ///     "upgradeStatus":"UPGRADED",
    ///     "location":[
    ///     12.9741833,
    ///     77.65403609999997
    ///     ],
    ///     "invites":[
    ///     ],
    ///     "users":[
    ///     {
    ///     "_id":"543209876543210987654321",
    ///     "email":"sample@gmail.com",
    ///     "firstname":"sample",
    ///     "lastname":"test",
    ///     "phone":"+919876598765"
    ///     }
    ///     ],
    ///     "admins":[
    ///     {
    ///     "_id":"543209876543210987654321",
    ///     "email":"sample@gmail.com",
    ///     "firstname":"sample",
    ///     "lastname":"test",
    ///     "phone":"+919876598765"
    ///     }
    ///     ],
    ///     "tag":"ALPHA"
    ///     }
    ///
    /// When called function fails to update timings for the specified device as per user location, server returns json error response with the error description code as like below.
    ///
    /// **Listing 3**
    ///
    ///     {
    ///        "code": 400,
    ///        "error": "invalid_params",
    ///        "error_description": "missing necessary params{\"query\":[\"vendor\"],\"body\":[],\"params\":[]}"
    ///     }
    ///
    /// - Parameters:
    ///   - id:String (Device id as a String from server)
    ///   - timeZone:String (Time Zone as per location set by user)
    ///   - callback:@escaping (Calls back changeTimeZone function to set timings as per user location)
    ///   - newDevice:Device (Here it takes device id to set timings as per the user location)
    ///   - failure:@escaping (Fails to set timings as per the location for the specified device)
    ///   - error:Any (Error can be APIError or general error, because of missing necessary parameters)
    ///
    public func changeTimeZone(id:String, timeZone: String, callback:@escaping (_ newDevice:Device)->Void, failure:@escaping (_ error:Any)->Void) -> Void {
        DeviceHttps.instance.changeTimeZone(id: id, timeZone: timeZone, callback: callback, failure: failure)
    }
    
    public func changeDeviceName(id: String, name: String, callback:@escaping (_ newDevice:Device)->Void, failure:@escaping (_ error:Any)->Void) -> Void {
        DeviceHttps.instance.changeDeviceName(id: id, name: name, callback: callback, failure: failure)
    }
    
    public func changeBulkEventStatus(ids: [String], status: String, callback:@escaping (_ newDevice:StatusResponse)->Void, failure:@escaping (_ error:Any)->Void) -> Void {
        DeviceHttps.instance.changeBulkEventStatus(ids: ids, status: status, callback: callback, failure: failure)
    }
    
    func devicePair(_ url:String,httpMethod:HTTPMethod,params :Parameters,status:@escaping (_ statuscode:Int) -> Void){
        let header = ["Content-type" : "application/json; charset=utf-8"]
        Alamofire.request(url, method: httpMethod, parameters: params, encoding: JSONEncoding.default, headers: header).responseJSON {
            response in
            print("DEVICE PAIRING RESPONSE \(response.response)")
            if let statusCode = response.response?.statusCode{
                status(statusCode)
            }else{
                status(1001)
            }
        }
    }

    
    public func pairNewDevice(accessToken:String,ipAddress:String, channel: String,ssid: String,encryption: String,password: String,security: String,location: [Double], chime:Bool, name: String,status:@escaping (_ statuscode:Int) -> Void) {
        //TODO: Supply access token
        devicePair("http://\(ipAddress)/fcgi-bin/v1/pair", httpMethod: .post, params: ["channel":"\(channel)","ssid":"\(ssid)","password":"\(password)","security":"\(security)","encryption":"\(encryption)","name":name,"location":location,"access_token":accessToken,"chime":chime], status: status)
    }

    public func changeDeviceWifi(ipAddress:String, channel: String,ssid: String,encryption: String,password: String,security: String, adminToken: String, isSecondary: Bool,status:@escaping (_ statuscode:Int) -> Void) {
        //TODO: Supply access token
        devicePair("http://\(ipAddress)/fcgi-bin/v1/update-wifi", httpMethod: .post, params: ["channel":"\(channel)","ssid":"\(ssid)","password":"\(password)","security":"\(security)","encryption":"\(encryption)","secondary":isSecondary,"adminToken":adminToken], status: status)
    }
    
    
    public func setChime(ipAddress:String,isChimeEnabled:Bool, user_id:String,status:@escaping (_ statuscode:Int) -> Void) {
        //TODO: Supply access token
        devicePair("http://\(ipAddress)/fcgi-bin/v1/setchime", httpMethod: .put, params: ["chime":"\(isChimeEnabled)","user":"\(user_id)"], status: status)
    }
    
    public func getSSIDList(ipAddress:String,callback:@escaping (_ statuscode:[SSID]) -> Void, _ failure: @escaping (Any) -> Void) {
        DeviceHttps.instance.getSSIDList(ipAddress: ipAddress, callback: callback, failure)
    }
    
    public func getPingResponse(ipAddress:String,callback:@escaping (_ statuscode:Ping) -> Void, _ failure: @escaping (Any) -> Void) {
        DeviceHttps.instance.getPingResponse(ipAddress: ipAddress, callback: callback, failure)
    }

}

