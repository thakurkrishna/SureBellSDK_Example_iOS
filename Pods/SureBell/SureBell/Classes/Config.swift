//
//  Config.swift
//  sureBellFramework-iOS
//
//  Created by PiOctave on 16/04/18.
//

//    Copyright (C) 2016-2018 PiOctave Solutions (Pvt) Ltd. (http://www.pioctave.com/)

//    All information contained herein is, and remains
//    the property of PiOctave Solutions (Pvt) Ltd and its suppliers
//    if any.  The intellectual and technical concepts contained
//    herein are proprietary to PiOctave Solutions (Pvt) Ltd
//    and are protected by trade secret or copyright law.
//    Dissemination of this information or reproduction of this material
//    is strictly forbidden unless prior written permission is obtained
//    from PiOctave Solutions (Pvt) Ltd.

//    THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
//    ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
//    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
//    ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
//    FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
//    DAMAGES. HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
//    STRICT LIABILITY, OR TORT ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
//    EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


import Foundation


/// Before initiating a video or live call user has to get list of values from turn server. This SessionConfig class is an instance of Device module
/// where it initializes the session, with that it gets the list of **turn** values from the server to start the session.
///
public class SessionConfig {
    public var isInitiator: Bool?
    public var turn = [TurnConfig]()
    public var streams: StreamsConfig?
}


public struct TurnConfig {
    public init(url: String, username: String, credential: String) {
        self.url = url
        self.username = username
        self.credential = credential
    }
    public var url: String
    public var username: String
    public var credential: String
    
}


public struct StreamsConfig {
    public init(audio: Bool, video: Bool, media: Bool) {
        self.audio = audio
        self.video = video
        self.media = media
    }
    public var audio: Bool?
    public var video: Bool?
    public var media: Bool?
}


/// <#Description#>
public class VideoConfig {
    var container: VideoLayoutParams
    var local: VideoLayoutParams?
    var camera:String?
    
    init(data: AnyObject) {
        let containerParams = data.object(forKey: "containerParams")!
        let localParams = data.object(forKey: "local")
        
        self.container = VideoLayoutParams(data: containerParams as AnyObject)
        
        if localParams != nil {
            self.local = VideoLayoutParams(data: localParams! as AnyObject)
        }
    }
}


class VideoLayoutParams {
    var x, y, width, height: Int
    
    init(x: Int, y: Int, width: Int, height: Int) {
        self.x = x
        self.y = y
        self.width = width
        self.height = height
    }
    
    init(data: AnyObject) {
        let position: [AnyObject] = data.object(forKey: "position")! as! [AnyObject]
        self.x = position[0] as! Int
        self.y = position[1] as! Int
        
        let size: [AnyObject] = data.object(forKey: "size")! as! [AnyObject]
        self.width = size[0] as! Int
        self.height = size[1] as! Int
    }
}
