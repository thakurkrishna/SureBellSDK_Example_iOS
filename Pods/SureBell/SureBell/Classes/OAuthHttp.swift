//
//  OAuthHttp.swift
//  sureBellFramework-iOS
//
//  Created by PiOctave on 17/04/18.
//

//    Copyright (C) 2016-2018 PiOctave Solutions (Pvt) Ltd. (http://www.pioctave.com/)

//    All information contained herein is, and remains
//    the property of PiOctave Solutions (Pvt) Ltd and its suppliers
//    if any.  The intellectual and technical concepts contained
//    herein are proprietary to PiOctave Solutions (Pvt) Ltd
//    and are protected by trade secret or copyright law.
//    Dissemination of this information or reproduction of this material
//    is strictly forbidden unless prior written permission is obtained
//    from PiOctave Solutions (Pvt) Ltd.

//    THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
//    ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
//    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
//    ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
//    FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
//    DAMAGES. HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
//    STRICT LIABILITY, OR TORT ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
//    EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


import Foundation
import Alamofire
import AlamofireObjectMapper
import ObjectMapper

class OAuthHttp{
    
    static var instance = OAuthHttp()
    private init(){
        
    }
    
    enum Router: URLRequestConvertible {
        static let baseURLString = OAuthConst.BASE_URL_STRING
        
        case getToken(userName:String, password:String, basicOAuth:String)
        
        var method: HTTPMethod {
            switch self {
            case .getToken:
                return .post
            }
        }
        
        var path: String {
            switch self {
            case .getToken:
                return "/oauth/token"
            }
        }
        
        func asURLRequest() throws -> URLRequest {
            let url = try Router.baseURLString.asURL()
            var urlRequest = URLRequest(url: url.appendingPathComponent(path))
            urlRequest.httpMethod = method.rawValue
            switch self {
            case .getToken(let userName,let password,let basicOAuth):
                urlRequest.addValue(basicOAuth, forHTTPHeaderField: "Authorization")
                urlRequest = try URLEncoding.default.encode(urlRequest, with: ["grant_type":"password","username":"\(userName)","password":"\(password)"])
            }
            return urlRequest
        }
    }
    
    func getToken(userName:String, password:String, basicOAuth:String, callback:@escaping (_ token:Token)->Void, failure:@escaping (_ error:Error)->Void) -> Void {
        BaseController.instance.makeRequest(urlRequestConvertible: Router.getToken(userName: userName, password: password, basicOAuth: basicOAuth)).responseObject { (response:DataResponse<Token>) in
            switch response.result {
            case .failure(let error):
                if let apiError = Mapper<APIError>().map(JSONString:String(data: response.data!, encoding: String.Encoding.utf8)! ){
                    failure(apiError)
                }else{
                    failure(error)
                }
            case .success(let token):
                print(token)
                UserDefaults.standard.set(token.access_token, forKey: "ACCESS_TOKEN")
                UserDefaults.standard.set(token.refresh_token, forKey: "REFRESH_TOKEN")
                callback(token)
            }
        }
    }
    
    deinit {
        // BaseDebug.log("Deinitializing OAuthHttp")
    }
    
}
