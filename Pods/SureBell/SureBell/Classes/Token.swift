//
//  Token.swift
//  Alamofire
//
//  Created by PiOctave on 17/04/18.
//

//    Copyright (C) 2016-2018 PiOctave Solutions (Pvt) Ltd. (http://www.pioctave.com/)

//    All information contained herein is, and remains
//    the property of PiOctave Solutions (Pvt) Ltd and its suppliers
//    if any.  The intellectual and technical concepts contained
//    herein are proprietary to PiOctave Solutions (Pvt) Ltd
//    and are protected by trade secret or copyright law.
//    Dissemination of this information or reproduction of this material
//    is strictly forbidden unless prior written permission is obtained
//    from PiOctave Solutions (Pvt) Ltd.

//    THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
//    ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
//    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
//    ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
//    FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
//    DAMAGES. HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
//    STRICT LIABILITY, OR TORT ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
//    EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


import Foundation
import ObjectMapper

/// User Access Token Description
public class Token:Mappable {
    
    public static let instance = Token()
    
    public var token_type:String?
    public var access_token:String?
    public var expires_in:Int?
    public var refresh_token:String?
    
    required convenience public init?(map: Map) {
        self.init()
    }
    
    public func mapping(map: Map) {
        self.token_type <- map["token_type"]
        self.access_token <- map["access_token"]
        self.expires_in <- map["expires_in"]
        self.refresh_token <- map["refresh_token"]
    }
    
    /// Calls back getToken() function and generates access token for the registered user.
    ///
    /// **Listing 1**
    ///
    ///     let userName:abcd@gmail.com, password:123456 (parameters given by user to server)
    ///
    /// When called function become successful, server gives the response as like below.
    ///
    /// **Listing 2**
    ///
    ///     {
    ///     "token_type": "bearer",
    ///     "access_token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJjbGllbnRJZCI6IjU2ODNhYTMzZTU2NGQ2MTQyNWIyOWNiMSIsIm5hbWUiOiJOaXRlZXNoIEt1bWFyIiwiaWF0IjoxNTI1MjU4MDc3LCJleHAiOjE1MjUyNjE2NzcsImF1ZCI6IjVhNjBiOWRiZTQzMjlmMDAxMGFmYWEyOCIsImlzcyI6ImF1dGgucGlvY3RhdmUuY29tIiwic3ViIjoiYWNjZXNzVG9rZW4ifQ.92zrlkdH9Fvb_04P6PFolwDEolxrtSa2P94MoxahsrM",
    ///     "expires_in": 3600,
    ///     "refresh_token": "7c69097e5b262fe780d23f7888c41d484f180988"
    ///     }
    ///
    /// When called function fails server gives the response as like below.
    ///
    /// **Listing 3**
    ///
    ///     {
    ///     "code": 400,
    ///     "error": "invalid_params",
    ///     "error_description": "missing necessary params{\"query\":[],\"body\":[\"userName\",\"password\",\"grant_type\"],\"params\":[]}"
    ///     }
    ///
    /// - Parameters:
    ///   - userName:String (Username of the registered user)
    ///   - password:String (Password of the registered user name)
    ///   - callback:@escaping (Calls back getToken() function and generates access token for the registered user)
    ///   - token:Token (AccessToken generates successfully)
    ///   - failure:@escaping (If the user is not registered then the call back fails)
    ///   - error:Error (Fails to get accessToken for the specified user)
    ///
    public func getToken(userName:String, password:String, callback:@escaping (_ token:Token)->Void, failure:@escaping (_ error:Error)->Void) -> Void {
        print(OAuthConst.CLIENT_ID)
        OAuthHttp.instance.getToken(userName: userName, password: password, basicOAuth: OAuthConst.CLIENT_ID, callback: callback, failure: failure)
    }
    
    /// getAccessToken
    ///
    /// - Returns: returns accessToken
    public func getAccessToken()->String?{
        if let token = UserDefaults.standard.string(forKey: "ACCESS_TOKEN"){
            return token
        }
       return nil
    }
    
    /// refreshToken
    ///
    /// - Returns: returns refreshToken
    public func refreshToken()->String?{
        if let token = UserDefaults.standard.string(forKey: "REFRESH_TOKEN"){
            return token
        }
        return nil
    }
    
    /// deleteToken
    public func deleteToken(){
        UserDefaults.standard.removeObject(forKey: "ACCESS_TOKEN")
        UserDefaults.standard.removeObject(forKey: "REFRESH_TOKEN")
    }

}
