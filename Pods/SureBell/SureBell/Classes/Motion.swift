//
//  Motion.swift
//  sureBellFramework-iOS
//
//  Created by PiOctave on 16/04/18.
//

//    Copyright (C) 2016-2018 PiOctave Solutions (Pvt) Ltd. (http://www.pioctave.com/)

//    All information contained herein is, and remains
//    the property of PiOctave Solutions (Pvt) Ltd and its suppliers
//    if any.  The intellectual and technical concepts contained
//    herein are proprietary to PiOctave Solutions (Pvt) Ltd
//    and are protected by trade secret or copyright law.
//    Dissemination of this information or reproduction of this material
//    is strictly forbidden unless prior written permission is obtained
//    from PiOctave Solutions (Pvt) Ltd.

//    THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
//    ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
//    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
//    ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
//    FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
//    DAMAGES. HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
//    STRICT LIABILITY, OR TORT ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
//    EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


import Foundation
import ObjectMapper


/// This Motion class is an instance of Device module where parameters of this class will get maps with the JSON object.
///  It includes **settings** and **status** are as parameters.
///
public class Motion:Mappable {
    public var settings : [TimeDetails]?
    public var status : Bool?
    
    required convenience public init?(map: Map) {
        self.init()
    }
    
    public func mapping(map: Map) {
        self.settings <- map["settings"]
        self.status <- map["status"]
    }
    
}

