//
//  PointView.swift
//  SureBell
//
//  Created by PiOctave on 26/04/18.
//

//    Copyright (C) 2016-2018 PiOctave Solutions (Pvt) Ltd. (http://www.pioctave.com/)

//    All information contained herein is, and remains
//    the property of PiOctave Solutions (Pvt) Ltd and its suppliers
//    if any.  The intellectual and technical concepts contained
//    herein are proprietary to PiOctave Solutions (Pvt) Ltd
//    and are protected by trade secret or copyright law.
//    Dissemination of this information or reproduction of this material
//    is strictly forbidden unless prior written permission is obtained
//    from PiOctave Solutions (Pvt) Ltd.

//    THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
//    ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
//    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
//    ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
//    FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
//    DAMAGES. HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
//    STRICT LIABILITY, OR TORT ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
//    EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


import Foundation

class PointView: UIControl {
    enum PointType {
        case permanent
        case temp
    }
    
    var type :PointType!
    var color : UIColor!
    var touchStart:CGPoint?
    var touchEndCallback = { (pointView: PointView) -> Void in }
    var touchBeganCallback = { (pointView: PointView) -> Void in }
    
    init(pointType : PointType, pointColor: UIColor){
        type = pointType
        color = pointColor
        if type == PointType.permanent{
            super.init(frame: CGRect(origin: CGPoint.zero, size: CGSize(width: 17.0 * 2, height: 17.0 * 2)))
            self.layer.cornerRadius = 17.0
            self.layer.masksToBounds = true
            self.backgroundColor = color
            self.addTarget(self, action: #selector(self.touchDragInside(_:withEvent:)), for: .touchDragInside)
        }else{
            super.init(frame: CGRect(origin: CGPoint.zero, size: CGSize(width: 14.0 * 2, height: 14.0 * 2)))
            self.layer.cornerRadius = 14.0
            self.layer.masksToBounds = true
            self.backgroundColor = color
            self.addTarget(self, action: #selector(self.touchDragInside(_:withEvent:)), for: .touchDragInside)
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    var dragCallBack = { (pointView: PointView, touch:UITouch) -> Void in }
    
    @objc func touchDragInside(_ pointView: PointView, withEvent event: UIEvent) {
        
        for touch in event.allTouches! {
            pointView.center = (touch).location(in: superview)
            dragCallBack(self,touch)
            return
        }
        
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event);
        for touch: AnyObject in touches {
            touchBeganCallback(self)
            //MotionSettingViewController.isChangeHappened = true
            let position :CGPoint = touch.location(in: self.superview)
            let location:CGPoint = (touch as! UITouch).location(in: self.superview)
            self.touchStart = location
            print("beg", position.x , position.y)
        }
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.touchStart = nil
        let touch = touches.first
        let location : CGPoint = (touch?.location(in: self))!
        
        if #available(iOS 9.0, *) {
            if self.point(inside: self.convert(location, to: self.forLastBaselineLayout), with: nil) {
                touchEndCallback(self)
            }
        } else {
            // Fallback on earlier versions
        }
        super.touchesEnded(touches, with: event)
    }
    
}
