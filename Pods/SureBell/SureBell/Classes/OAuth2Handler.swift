//
//  OAuth2Handler.swift
//  sureBellFramework-iOS
//
//  Created by PiOctave on 17/04/18.
//

//    Copyright (C) 2016-2018 PiOctave Solutions (Pvt) Ltd. (http://www.pioctave.com/)

//    All information contained herein is, and remains
//    the property of PiOctave Solutions (Pvt) Ltd and its suppliers
//    if any.  The intellectual and technical concepts contained
//    herein are proprietary to PiOctave Solutions (Pvt) Ltd
//    and are protected by trade secret or copyright law.
//    Dissemination of this information or reproduction of this material
//    is strictly forbidden unless prior written permission is obtained
//    from PiOctave Solutions (Pvt) Ltd.

//    THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
//    ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
//    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
//    ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
//    FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
//    DAMAGES. HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
//    STRICT LIABILITY, OR TORT ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
//    EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


import Foundation
import Alamofire

class OAuth2Handler: RequestAdapter, RequestRetrier {
    
    private typealias RefreshCompletion = (_ succeeded: Bool, _ accessToken: String?, _ refreshToken: String?) -> Void
    
    private let sessionManager: SessionManager = {
        let configuration = URLSessionConfiguration.default
        configuration.httpAdditionalHeaders = SessionManager.defaultHTTPHeaders
        return SessionManager(configuration: configuration)
    }()
    
    private let lock = NSLock()
    
    private var clientID: String
    private var baseURLString: String
    
    
    private var isRefreshing = false
    private var requestsToRetry: [RequestRetryCompletion] = []
    
    // MARK: - Initialization
    
    init(clientID: String, baseURLString: String) {
        self.clientID = clientID
        self.baseURLString = baseURLString
    }
    
    // MARK: - RequestAdapter
    
    func adapt(_ urlRequest: URLRequest) throws -> URLRequest {
        if let token = UserDefaults.standard.string(forKey: "ACCESS_TOKEN"){
            if let urlString = urlRequest.url?.absoluteString, urlString.hasPrefix(baseURLString) {
                var urlRequest = urlRequest
                print(urlRequest.allHTTPHeaderFields)
                if let headerValues = urlRequest.allHTTPHeaderFields?.values{
                if !headerValues.contains(clientID){
                        urlRequest.setValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
                    }
                }else{
                   urlRequest.setValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
                }
                
                print(urlRequest.allHTTPHeaderFields)
                return urlRequest
            }
            return urlRequest
        }else{
            return urlRequest
            //TODO:
             print("Access token not found")
            // throw NSError(domain: "Login Failed", code: 400, userInfo: ["error_description" : "Login Failed"])
        }
    }
    
    // MARK: - RequestRetrier
    
    func should(_ manager: SessionManager, retry request: Request, with error: Error, completion: @escaping RequestRetryCompletion) {
        lock.lock() ; defer { lock.unlock() }
       // print(request.request?.allHTTPHeaderFields)
        if let response = request.task?.response as? HTTPURLResponse, response.statusCode == 401 {
            requestsToRetry.append(completion)
            
            if !isRefreshing {
                refreshTokens { [weak self] succeeded, accessToken, refreshToken in
                    guard let strongSelf = self else { return }
                    
                    strongSelf.lock.lock() ; defer { strongSelf.lock.unlock() }
                    
                    strongSelf.requestsToRetry.forEach { $0(succeeded, 0.0) }
                    strongSelf.requestsToRetry.removeAll()
                }
            }
        } else {
            completion(false, 0.0)
        }
    }
    
    // MARK: - Private - Refresh Tokens
    
    private func refreshTokens(completion: @escaping RefreshCompletion) {
        guard !isRefreshing else { return }
        
        isRefreshing = true
        
                let urlString = "\(baseURLString)/oauth/token"
                guard let accessToken = UserDefaults.standard.string(forKey: "ACCESS_TOKEN") else{return}
                guard let refreshToken = UserDefaults.standard.string(forKey: "REFRESH_TOKEN") else{return}
        
                let parameters: [String: Any] = [
                    "access_token": accessToken,
                    "refresh_token": refreshToken,
                    "client_id": clientID,
                    "grant_type": "refresh_token"
                ]
        let headers = ["Authorization":clientID]
        sessionManager.request(urlString, method: .post, parameters: parameters, encoding: URLEncoding.default, headers: headers)
                    .responseObject { (response:DataResponse<Token>) in
                       // print(String(data: response.data!, encoding: String.Encoding.utf8) as String!)
                        switch response.result {
                        case .failure( _):
                            completion(false, nil, nil)
                        case .success(let token):
                            UserDefaults.standard.set(token.access_token, forKey: "ACCESS_TOKEN")
                            UserDefaults.standard.set(token.refresh_token, forKey: "REFRESH_TOKEN")
                            completion(true, accessToken, refreshToken)
                        }
                        self.isRefreshing = false
                }
    }
    
    deinit {
        // BaseDebug.log("Deinitializing OAuth2Handler")
    }
}
