//
//  Session.swift
//  sureBellFramework-iOS
//
//  Created by PiOctave on 16/04/18.
//

//    Copyright (C) 2016-2018 PiOctave Solutions (Pvt) Ltd. (http://www.pioctave.com/)

//    All information contained herein is, and remains
//    the property of PiOctave Solutions (Pvt) Ltd and its suppliers
//    if any.  The intellectual and technical concepts contained
//    herein are proprietary to PiOctave Solutions (Pvt) Ltd
//    and are protected by trade secret or copyright law.
//    Dissemination of this information or reproduction of this material
//    is strictly forbidden unless prior written permission is obtained
//    from PiOctave Solutions (Pvt) Ltd.

//    THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
//    ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
//    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
//    ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
//    FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
//    DAMAGES. HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
//    STRICT LIABILITY, OR TORT ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
//    EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


import Foundation
import WebRTC
import SwiftyJSON


/// <#Description#>
public class Session:NSObject,RTCSignalHandler {
    var plugin: PhoneRTCPlugin?
    var config: SessionConfig?
    var constraints: RTCMediaConstraints?
    var peerConnection: RTCPeerConnection?
    var pcObserver: PCObserver?
    var queuedRemoteCandidates: [String:[RTCIceCandidate]]
    var peerConnectionFactory: RTCPeerConnectionFactory?
    var stream: RTCMediaStream?
    var videoTrack: RTCVideoTrack?
    var sessionKey: String?
    var signallingChannel:SignallingChannel?
    var to:String?
    var type:String?
    var callId:String?
    var timer:Timer?
    static var statsTime:Timer?
    var webSocketMessage:String = ""
    var callStartTime:Date?
    var candidateCount = 0
    var iceGathringStartTime:Date?
    var analyticsParameters: [String: NSObject] = ["def":"" as NSObject]
    var localCandidateQueue: [RTCIceCandidate?]?
    var remoteStreams: [RTCMediaStream]
    fileprivate var seqNo = 0
    var isLocal = false
    var turnAddressList:Any?
    var initTime:Date?
    var isMediaSignalStarted = false
    var isCallHangup = false
    
    init(plugin: PhoneRTCPlugin,
         peerConnectionFactory: RTCPeerConnectionFactory,
         config: SessionConfig,
         sessionKey: String, isLocal:Bool,isSDESSupported:Bool, callInItType: String) {
        self.isLocal = isLocal
        self.plugin = plugin
        self.queuedRemoteCandidates = [:]
        self.localCandidateQueue = [] //TODO:(niteesh) set this to nil  after checkign the version compatiblity
        self.remoteStreams = []
        self.config = config
        self.peerConnectionFactory = peerConnectionFactory
        self.sessionKey = sessionKey
        
        super.init()
        
        // initialize basic media constraints
        self.constraints = RTCMediaConstraints(
            mandatoryConstraints: [
                "OfferToReceiveAudio":"true",
                "googIPv6":"false",
                "OfferToReceiveVideo": "true",
                "echoCancellation": "false",
                "googEchoCancellation": "true",
                "googleNoiseSuppression":"true",
                "googHighpassFilter":"false",
                "levelControl":"true",
                "googAutoGainControl":"false"
            ],
            optionalConstraints: [
                "internalSctpDataChannels":"true",
                "DtlsSrtpKeyAgreement": "\(!isSDESSupported)"
            ]
        )
        self.initTime = Date()
        
        analyticsParameters.removeValue(forKey: "def")
        
        if callInItType == "CallKit"{
            analyticsParameters.updateValue("ring" as NSObject, forKey: "callFrom")
        }else{
            analyticsParameters.updateValue("liveView" as NSObject, forKey: "callFrom")
        }
        
        timer = Timer.scheduledTimer(timeInterval: 20, target: self, selector: #selector(Session.timerFire), userInfo: nil, repeats: false)
        
        Session.statsTime?.invalidate()
        Session.statsTime = nil
        Session.statsTime = Timer.scheduledTimer(timeInterval: 0.02, target: self, selector: #selector(Session.stats), userInfo: nil, repeats: true)
    }
    
    func setSignallingDelegate(signallingChannelDelegate:SignallingChannel){
        self.signallingChannel = signallingChannelDelegate
        self.signallingChannel!.setHandller(self)
    }
    
    func setTurnAddressList(turnAddressList:Any?){
        self.turnAddressList = turnAddressList
    }
    
    @objc func timerFire(){
        do {
            let encodedString : Data = (self.webSocketMessage as NSString).data(using: String.Encoding.utf8.rawValue)!
            let json = try JSON(data: encodedString)
            if self.webSocketMessage == ""{
                NotificationCenter.default.post(name: Notification.Name(rawValue: "myNotification"), object: ["signal":"EXIT"])
                //analyticsParameters.updateValue(true as NSObject, forKey: "disconnectedWithError")
                //analyticsParameters.updateValue(0 as NSObject, forKey: kFIRParameterValue)
                disconnect(true)
            }else{
                if let type = json["type"].string{
                    if (type.uppercased() == "REGISTER"){
                        NotificationCenter.default.post(name: Notification.Name(rawValue: "myNotification"), object: ["signal":"EXIT"])
                        //analyticsParameters.updateValue(true as NSObject, forKey: "disconnectedWithError")
                        //analyticsParameters.updateValue(0 as NSObject, forKey: AnalyticsParameterValue)
                        disconnect(true)
                    }else{
                        if let signal = json["from"]["signal"].string{
                            if signal == "hangup"{
                                NotificationCenter.default.post(name: Notification.Name(rawValue: "myNotification"), object: ["signal": "HANGUP"])
                                //analyticsParameters.updateValue(true as NSObject, forKey: "disconnectedWithError")
                                //analyticsParameters.updateValue(0 as NSObject, forKey: AnalyticsParameterValue)
                                disconnect(true)
                            }
                        }
                    }
                }
            }
        }catch{
            print("Problem found")
        }
    }
    
    
    //TODO: (Niteesh) to be uncommented in future
    func sendEndOfCandidate(){
        if self.callId == nil{
            localCandidateQueue!.append(nil)
        }else{
            localCandidateQueue!.append(nil)
            drainLocalCandidateQueue()
        }
    }
    
    
    func sendSDP(_ sdp:RTCSessionDescription){
        if self.callId != nil{
            //TODO : (niteesh)
            self.signallingChannel!.sendSdp(self.callId!, to: self.to!, type: self.type!, sdp: sdp, turnAddressList: self.turnAddressList)
            //self.signallingChannel!.sendSDP(self.callId!, sdp: sdp)
        }else{
            self.signallingChannel!.sendSDP(self.to!, type: self.type!, sdp: sdp, turnAddressList: self.turnAddressList)
        }
    }
    

    func isLocalCall()->Bool{
        return isLocal;
    }
    
    
    func isCandidateRelevant(iceCandidate:RTCIceCandidate)->Bool{
        if(isLocalCall()){
            if(!iceCandidate.sdp.contains("host")){
                return false
            }
        }
        return true
    }
    
    func istcpCandidate(iceCandidate:RTCIceCandidate)->Bool{
        if(iceCandidate.sdp.contains("tcp")){
            return true
        }else{
            return false
        }
    }
    
    
    func sendCandidate(_ iceCandidate:RTCIceCandidate?){
        //TODO: (niteesh)
        if localCandidateQueue != nil{
            if(iceCandidate != nil){
                if isCandidateRelevant(iceCandidate: iceCandidate!){
                    if !istcpCandidate(iceCandidate: iceCandidate!){
                        localCandidateQueue!.append(iceCandidate!)
                    }
                }
            }else{
                localCandidateQueue!.append(nil);
            }
        }
        
        if self.callId != nil {
            drainLocalCandidateQueue()
        }
        
    }
    
    var isEndOfCandidateSent = false
    var stopSendingCandidates = false
    
    
    
    func drainLocalCandidateQueue(){
        var sendEndOfCandidate = false
        if(localCandidateQueue != nil){
            for iceCandidate in localCandidateQueue! {
                if(iceCandidate == nil){
                    sendEndOfCandidate = true
                    continue
                }
                //TODO:(niteesh)
                if let iceDescription = iceCandidate?.description{
                    if !stopSendingCandidates{
                        if !iceDescription.contains("169.254"){
                            self.seqNo = self.seqNo + 1
                            self.signallingChannel!.sendCandidate(self.callId!,to: self.to!, type: self.type!, candidate: iceCandidate,i: self.seqNo, turnAddressList: self.turnAddressList)
                        }
                    }
                    
                    if iceDescription.contains("relay"){
                        if !isEndOfCandidateSent{
                            isEndOfCandidateSent = true
                            stopSendingCandidates = true
                            if !iceDescription.contains("169.254"){
                                self.seqNo = self.seqNo + 1
                                self.signallingChannel!.sendCandidate(self.callId!,to: self.to!, type: self.type!, candidate: iceCandidate,i: seqNo, turnAddressList: self.turnAddressList)
                            }
                            self.sendEndOfCandidate()
                        }
                    }
                }
            }
            
            if sendEndOfCandidate{
                self.seqNo = self.seqNo + 1
                self.signallingChannel!.sendEndOfCandidate(self.callId!, to:self.to!, type: self.type!, i: seqNo, turnAddressList: self.turnAddressList)
                self.sendMediaSignal()
            }
        }
        localCandidateQueue = []
    }
    
    
    func sendMediaSignal(){
        if (config?.streams?.media)!{
            self.seqNo = self.seqNo + 1
            if let signallingChannel = self.signallingChannel,let callid = self.callId,let to = self.to,let type = self.type{
                signallingChannel.sendMediaSignal(callid, to: to, type: type, i: self.seqNo, turnAddressList: self.turnAddressList)
            }else{
                self.isMediaSignalStarted = true
            }
        }
    }
    
    
    func handleDescriptionSet(){
        if (self.config!.isInitiator != nil) {
            if self.peerConnection?.remoteDescription != nil {
                print("SDP onSuccess - drain candidates")
                self.drainRemoteCandidates()
                self.drainLocalCandidateQueue()
            }
        } else {
            if self.peerConnection?.localDescription != nil {
                self.drainRemoteCandidates()
                self.drainLocalCandidateQueue()
            } else {
                self.peerConnection?.answer(for: self.constraints!, completionHandler: { (rtcDes, error) in
                    if error != nil {
                        print("SDP OnFailure: \(String(describing: error))")
                        return
                    }
                    if let originalSDP = rtcDes{
                        self.peerConnection?.setLocalDescription(originalSDP, completionHandler: { (error) in
                            if error != nil {
                                print("didSetSessionDescriptionWithError SDP OnFailure: \(String(describing: error))")
                                return
                            }
                            self.handleDescriptionSet()
                        })
                        if let callStarttime = self.callStartTime{
                            self.analyticsParameters.updateValue((Date().timeIntervalSince(callStarttime))*1000.0 as NSObject, forKey: "sending")
                        }
                        self.sendSDP(originalSDP)
                    }
                })
            }
        }
    }
    
    
    func getCurrentMillis()->Int64 {
        return Int64(Date().timeIntervalSince1970 * 1000)
    }
    
    
    func call(_ to:String, type:String) {
        // create a list of ICE servers
        if !self.isCallHangup{
            self.callStartTime = Date()
            if let callStarttime = self.callStartTime{
                if let initTime = self.initTime{
                    analyticsParameters.updateValue((callStarttime.timeIntervalSince(initTime))*1000.0 as NSObject, forKey: "creatingPeerConnection")
                }
            }
            
            analyticsParameters.updateValue(to as NSObject, forKey: "to-device")
            
            var iceServers: [RTCIceServer] = []
            //                iceServers.append(RTCIceServer(
            //                    urlStrings: ["stun:stun.l.google.com:19302"],
            //                    username: "",
            //                    credential: ""))
            
            
            if let config = self.config{
                for item in config.turn{
                    iceServers.append(RTCIceServer(urlStrings:[item.url],username:item.username,credential:item.credential))
                }
            }
            
            //        iceServers.append(RTCIceServer(
            //            urlStrings: [self.config!.turn!.url],
            //            username: self.config!.turn!.username,
            //            credential: self.config!.turn!.credential))
            
            let rtcConfig = RTCConfiguration()
            rtcConfig.iceServers = iceServers
            
            self.to = to
            self.type = type
            self.callId = to + "\(getCurrentMillis())"
            // initialize a PeerConnection
            self.pcObserver = PCObserver(session: self)
            self.peerConnection =
                peerConnectionFactory?.peerConnection(with: rtcConfig,
                                                      constraints: self.constraints!,
                                                      delegate: self.pcObserver)
            
            // create a media stream and add audio and/or video tracks
            createOrUpdateStream()
            // create offer if initiator
            if (self.config!.isInitiator != nil) {
                self.peerConnection?.offer(for: self.constraints!, completionHandler: { (rtcDes, error) in
                    if error != nil {
                        print("SDP OnFailure: \(String(describing: error))")
                        return
                    }
                    if let originalSDP = rtcDes{
                        self.peerConnection?.setLocalDescription(originalSDP, completionHandler: { (error) in
                            if error != nil {
                                print("didSetSessionDescriptionWithError SDP OnFailure: \(String(describing: error))")
                                return
                            }
                            self.handleDescriptionSet()
                        })
                        if let callStarttime = self.callStartTime{
                            self.analyticsParameters.updateValue((Date().timeIntervalSince(callStarttime))*1000.0 as NSObject, forKey: "sending")
                        }
                        self.sendSDP(originalSDP)
                    }
                })
            }
        }else{
            self.disconnect(false)
        }
    }
    

    func createOrUpdateStream() {
        if self.stream != nil {
            self.peerConnection?.remove(self.stream!)
            self.stream = nil
        }
        self.stream = peerConnectionFactory!.mediaStream(withStreamId: "ARDAMS")
        if self.config!.streams!.audio! {
            if self.plugin!.localAudioTrack == nil {
                self.plugin!.initLocalAudioTrack()
            }
            self.stream!.addAudioTrack(self.plugin!.localAudioTrack!)
        }
        
        //        if self.config!.streams!.video {
        //            if self.plugin!.localVideoTrack == nil {
        //                if let sessionKey = self.sessionKey{
        //                    self.plugin!.initLocalVideoTrack(sessionKey)
        //                }
        //            }
        //            self.stream!.addVideoTrack(self.plugin!.localVideoTrack!)
        //        }
        self.peerConnection?.add(self.stream!)
    }
    
    @objc func stats(){
        print("Memory Leak in Stats")
        peerConnection?.stats(for: nil, statsOutputLevel: RTCStatsOutputLevel.standard) { (obje:[RTCLegacyStatsReport]) in
            if !obje.isEmpty{
                // print(obje)
                for obj in obje{
                    if (obj.type) == "ssrc" && (obj.reportId.contains("ssrc"))
                        && (obj.reportId.contains("recv")){
                        if let bytesRecived = obj.values["bytesReceived"]{
                            if let bytes = Int64(bytesRecived){
                                if bytes != 0{
                                    if let frameWidth = obj.values["googFrameWidthReceived"]{
                                        if let width = Int64(frameWidth){
                                            if width != 0{
                                                NotificationCenter.default.post(name: Notification.Name(rawValue: "CHANGE_CALL_TYPE_VIDEO_TO_AUDIO"), object: 1)
                                            }else{
                                                NotificationCenter.default.post(name: Notification.Name(rawValue: "CHANGE_CALL_TYPE_VIDEO_TO_AUDIO"), object: 0)
                                            }
                                        }}
                                    NotificationCenter.default.post(name: Notification.Name(rawValue: "REMOVE_CALL_IMAGE_AFTER_CALL_CONNECTED"), object: nil)
                                    
                                }else{
                                    
                                }
                                // print(bytes)
                            }
                        }
                    }
                }
            }
        }
    }
    
    
    
    public func receiveMessage(_ message: String?) {
        if let soketData = message{
            self.webSocketMessage = soketData
            var error : NSError?
            let data : AnyObject?
            do {
                if let encodedText = soketData.data(using: String.Encoding.utf8){
                    data = try JSONSerialization.jsonObject(
                        with: encodedText,
                        options: []) as AnyObject
                    
                    let object = data as AnyObject
                    if let typeObject = object.object(forKey: "type") as? String{
                        if typeObject == "error"{
                            analyticsParameters.updateValue(false as NSObject, forKey: "disconnectedWithError")
                            // analyticsParameters.updateValue(0 as NSObject, forKey: AnalyticsParameterValue)
                            analyticsParameters.updateValue("recivedDeviceOffline" as NSObject, forKey: "state")
                            disconnect(false)
                            NotificationCenter.default.post(name: Notification.Name(rawValue: "myNotification"), object: ["signal":"OFFLINE"])
                            return
                        }
                    }
                    
                    
                    if let type = object.object(forKey: "type") as? String{
                        if (type.caseInsensitiveCompare("Call") != ComparisonResult.orderedSame){
                            return
                        }
                    }else{
                        return
                    }
                    
                    
                    
                    if let fromObject = object.object(forKey: "from"){
                        if isHangup(fromObject as AnyObject){
                            var callId = "";
                            if let callid = object.object(forKey: "id") as? String{
                                callId = callid
                            }
                            analyticsParameters.updateValue(false as NSObject, forKey: "disconnectedWithError")
                            // analyticsParameters.updateValue(1 as NSObject, forKey: AnalyticsParameterValue)
                            analyticsParameters.updateValue("recivedDeviceHangup" as NSObject, forKey: "state")
                            disconnect(false)
                            var notificationObject = [String: String]()
                            if let reason = (fromObject as AnyObject).object(forKey: "reason") as? String {
                                analyticsParameters.updateValue(reason as NSObject, forKey: "disconnectedWithErrorMessage")
                                notificationObject = ["signal": "HANGUP", "reason":reason ]
                            }else {
                                notificationObject = ["signal": "HANGUP"]
                            }
                            if (self.callId != nil) || (self.callId != ""){
                                NotificationCenter.default.post(name: Notification.Name(rawValue: "myNotification"), object: notificationObject)
                            }else if self.callId == callId{
                                NotificationCenter.default.post(name: Notification.Name(rawValue: "myNotification"), object: notificationObject )
                                
                            }
                        }else if isBusy(fromObject as AnyObject){
                            analyticsParameters.updateValue(false as NSObject, forKey: "disconnectedWithError")
                            // analyticsParameters.updateValue(1 as NSObject, forKey: AnalyticsParameterValue)
                            analyticsParameters.updateValue("recivedDeviceBusy" as NSObject, forKey: "state")
                            disconnect(false)
                            NotificationCenter.default.post(name: Notification.Name(rawValue: "myNotification"), object: ["signal": "BUSY"])
                        }else if isCandidate(fromObject as AnyObject){
                            if let callStarttime = self.callStartTime{
                                analyticsParameters.updateValue((Date().timeIntervalSince(callStarttime))*1000.0 as NSObject, forKey: "receivedRemote")
                            }
                            var callId = "";
                            if let callid = object.object(forKey: "id") as? String{
                                callId = callid
                            }
                            
                            
                            if let candidateObject = (fromObject as AnyObject).object(forKey: "candidate"){
                                if candidateObject as? NSObject == NSNull(){
                                    print("####EOD Recived\(Utils.getCurrentMillis())")
                                    return
                                }
                                var sdpValue = ""
                                var sdpLineIndexValue = Int32(0)
                                var midValue = ""
                                
                                if let mid = (candidateObject as AnyObject).object(forKey: "sdpMid") as? String{
                                    midValue = mid
                                }
                                
                                
                                if let sdpLineIndex = (candidateObject as AnyObject).object(forKey: "sdpMLineIndex") as?NSNumber{
                                    sdpLineIndexValue = sdpLineIndex.int32Value
                                }
                                
                                
                                if let sdp: String = (candidateObject as AnyObject).object(forKey: "candidate") as? String{
                                    print("####CANDIDATE Recived\(Utils.getCurrentMillis())")
                                    sdpValue = sdp
                                }
                                
                                let candidate = RTCIceCandidate(
                                    sdp: sdpValue,
                                    sdpMLineIndex: sdpLineIndexValue,
                                    sdpMid: midValue
                                )
                                print("check\(String(describing: self.callId)) \(callId)")
                                if(self.callId != nil) && callId == self.callId{
                                    candidateCount = candidateCount + 1
                                    self.peerConnection?.add(candidate)
                                    print("candidate added to peerconnection \(getCurrentMillis())")
                                }else if self.queuedRemoteCandidates[callId] == nil{
                                    self.queuedRemoteCandidates[callId] = [candidate]
                                } else  {
                                    if let remoteQueue = self.queuedRemoteCandidates[callId]{
                                        var remoteCandidateQueue = remoteQueue as Array
                                        remoteCandidateQueue.append(candidate)
                                    }
                                }
                            }
                            
                        }else if isSdp(fromObject as AnyObject){
                            if let callId = object.object(forKey: "id") as? String{
                                if(self.callId == nil){
                                    self.callId = callId
                                }else if(self.callId != callId){
                                    return
                                }
                            }
                            if let sdpObject = (fromObject as AnyObject).object(forKey: "sdp") {
                                print("####SDP Recived\(Utils.getCurrentMillis())")
                                if let typeObj = (sdpObject as AnyObject).object(forKey: "type") as? String{
                                    let type = RTCSessionDescription.type(for: typeObj)
                                    
                                    if let callStarttime = self.callStartTime{
                                        analyticsParameters.updateValue((Date().timeIntervalSince(callStarttime))*1000.0 as NSObject, forKey: "receivedRemote" + RTCSessionDescription.string(for: type))
                                    }
                                    self.timer?.invalidate()
                                    //Toast(text:" receivedRemote SDP").show()
                                    if let sdpMessage = (sdpObject as AnyObject).object(forKey: "sdp") as? String{
                                        let sdp = RTCSessionDescription(type: type, sdp: sdpMessage)
                                        self.peerConnection?.setRemoteDescription(sdp, completionHandler: { (error) in
                                            if error != nil {
                                                print("didSetSessionDescriptionWithError SDP OnFailure: \(String(describing: error))")
                                                return
                                            }
                                            self.handleDescriptionSet()
                                        })
                                    }
                                }
                            }
                        }
                    }else{
                        return
                    }
                }
                
            } catch let error1 as NSError {
                error = error1
                print("error while reciving message",error ?? "")
                data = nil
            }
            
        }
        
    }
    
    

    func isBusy(_ message:AnyObject) -> Bool{
        if let msg = message.object(forKey: "signal")as? String{
            if msg == "busy"{
                return true
            }else{
                return false
            }
            
        } else {
            return false
        }
    }
    
    
    func isHangup(_ message:AnyObject) -> Bool{
        if let msg = message.object(forKey: "signal") as? String{
            if msg == "hangup"{
                return true
            }else{
                return false
            }
        } else {
            return false
        }
    }
    
    
    func isCandidate(_ message:AnyObject) -> Bool{
        if let _ = message.object(forKey: "candidate"){
            return true
        } else {
            return false
        }
    }
    
    
    func isSdp(_ message:AnyObject)-> Bool{
        if let _ = message.object(forKey: "sdp"){
            return true
        } else {
            return false
        }
    }
    
    func disconnect(_ sendByeMessage: Bool) {
        Session.statsTime?.invalidate()
        Session.statsTime = nil
        print("SESSION DISCONNECT EMPTY \(sendByeMessage)")
        peerConnection?.stats(for: nil, statsOutputLevel: RTCStatsOutputLevel.standard) { (obje:[RTCLegacyStatsReport]) in
            if !obje.isEmpty{
                //  print(obje)
                for obj in obje{
                    if (obj.type) == "ssrc" && (obj.reportId.contains("ssrc")){
                        if let bytesRecived = obj.values["bytesReceived"]{
                            if let bytes = Int64(bytesRecived){
                                self.analyticsParameters.updateValue(bytes as NSObject, forKey: "byteReceived")
                            }
                        }
                        if let media = obj.values["mediaType"]{
                            if media == "video"{
                                if let bytesRecived = obj.values["bytesReceived"]{
                                    if let bytes = Int64(bytesRecived){
                                        self.analyticsParameters.updateValue(bytes as NSObject, forKey: "videoByteReceived")
                                    }
                                }
                                if let packetLost = obj.values["packetsLost"]{
                                    if let bytes = Int64(packetLost){
                                        self.analyticsParameters.updateValue(bytes as NSObject, forKey: "videoPacketsLost")
                                    }
                                }
                                if let packetLost = obj.values["packetsReceived"]{
                                    if let bytes = Int64(packetLost){
                                        self.analyticsParameters.updateValue(bytes as NSObject, forKey: "videoPacketsReceived")
                                    }
                                }
                                if let frameWidth = obj.values["googFrameWidthReceived"]{
                                    if let width = Int64(frameWidth){
                                        self.analyticsParameters.updateValue(width as NSObject, forKey: "frameWidthReceived")
                                    }
                                }
                                if let frameHeight = obj.values["googFrameHeightReceived"]{
                                    if let height = Int64(frameHeight){
                                        self.analyticsParameters.updateValue(height as NSObject, forKey: "frameHeightReceived")
                                    }
                                }
                            }
                        }
                        if let bytesSent = obj.values["bytesSent"]{
                            if let bytes = Int64(bytesSent){
                                self.analyticsParameters.updateValue(bytes as NSObject, forKey: "byteSend")
                            }
                        }
                        if let packetsSent = obj.values["packetsSent"]{
                            if let packets = Int64(packetsSent){
                                self.analyticsParameters.updateValue(packets as NSObject, forKey: "packetSent")
                            }
                        }
                    }
                }
            }
        }
        
        self.timer?.invalidate()
        
        if let callid = self.callId{
            //  analyticsParameters.updateValue(callid as NSObject, forKey: "callId")
            print("CALL ID AVAILABLE")
            if sendByeMessage{
                self.signallingChannel?.hangupCall(callid, to: self.to!, type: self.type!)
            }
        }else{
            self.isCallHangup = true
        }
        
        if self.signallingChannel == nil{
            print("signallingChannel AVAILABLE")
        }
        
        self.signallingChannel?.closeSocket();
        
        
        if self.videoTrack != nil {
            self.removeVideoTrack(self.videoTrack!)
        }
        self.remoteStreams.removeAll()
        
        if let strm = self.stream{
            self.peerConnection?.remove(strm)
        }
        
        self.stream = nil
        
        self.peerConnection?.close()
        self.peerConnection = nil
        self.queuedRemoteCandidates.removeAll()
        
        Session.statsTime?.invalidate()
        Session.statsTime = nil
        
        
        if let callStarttime = self.callStartTime{
            // analyticsParameters.updateValue((Date().timeIntervalSince(callStarttime))*1000.0 as NSObject, forKey: "callDuration")
        }
        
        // if analyticsParameters[kFIRParameterValue] == nil {
        //  analyticsParameters.updateValue(false as NSObject, forKey: "disconnectedWithError")
        //  analyticsParameters.updateValue(1 as NSObject, forKey: AnalyticsParameterValue)
        // }
        
        if self.isLocal{
            //   analyticsParameters.updateValue("Local" as NSObject, forKey: "callType")
        }else{
            //analyticsParameters.updateValue("Server" as NSObject, forKey: "callType")
        }
        
        print("analyticsParameters",analyticsParameters)
        // Analytics.logEvent("webrtcCall", parameters: analyticsParameters)
        if let sessionKey = self.sessionKey{
            self.plugin!.onSessionDisconnect(sessionKey)
        }
        self.pcObserver = nil
        self.callId = nil
    }
    
    
    func addStream(_ stream :  RTCMediaStream){
        self.remoteStreams.append(stream);
    }
    
    
    func muteAudio (isAudio:Bool){
        for stream in self.remoteStreams {
            // stream.mute
            stream.audioTracks[0].isEnabled = isAudio
        }
    }
    
    func addVideoTrack(_ videoTrack: RTCVideoTrack) {
        self.videoTrack = videoTrack
        self.plugin!.addRemoteVideoTrack(videoTrack)
        if let callStarttime = self.callStartTime{
            analyticsParameters.updateValue((Date().timeIntervalSince(callStarttime))*1000.0 as NSObject, forKey: "callConnected")
        }
        print("call connected \(getCurrentMillis())")
        DispatchQueue.main.async {
            print("callConnected")
        }
        DispatchQueue.main.async {
            let state = UIApplication.shared.applicationState
            
            if state == .background {
                do {
                    if UIDevice.current.userInterfaceIdiom != .pad{
                        if #available(iOS 10.0, *) {
                            try AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayAndRecord, mode: AVAudioSessionModeDefault, options: [.duckOthers])
                        } else {
                            // Fallback on earlier versions
                        }
                    }
                } catch let error{
                    print("SetCategory \(error)")
                }
            }else if state == .active {
                do {
                    if UIDevice.current.userInterfaceIdiom != .pad{
                        if #available(iOS 10.0, *) {
                            try AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayAndRecord, mode: AVAudioSessionModeDefault, options: [.duckOthers, .defaultToSpeaker])
                        } else {
                            // Fallback on earlier versions
                        }
                    }
                } catch let error{
                    print("SetCategory \(error)")
                }
            }
        }
    }
    
    
    func removeVideoTrack(_ videoTrack: RTCVideoTrack) {
        self.plugin!.removeRemoteVideoTrack(videoTrack)
    }
    
    
    
    //    func firstMatch(_ pattern: NSRegularExpression, string: String) -> String? {
    //        let nsString = string as NSString
    //
    //        let result = pattern.firstMatch(in: string,
    //                                        options: NSRegularExpression.MatchingOptions(),
    //                                        range: NSMakeRange(0, nsString.length))
    //
    //        if result == nil {
    //            return nil
    //        }
    //
    //        return nsString.substring(with: result!.rangeAt(1))
    //    }
    //
    func sendMessage(_ message: Data) {
        //        self.plugin!.sendMessage("", message: message)
    }
    
    func drainRemoteCandidates() {
        
        if self.queuedRemoteCandidates[self.callId!] != nil {
            for candidate in self.queuedRemoteCandidates[self.callId!]!{
                //this is not calling
                self.peerConnection?.add(candidate)
            }
            self.queuedRemoteCandidates = [:]
        }
    }
    
    func sosCall(){
        self.signallingChannel?.sosCall(self.callId!, to: self.to!, type: self.type!)
    }
    
    func onChangeIceConnectionState(_ newState: RTCIceConnectionState){
        if(RTCIceConnectionState.connected == newState){
            analyticsParameters.updateValue((Date().timeIntervalSince(self.callStartTime!))*1000.0 as NSObject, forKey: "iceConnected")
        }else if(RTCIceConnectionState.disconnected == newState){
            analyticsParameters.updateValue((Date().timeIntervalSince(self.callStartTime!))*1000.0 as NSObject, forKey: "iceDisconnected")
        }
    }
    
    
    func onChangeIceGathringState(_ newState: RTCIceGatheringState){
        if newState == RTCIceGatheringState.complete {
            analyticsParameters.updateValue((Date().timeIntervalSince(self.callStartTime!))*1000.0 as NSObject, forKey: "iceGathringComplete")
            if !isEndOfCandidateSent{
                self.sendEndOfCandidate()
            }
        } else if newState == RTCIceGatheringState.gathering{
            self.iceGathringStartTime = Date()
        }else {
            if let iceGetheringTime = self.iceGathringStartTime{
                analyticsParameters.updateValue((Date().timeIntervalSince(iceGetheringTime))*1000.0 as NSObject, forKey: "iceGathringComplete")
            }
        }
    }
    
    
    
}

