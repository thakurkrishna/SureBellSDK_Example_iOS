//
//  UserHttp.swift
//  sureBellFramework-iOS
//
//  Created by PiOctave on 17/04/18.
//

//    Copyright (C) 2016-2018 PiOctave Solutions (Pvt) Ltd. (http://www.pioctave.com/)

//    All information contained herein is, and remains
//    the property of PiOctave Solutions (Pvt) Ltd and its suppliers
//    if any.  The intellectual and technical concepts contained
//    herein are proprietary to PiOctave Solutions (Pvt) Ltd
//    and are protected by trade secret or copyright law.
//    Dissemination of this information or reproduction of this material
//    is strictly forbidden unless prior written permission is obtained
//    from PiOctave Solutions (Pvt) Ltd.

//    THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
//    ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
//    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
//    ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
//    FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
//    DAMAGES. HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
//    STRICT LIABILITY, OR TORT ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
//    EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


import Foundation
import Alamofire
import AlamofireObjectMapper
import ObjectMapper

class UserHttp{
    
    static var instance = UserHttp()
    private init(){
        
    }
    
    enum Router: URLRequestConvertible {
        static let baseURLString = OAuthConst.BASE_URL_STRING
        
        case create(firstName:String,lastName:String,email:String,phone:String,password:String, countryDialCode:String)
        case updateNotificationToken(token:String, device:String)
        case deleteNotificationToken(token: String)
        case registerVendor()
        case getMe()
        case updateMe(user:User)
        case resetPassword(parameters: Parameters)
        case updatePassword(parameters: Parameters)
        // case resendVerificationCode()
        case resendPhoneVerificationCode(id:String)
        case resendEmailVerification(id:String)
        case generateLoginOtp(phone:String)
        case verifyEmail( id:String, code:String)
        case verifyPhone(id:String, code:String)
        case forgotPassword(userName:String)
        case logoutFromOtherDevices()
        case setProfileImage()
        
        var method: HTTPMethod {
            switch self {
            case .create:
                return .post
            case .updateNotificationToken:
                return .put
            case .deleteNotificationToken:
                return .delete
            case .registerVendor:
                return .put
            case .getMe:
                return .get
            case .updateMe:
                return .put
            case .resetPassword:
                return .post
            case .updatePassword:
                return .put
                //            case .resendVerificationCode:
            //                return .post
            case .resendPhoneVerificationCode:
                return .post
            case .resendEmailVerification:
                return .post
            case .generateLoginOtp:
                return .post
            case .verifyEmail:
                return .get
            case .verifyPhone:
                return .get
            case .forgotPassword:
                return .post
            case .logoutFromOtherDevices:
                return .delete
            case .setProfileImage:
                return .put
            }
        }
        
        var path: String {
            switch self {
            case .create:
                return "/user"
            case .updateNotificationToken:
                return "/user/notification-token"
            case .deleteNotificationToken:
                return "/user/notification-token"
            case .registerVendor:
                return "/user/register-vendor"
            case .getMe:
                return "/user/me"
            case .updateMe:
                return "/user/me"
            case .resetPassword:
                return "/user/reset-passwd"
            case .updatePassword:
                return "/user/password"
                //            case .resendVerificationCode:
            //                return "/user/resend-verification-code"
            case .resendPhoneVerificationCode:
                return "/user/resend-phone-verification-code"
            case .resendEmailVerification:
                return "/user/resend-email-verification-code"
            case .generateLoginOtp:
                return "/user/generate-login-otp"
            case .verifyEmail:
                return "/user/verify-email"
            case .verifyPhone:
                return "/user/verify-phone"
            case .forgotPassword:
                return "/user/forgot-passwd"
            case .logoutFromOtherDevices:
                return "/user/access-token"
            case .setProfileImage:
                return "/user/me"
            }
        }
        
        func asURLRequest() throws -> URLRequest {
            let url = try Router.baseURLString.asURL()
            
            var urlRequest = URLRequest(url: url.appendingPathComponent(path))
            urlRequest.httpMethod = method.rawValue
            switch self {
            case let .create(user):
                urlRequest = try JSONEncoding.default.encode(urlRequest, with: ["firstname":user.firstName,"lastname":user.lastName,"email":user.email,"phone":user.phone,"password":user.password,"countryDialCode":user.countryDialCode])
            case let .updateNotificationToken(token, device):
                urlRequest = try JSONEncoding.default.encode(urlRequest, with: ["token":token, "device":device])
            case let .deleteNotificationToken(token):
                urlRequest = try URLEncoding.default.encode(urlRequest, with: ["token":token] )
            case let .updateMe(user):
                urlRequest = try JSONEncoding.default.encode(urlRequest, with: Mapper().toJSON(user))
            case let .resetPassword(parameters):
                urlRequest = try JSONEncoding.default.encode(urlRequest, with: parameters)
            case let .updatePassword(parameters):
                urlRequest = try JSONEncoding.default.encode(urlRequest, with: parameters)
            case let .resendPhoneVerificationCode(id):
                urlRequest = try JSONEncoding.default.encode(urlRequest, with: ["id":id])
            case let .resendEmailVerification(id):
                urlRequest = try JSONEncoding.default.encode(urlRequest, with: ["id":id])
            case let .generateLoginOtp(phone):
                urlRequest = try JSONEncoding.default.encode(urlRequest, with: ["phone":phone])
            case let .verifyEmail(id, code):
                urlRequest = try URLEncoding.default.encode(urlRequest, with: ["id":id, "code":code])
            case let .verifyPhone(id, code):
                urlRequest = try URLEncoding.default.encode(urlRequest, with: ["id":id, "code":code])
            case let .forgotPassword(phone):
                urlRequest = try JSONEncoding.default.encode(urlRequest, with: ["email":phone])
            case let .logoutFromOtherDevices(refreshToken):
                urlRequest = try URLEncoding.default.encode(urlRequest, with: ["refreshToken":refreshToken])
            case .setProfileImage:
                urlRequest = try JSONEncoding.default.encode(urlRequest, with: ["profilePicture":"profilePicture"])
            default:
                break
            }
            
            return urlRequest
        }
    }
    
    fileprivate func makeObjectRequest(_ urlRequestConvertible: URLRequestConvertible, _ failure: @escaping (Any) -> Void, _ callback: @escaping (User) -> Void) -> Void {
        BaseController.instance.makeRequest(urlRequestConvertible: urlRequestConvertible).responseObject { (response:DataResponse<User>) in
            switch response.result {
            case let .failure(error):
                if let apiError = Mapper<APIError>().map(JSONString:String(data: response.data!, encoding: String.Encoding.utf8)! ){
                    failure(apiError)
                }else{
                    failure(error)
                }
            case let .success(user):
                callback(user)
            }
        }
    }
    
    fileprivate func makeArrayRequest(_ urlRequestConvertible: URLRequestConvertible, _ failure: @escaping (Any) -> Void, _ callback: @escaping ([User]) -> Void) -> Void {
        BaseController.instance.makeRequest(urlRequestConvertible:urlRequestConvertible).responseArray { (response:DataResponse<[User]>) in
            switch response.result {
            case let .failure(error):
                if let apiError = Mapper<APIError>().map(JSONString:String(data: response.data!, encoding: String.Encoding.utf8)! ){
                    failure(apiError)
                }else{
                    failure(error)
                }
            case let .success(users):
                callback(users)
            }
        }
    }
    
    fileprivate func makeStatusResponseObjectRequest(_ urlRequestConvertible: URLRequestConvertible, _ failure: @escaping (Any) -> Void, _ callback: @escaping (StatusResponse) -> Void) -> Void {
        BaseController.instance.makeRequest(urlRequestConvertible: urlRequestConvertible).responseObject { (response:DataResponse<StatusResponse>) in
            switch response.result {
            case let .failure(error):
                if let apiError = Mapper<APIError>().map(JSONString:String(data: response.data!, encoding: String.Encoding.utf8)! ){
                    failure(apiError)
                }else{
                    failure(error)
                }
            case let .success(statusResponse):
                callback(statusResponse)
            }
        }
    }
    
    func create(firstName:String,lastName:String, email:String, phone:String,password:String, countryDialCode:String, callback:@escaping (_ newUser:User)->Void, failure:@escaping (_ error:Any)->Void) -> Void {
        makeObjectRequest(Router.create(firstName: firstName, lastName: lastName, email: email, phone: phone, password: password, countryDialCode: countryDialCode), failure, callback)
    }
    func updateNotificationToken(token:String, device:String, callback:@escaping (_ newUser:User)->Void, failure:@escaping (_ error:Any)->Void) -> Void {
        makeObjectRequest(Router.updateNotificationToken(token: token, device: device), failure, callback)
    }
    func deleteNotificationToken(token: String, callback:@escaping (_ newUser:User)->Void, failure:@escaping (_ error:Any)->Void) -> Void {
        makeObjectRequest(Router.deleteNotificationToken(token: token), failure, callback)
    }
    func registerVendor(callback:@escaping (_ newUser:User)->Void, failure:@escaping (_ error:Any)->Void) -> Void {
        makeObjectRequest(Router.registerVendor(), failure, callback)
    }
    func getMe(callback:@escaping (_ newUser:User)->Void, failure:@escaping (_ error:Any)->Void) -> Void {
        makeObjectRequest(Router.getMe(), failure, callback)
    }
    func updateMe(userUpdate: User, callback:@escaping ( _ newUser:User)->Void, failure:@escaping (_ error:Any)->Void) -> Void {
        makeObjectRequest(Router.updateMe(user:userUpdate), failure, callback)
    }
    func resetPassword(parameters: Parameters, callback:@escaping ( _ newUser:StatusResponse)->Void, failure:@escaping (_ error:Any)->Void) -> Void {
        makeStatusResponseObjectRequest(Router.resetPassword(parameters: parameters), failure, callback)
    }
    func updatePassword(parameters: Parameters, callback:@escaping ( _ newUser:User)->Void, failure:@escaping (_ error:Any)->Void) -> Void {
        makeObjectRequest(Router.updatePassword(parameters:parameters), failure, callback)
    }
    //    func resendVerificationCode(callback:@escaping ( _ newUser:StatusResponse)->Void, failure:@escaping (_ error:Any)->Void) -> Void {
    //        makeStatusResponseObjectRequest(Router.resendVerificationCode(), failure, callback)
    //    }
    func resendPhoneVerificationCode(id:String,callback:@escaping ( _ newUser:StatusResponse)->Void, failure:@escaping (_ error:Any)->Void) -> Void {
        makeStatusResponseObjectRequest(Router.resendPhoneVerificationCode(id:id), failure, callback)
    }
    func resendEmailVerification(id:String,callback:@escaping ( _ newUser:StatusResponse)->Void, failure:@escaping (_ error:Any)->Void) -> Void {
        makeStatusResponseObjectRequest(Router.resendEmailVerification(id:id), failure, callback)
    }
    func generateLoginOtp(phone:String,callback:@escaping ( _ newUser:StatusResponse)->Void, failure:@escaping (_ error:Any)->Void) -> Void {
        makeStatusResponseObjectRequest(Router.generateLoginOtp(phone: phone), failure, callback)
    }
    func verifyEmail( id:String,  code:String,callback:@escaping ( _ newUser:StatusResponse)->Void, failure:@escaping (_ error:Any)->Void) -> Void {
        makeStatusResponseObjectRequest(Router.verifyEmail(id:id, code:code), failure, callback)
    }
    func verifyPhone(id:String,  code:String,callback:@escaping ( _ newUser:StatusResponse)->Void, failure:@escaping (_ error:Any)->Void) -> Void {
        makeStatusResponseObjectRequest(Router.verifyPhone(id:id, code:code), failure, callback)
    }
    func forgotPassword(userName:String,callback:@escaping ( _ newUser:StatusResponse)->Void, failure:@escaping (_ error:Any)->Void) -> Void {
        makeStatusResponseObjectRequest(Router.forgotPassword(userName: userName), failure, callback)
    }
    func logoutFromOtherDevices(callback:@escaping ( _ newUser:StatusResponse)->Void, failure:@escaping (_ error:Any)->Void) -> Void {
        makeStatusResponseObjectRequest(Router.logoutFromOtherDevices(), failure, callback)
    }
    func uploadProfileImage(imageData: Data, callback:@escaping (_ status:Bool)->Void, failure:@escaping (_ error:Any)->Void) -> Void {
        makeObjectRequest(Router.setProfileImage(), failure, { (user) in
            if let url = user.profilePicture {
                self.uploadUserImage(url, imageData: imageData, callback: { (status) in
                    callback(status)
                })
            }else{
                let apiError = APIError()
                apiError.error_description = "failed to upload"
                apiError.code = 509
                apiError.error = "Failed"
                failure(failure)
                print(apiError.error)
            }
        })
    }
    
    func uploadUserImage(_ url:String, imageData:Data, callback:@escaping (_ status:Bool)->Void) {
        if let requestUrl = URL(string: url){
            let lobj_Request = NSMutableURLRequest(url: requestUrl)
            lobj_Request.httpMethod = "PUT"
            lobj_Request.httpBody = imageData
            lobj_Request.setValue("public-read", forHTTPHeaderField: "x-goog-acl")
            lobj_Request.timeoutInterval = 30
            lobj_Request.httpShouldHandleCookies=true
            lobj_Request.cachePolicy = NSURLRequest.CachePolicy.reloadIgnoringCacheData
            
            let task = URLSession.shared.dataTask(with: lobj_Request as URLRequest) {
                (data, response, error) -> Void in
                let httpResponse = response as? HTTPURLResponse
                var statuscode:Int?
                if let statusCode = httpResponse?.statusCode{
                    statuscode = statusCode
                    if statuscode == 200 {
                        callback(true)
                    }else{
                        callback(false)
                    }
                }else{
                    callback(false)
                }
            }
            task.resume()
        }
    }
}

