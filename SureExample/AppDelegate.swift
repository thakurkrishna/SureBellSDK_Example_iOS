//
//  AppDelegate.swift
//  SureExample
//
//  Created by piOctave on 6/6/18.
//  Copyright © 2018 piOctave. All rights reserved.
//

import UIKit
import CoreData
import UserNotifications
import PushKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, NetServiceBrowserDelegate, NetServiceDelegate {
    
    var window: UIWindow?
    let reachability = Reachability()!
    var browser:NetServiceBrowser?
    var services = [NetService]()
    
    
    class var shared: AppDelegate {
        return UIApplication.shared.delegate as! AppDelegate
    }
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        launchAppScreenWithOption(withIdentifier: "LoginViewController")
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.reachabilityChanged),name: ReachabilityChangedNotification,object: reachability)
        do{
            try reachability.startNotifier()
        }catch{
            print("could not start reachability notifier")
        }
        return true
    }
    
    func connectedWithWifi(){
        
        NotificationCenter.default.post(name: Notification.Name(rawValue: "NETWORK_CHANGED"), object: nil)
        
        services.removeAll()
        
        self.browser = NetServiceBrowser()
        self.browser?.delegate = self;
        self.browser?.searchForServices(ofType: "_https._tcp.", inDomain: "local.")
        
        SharedData.AppSetting.IS_INTERNET_CONNECTED = true
    }
    
    func connectedWithCellular(){
        NotificationCenter.default.post(name: Notification.Name(rawValue: "NETWORK_CHANGED"), object: nil)
        DispatchQueue.main.async {
            self.services.removeAll()
            self.browser = nil
            print("Reachable via Cellular")
            NotificationCenter.default.post(name: Notification.Name(rawValue: "UPDATE_ACTIVE_LOCAL"), object: 0)
            SharedData.AppSetting.IS_INTERNET_CONNECTED = true
        }
    }
    
    func notconnectedWithInternet(){
        print("Network not reachable")
        NotificationCenter.default.post(name: Notification.Name(rawValue: "NETWORK_CHANGED"), object: nil)
        services.removeAll()
        browser = nil
        NotificationCenter.default.post(name: Notification.Name(rawValue: "UPDATE_ACTIVE_LOCAL"), object: 0)
        DispatchQueue.main.async {
            SharedData.AppSetting.IS_INTERNET_CONNECTED = false
        }
    }
    
    func topMostViewController() -> UIViewController? {
        let LoginViewController = UIApplication.shared.windows.first?.rootViewController
        return self.topMostViewControllerOfViewController(LoginViewController)
    }
    
    func topMostViewControllerOfViewController(_ viewController: UIViewController?) -> UIViewController? {
        // UITabBarController
        if let tabBarController = viewController as? UITabBarController,
            let selectedViewController = tabBarController.selectedViewController {
            return self.topMostViewControllerOfViewController(selectedViewController)
        }
        
        // UINavigationController
        if let navigationController = viewController as? UINavigationController,
            let visibleViewController = navigationController.visibleViewController {
            return self.topMostViewControllerOfViewController(visibleViewController)
        }
        
        // presented view controller
        if let presentedViewController = viewController?.presentedViewController {
            return self.topMostViewControllerOfViewController(presentedViewController)
        }
        
        // child view controller
        for subview in viewController?.view?.subviews ?? [] {
            if let childViewController = subview.next as? UIViewController {
                return self.topMostViewControllerOfViewController(childViewController)
            }
        }
        return viewController
    }
    
    
    func checkInternetReachability(){
        DispatchQueue.main.async {
            if self.reachability.currentReachabilityStatus == .reachableViaWiFi{
                self.connectedWithWifi()
            }else if self.reachability.currentReachabilityStatus == .reachableViaWWAN{
                self.connectedWithCellular()
            }else{
                self.notconnectedWithInternet()
            }
        }
    }
    
    
    @objc func reachabilityChanged(note: NSNotification) {
        NotificationCenter.default.post(name: Notification.Name(rawValue: "ESPWIFICONNECTED"), object: nil)
        DispatchQueue.main.async {
            let reachability = note.object as! Reachability
            if reachability.isReachable {
                if reachability.isReachableViaWiFi {
                    self.connectedWithWifi()
                } else {
                    self.connectedWithCellular()
                }
            }else {
                self.notconnectedWithInternet()
            }
        }
    }
    
    
    func launchAppScreenWithOption(withIdentifier:String){
        switch withIdentifier {
        case "LoginViewController":
            let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
            let login: LoginViewController = storyboard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
            let nav: NavigationController = storyboard.instantiateViewController(withIdentifier: "NavigationController") as! NavigationController
            nav.viewControllers = [login]
            self.window?.rootViewController = nav
        case "RegisterViewController":
            let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
            let register: RegisterViewController = storyboard.instantiateViewController(withIdentifier: "RegisterViewController") as! RegisterViewController
            let nav: NavigationController = storyboard.instantiateViewController(withIdentifier: "NavigationController") as! NavigationController
            nav.viewControllers = [register]
            self.window?.rootViewController = nav
        default:
            print("Screen not decided yet to open")
        }
    }
    
    
    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
         */
        let container = NSPersistentContainer(name: "")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    
}


extension AppDelegate: UNUserNotificationCenterDelegate {
    
    /// Ask for permission for first time in app
    func registerForRemoteNotification() {
        let center  = UNUserNotificationCenter.current()
        center.delegate = self
        center.requestAuthorization(options: [.sound, .alert, .badge], completionHandler: { (granted, error) in
            DispatchQueue.main.async(execute: {
                UIApplication.shared.registerForRemoteNotifications()
            })
        })
    }
    
    /// Called when a notification is delivered to a foreground app.
    /// UNUserNotificationCenter for iOS 10
    /// - Parameters:
    ///   - center: delegate params
    ///   - notification: delegate params
    ///   - completionHandler: delegate params
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        completionHandler([.sound, .alert, .badge])
    }
    
    //Called to let your app know which action was selected by the user for a given notification.
    
    /// This is UNUserNotificationCenter delegate  for iOS:10 and above. This method will get called when Event fires from notification tray (on selection of any notification)
    /// - Parameters:
    ///   - center: delegate params
    ///   - response: delegate params
    ///   - completionHandler: delegate params
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        if response.notification.request.content.title == "Motion Event"{
            let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
            let event = storyboard.instantiateViewController(withIdentifier: "EventListViewController") as! EventListViewController
            if let id = response.notification.request.content.userInfo["deviceID"] as? String{
                event.deviceID = id
            }
            if let name = response.notification.request.content.userInfo["deviceName"] as? String{
                event.deviceName = name
            }
        }else if response.notification.request.content.categoryIdentifier == "com.pioctave.localNotification.WifiSetup"{
            // SharedData.DeviceDataInNavigation.NAVIGATION_FROM = "WifiSetup"
        }
        
        completionHandler()
    }
}


extension AppDelegate: PKPushRegistryDelegate {
    
    /// This is PushKit framework delegate method. This method will get called when Users
    /// PushKit token updates
    /// - Parameters:
    ///   - registry: delegate params
    ///   - credentials: delegate params
    ///   - type: delegate params
    func pushRegistry(_ registry: PKPushRegistry, didUpdate credentials: PKPushCredentials, for type: PKPushType) {
        let token = "\(credentials.token.map { String(format: "%02.2hhx", $0) }.joined())"
        print("PUSH TOKEN ",token)
    }
    
    /*
     Application PushKit notification delegates management
     */
    // MARK: Register for push
    /// This is UIApplication framework delegate method. This method will get called when User
    /// will register for notification Note: method deprecated in iOS 10
    /// - Parameters:
    ///   - application:  delegate params
    ///   - notificationSettings:  delegate params
    func application(_ application: UIApplication, didRegister notificationSettings: UNNotificationSetting){
        let voipRegistry = PKPushRegistry(queue: DispatchQueue.main)
        voipRegistry.desiredPushTypes = Set([PKPushType.voIP])
        voipRegistry.delegate = self
    }
    

    /// Get Users Unique id
    /// - Returns: returns ID as String
    func getDeviceId()-> String{
        let appId = UserDefaults.standard
        let key = "MEDIUMID"
        if let deviceID = appId.object(forKey: key){
            return deviceID as! String
        }else{
            if let vendorKey:String = UIDevice().identifierForVendor?.uuidString{
                appId.set(vendorKey, forKey: key)
                appId.synchronize()
                return vendorKey
            }else{
                let uuid = UUID().uuidString
                appId.set(uuid, forKey: key)
                appId.synchronize()
                return uuid
            }
        }
        
    }
    
    func pushRegistry(_ registry: PKPushRegistry, didInvalidatePushTokenFor type: PKPushType) {
        NSLog("token invalidated")
    }
    
    
    /// Show UserNotification in notification tray with the media data if available
    ///
    /// - Parameters:
    ///   - title: Title for notification
    ///   - details: Description for notification
    ///   - id: eventID to use in notification identifire
    ///   - media: media url image/video
    func showPushNotification(_ title: String, details: String, id:String, media:String,userInfo:[String : Any]) {
        let interval = TimeInterval(0.1)
        let trigger = UNTimeIntervalNotificationTrigger(timeInterval: interval, repeats: false)
        let content = UNMutableNotificationContent()
        if media.contains("https://"){
            let fileUrl = URL(string: media)
            if let url = fileUrl{
                URLSession.shared.downloadTask(with: url) { (location, response, error) in
                    if let location = location {
                        let tmpDirectory = NSTemporaryDirectory()
                        let tmpFile = "file://".appending(tmpDirectory).appending((fileUrl?.lastPathComponent)!)
                        let tmpUrl = URL(string: tmpFile)!
                        try? FileManager.default.moveItem(at: location, to: tmpUrl)
                        // Add the attachment to the notification content
                        if let attachment = try? UNNotificationAttachment(identifier: id, url: tmpUrl) {
                            content.attachments = [attachment]
                            content.title = title
                            content.body = details
                            content.userInfo = userInfo
                            if let name = UserDefaults.standard.string(forKey: "SELECTED_ALERT"){
                                if name != "Default"{
                                    content.sound = UNNotificationSound(named: "\(name).mp3")
                                }else{
                                    content.sound = UNNotificationSound.default()
                                }
                            }else{
                                content.sound = UNNotificationSound.default()
                            }
                            content.categoryIdentifier = id
                            let req = UNNotificationRequest(identifier: id, content: content, trigger: trigger)
                            let center = UNUserNotificationCenter.current()
                            center.getNotificationSettings(completionHandler: { (settings) in
                                switch settings.authorizationStatus {
                                case .notDetermined:
                                    center.requestAuthorization(options: [.alert, .sound], completionHandler: { (ok, err) in
                                        if let err = err {
                                            print(err)
                                            return
                                        }
                                        if ok {
                                            DispatchQueue.main.async {
                                                center.add(req, withCompletionHandler: nil)
                                            }
                                        }
                                    })
                                case .denied: break
                                case .authorized:
                                    DispatchQueue.main.async {
                                        center.add(req, withCompletionHandler: nil)
                                    }
                                    break
                                }
                            })
                        }
                    }
                    //TODO:
                    }.resume()
            }
        }else{
            //TODO: Change both case in comparision
            content.title = title
            content.userInfo = userInfo
            content.body = details
            if let name = UserDefaults.standard.string(forKey: "SELECTED_ALERT"){
                if name != "Default"{
                    content.sound = UNNotificationSound(named: "\(name).mp3")
                }else{
                    content.sound = UNNotificationSound.default()
                }
            }else{
                content.sound = UNNotificationSound.default()
            }
            content.categoryIdentifier = id
            let req = UNNotificationRequest(identifier: id, content: content, trigger: trigger)
            let center = UNUserNotificationCenter.current()
            center.getNotificationSettings(completionHandler: { settings in
                switch settings.authorizationStatus {
                case .notDetermined:
                    center.requestAuthorization(options: [.alert, .sound], completionHandler: { ok, err in
                        if let err = err {
                            print(err)
                            return
                        }
                        if ok {
                            DispatchQueue.main.async {
                                center.add(req, withCompletionHandler: nil)
                            }
                        }
                    })
                case .denied: break
                case .authorized:
                    DispatchQueue.main.async {
                        center.add(req, withCompletionHandler: nil)
                    }
                    break
                }
            })
        }
    }
    
}

