//
//  GenralFunctions.swift
//  SureBell_Example
//
//  Created by piOctave on 6/5/18.
//  Copyright © 2018 CocoaPods. All rights reserved.
//

import Foundation
import UIKit
import SystemConfiguration.CaptiveNetwork

class GenralFunctions: NSObject {
    
    func getHumanReadbelTime(_ date: String)->String{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        let datee = dateFormatter.date(from: date)
        dateFormatter.dateStyle = .medium
        dateFormatter.doesRelativeDateFormatting = true
        
        let timeFormatter = DateFormatter()
        timeFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        let time = timeFormatter.date(from: date)
        timeFormatter.dateFormat = "h:mm a"
        
        let timeFormated = "\(dateFormatter.string(from: datee!)), \(timeFormatter.string(from: time!))"
        return timeFormated
    }

    static func fetchSSIDInfo() -> [String] {
        var wifiInfoList = [String]()
        if let interfaces:CFArray = CNCopySupportedInterfaces() {
            for i in 0..<CFArrayGetCount(interfaces){
                let interfaceName: UnsafeRawPointer = CFArrayGetValueAtIndex(interfaces, i)
                let rec = unsafeBitCast(interfaceName, to: AnyObject.self)
                let unsafeInterfaceData = CNCopyCurrentNetworkInfo("\(rec)" as CFString)
                if unsafeInterfaceData != nil {

                    let interfaceData = unsafeInterfaceData! as Dictionary!
                    wifiInfoList.append(((interfaceData as? [String : AnyObject])?["SSID"])! as! String)

                    wifiInfoList.append(((interfaceData as? [String : AnyObject])?["BSSID"])! as! String)
                }
            }
        }
        return wifiInfoList
    }
    
}

extension String {
    var isNumeric: Bool {
        guard self.count > 0 else { return false }
        let nums: Set<Character> = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "+"]
        return Set(self).isSubset(of: nums)
    }
}







