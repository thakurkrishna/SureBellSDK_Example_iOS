
import Foundation
import UIKit

class SharedData{
    
    class AppSetting{
        static var IS_INTERNET_CONNECTED = false
    }
    
    class UserDetails{
        static var USER_ACCESS_TOKEN = "Error"
        static var USER_REFRESH_TOKEN = ""
        static var IS_USER_LOGGED_IN = false
        static var IS_USER_ADMIN = false
    }
    
    class AddDeviceFlow{
        static var ADD_DEVICE_FROM = ""
        static var ADMIN_TOKEN = ""
        static var DEVICE_TYPE = ""
        static var IS_SECONDARY_WIFI = false
        static var IS_DIGNOSTIC = false
    }
    
}

