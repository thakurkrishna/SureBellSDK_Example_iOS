//
//  DeviceDataUsageViewController.swift
//  SureExample
//
//  Created by piOctave on 6/12/18.
//  Copyright © 2018 piOctave. All rights reserved.
//

import UIKit
import SureBell

class DeviceDataUsageViewController:UIViewController {
    @IBOutlet weak var headerView: UIView!
    
    @IBOutlet weak var toDate: UITextField!
    @IBOutlet weak var fromDate: UITextField!
    @IBOutlet weak var dateUsageLabel: UILabel!
    
    var deviceID:String?
    var dataUsage = false
    var titlelbl = false

    @IBAction func click(_ sender: Any) {
        if dataUsage {
            self.dataUsageAction()
        }else {
            self.eventsAction()
        }
    }
    
    func dataUsageAction() {
        if  SharedData.AppSetting.IS_INTERNET_CONNECTED{
            if checkEmptyValidation() {
                if checkFromAndToDate() {
                    Device.instance.getDataUseByDevice(id: deviceID!, from: fromDate.text!, to: toDate.text!, callback: { (data) in
                        DispatchQueue.main.async {
                            self.dateUsageLabel.text = self.prettyBytes(numBytes: data.totalImageSize! + data.totalVideoSize!)
                            self.dateUsageLabel.textColor = UIColor.red
                        }
                    }, failure: { (error) in
                        if let error =  error as? APIError{
                            guard  let des = error.error_description else{return}
                            print(des)
                        }else if let error =  error as? Error{
                            print(error.localizedDescription)
                        }
                    })
                }
            }
        }else{
            self.displayMyAlertMessage(userMessage: "No Internet Connection")
        }
    }
    
    func eventsAction() {
        if  SharedData.AppSetting.IS_INTERNET_CONNECTED{
            if checkEmptyValidation() {
                if checkFromAndToDate() {
                    let dateFormatter = DateFormatter()
                    dateFormatter.dateFormat = "yyyy-MM-dd"
                    let fdateObj = dateFormatter.date(from: fromDate.text!)
                    let tdateObj = dateFormatter.date(from: toDate.text!)

                    let fromDte = (Calendar.current as NSCalendar)
                        .date(byAdding: .day,value: 1,to: fdateObj!,options: [])
                    let toDte = (Calendar.current as NSCalendar)
                        .date(byAdding: .day,value: 1,to: tdateObj!,options: [])

                    Device.instance.getEventsByDate(deviceID!, from: fromDte!, to: toDte!, offset: 0, callback: { (events) in
                        DispatchQueue.main.async {
                            let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
                            let controller: EventListViewController = storyboard.instantiateViewController(withIdentifier: "EventListViewController") as! EventListViewController
                            
                            if let deviceID = self.deviceID{
                                controller.deviceID = deviceID
                                controller.events = events
                                controller.geteventsbydate = true
                            }
                            self.navigationController?.pushViewController(controller, animated: true)
                        }
                    }, failure: { (error) in
                        if let error =  error as? APIError{
                            guard  let des = error.error_description else{return}
                            print(des)
                        }else if let error =  error as? Error{
                            print(error.localizedDescription)
                        }
                    })
                }
            }
        }else{
            self.displayMyAlertMessage(userMessage: "No Internet Connection")
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let customBackButton = UIBarButtonItem(image: UIImage(named: "back") , style: .plain, target: self, action: #selector(backAction(sender:)))
        customBackButton.imageInsets = UIEdgeInsets(top: 2, left: -8, bottom: 0, right: 0)
        navigationItem.leftBarButtonItem = customBackButton
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        navigationController?.navigationBar.barStyle = UIBarStyle.black
        navigationController?.navigationBar.tintColor = UIColor.white
        
        if titlelbl {
            self.navigationItem.title = "Device Data Usage"
            self.dateUsageLabel.isHidden = false
        }else {
            self.navigationItem.title = "Events By Date"
            self.dateUsageLabel.isHidden = true
        }
    }
    
    func prettyBytes(numBytes: Int) -> String {
        let scale: Double = 1024
        let abbrevs = [ "EB", "PB", "TB", "GB", "MB", "KB", "Bytes"]
        let numAbbrevs = abbrevs.count
        
        let bytes = Double(numBytes)
        var maximum = pow(Double(scale), Double(numAbbrevs - 1))
        
        for i in 0..<numAbbrevs {
            if bytes > maximum {
                return String(format: "%0.2f %@", arguments: [bytes / maximum, abbrevs[i]])
            }
            maximum /= scale
        }
        return "\(numBytes) Bytes"
    }

    @objc func backAction(sender: UIBarButtonItem) {
        navigationController?.popViewController(animated: true)
    }
    
    func displayMyAlertMessage(userMessage:String){
        let myAlert = UIAlertController(title: "Alert", message: userMessage, preferredStyle: UIAlertControllerStyle.alert)
        
        let okAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil)
        
        myAlert.addAction(okAction)
        self.present(myAlert, animated: true, completion: nil)
    }
    
    func checkEmptyValidation() -> Bool{
        var message:String = ""
        if fromDate!.text == "" || (fromDate!.text?.isEmpty)!{
            message = "Enter From Date"
            displayMyAlertMessage(userMessage: message)
            return false
        }else if toDate!.text == "" || (toDate!.text?.isEmpty)!{
            message = "Enter To Date"
            displayMyAlertMessage(userMessage: message)
            return false
        }else{
            return true
        }
    }

    func checkFromAndToDate() -> Bool{
        var message:String = ""
        if fromDate!.text! > toDate!.text! {
            message = "From date must to greater than To date"
            displayMyAlertMessage(userMessage: message)
            return false
        }else{
            return true
        }
    }

}
