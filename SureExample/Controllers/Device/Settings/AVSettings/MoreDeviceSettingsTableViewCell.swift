//
//  MoreDeviceSettingsTableViewCell.swift
//  SureExample
//
//  Created by piOctave on 6/8/18.
//  Copyright © 2018 piOctave. All rights reserved.
//

import UIKit

class MoreDeviceSettingsTableViewCell: UITableViewCell {
    
    @IBOutlet weak var segment: UISegmentedControl!
    
    @IBOutlet weak var title: UILabel!
    
    var sliderChangedValueDel : (() -> Void)? = nil
    
    override func awakeFromNib() {
        super.awakeFromNib()
        segment.tintColor = UIColor.darkGray
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    @IBAction func avConfigSliderValue(_ sender: Any) {
        if let sliderChangedValue = self.sliderChangedValueDel{
            sliderChangedValue()
        }
    }
    
}

