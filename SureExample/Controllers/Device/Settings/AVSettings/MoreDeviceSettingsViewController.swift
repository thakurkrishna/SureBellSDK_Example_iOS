//
//  MoreDeviceSettingsViewController.swift
//  SureExample
//
//  Created by piOctave on 6/8/18.
//  Copyright © 2018 piOctave. All rights reserved.
//

import UIKit
import SureBell

class MoreDeviceSettingsViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var hedaerView: UIView!
    
    typealias ItemInfo = (title: String, value: Int)
    fileprivate var items = [ItemInfo]()
    var speakerVolume = 2
    var micGain = 2
    var videoRecordingQuality = 0
    var videoStreamingQuality = 1
    var deviceID:String?

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let customBackButton = UIBarButtonItem(image: UIImage(named: "back") , style: .plain, target: self, action: #selector(backAction(sender:)))
        customBackButton.imageInsets = UIEdgeInsets(top: 2, left: -8, bottom: 0, right: 0)
        navigationItem.leftBarButtonItem = customBackButton

        items.append(("Speaker Volume",1))
        items.append(("Mic Gain",1))
        items.append(("Video Recording Quality",1))
        items.append(("Video Streaming Quality",1))
        
        tableView.dataSource = self
        tableView.delegate = self
        
        setSettingsValue()
    }
    
    @objc func backAction(sender: UIBarButtonItem) {
        navigationController?.popViewController(animated: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        navigationController?.navigationBar.barStyle = UIBarStyle.black
        navigationController?.navigationBar.tintColor = UIColor.white
        self.navigationItem.title = "AV Settings"
    }
    
    fileprivate func setSettingsValue() {
        if let deviceID = self.deviceID{
            Device.instance.getDevice(id: deviceID, callback: { (device) in
                DispatchQueue.main.async {
                if let settings = device.settings{
                    if let speakerVolumeValue = settings.audio?.speaker_vol{
                        self.speakerVolume = speakerVolumeValue
                        self.items[0] = ("Speaker Volume",speakerVolumeValue)
                    }else{
                        self.items[0] = ("Speaker Volume",self.speakerVolume)
                    }
                    
                    if let micGainValue = settings.audio?.mic_gain{
                        self.micGain = micGainValue
                        self.items[1] = ("Mic Gain",micGainValue)
                    }else{
                        self.items[1] = ("Mic Gain",self.micGain)
                    }
                    
                    if let videoRecordingQualityValue = settings.video?.recording?.quality{
                        self.videoRecordingQuality = videoRecordingQualityValue
                        self.items[2] = ("Video Recording Quality",videoRecordingQualityValue)
                    }else{
                        self.items[2] = ("Video Recording Quality",self.videoRecordingQuality)
                    }
                    
                    if let videoStreamingQualityValue = settings.video?.live?.quality{
                        self.videoStreamingQuality = videoStreamingQualityValue
                        self.items[3] = ("Video Streaming Quality",videoStreamingQualityValue)
                    }else{
                        self.items[3] = ("Video Streaming Quality",self.videoStreamingQuality)
                    }
                    self.tableView.reloadData()
                }
            }
            }, failure: { (error) in
                if let error =  error as? APIError{
                    guard  let des = error.error_description else{return}
                    print(des)
                }else if let error =  error as? Error{
                    print(error.localizedDescription)
                }
            })
        }
    }

    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! MoreDeviceSettingsTableViewCell
        if items.count > 0{
            cell.title.text =  items[indexPath.row].title
            if items[indexPath.row].title == "Video Streaming Quality"{
                let item = ["AUTO", "LOW", "MED","HIGH"]
                cell.segment.replaceSegments(segments: item)
                cell.segment.selectedSegmentIndex = items[indexPath.row].value
            }else if items[indexPath.row].title == "Video Recording Quality"{
                let item = ["AUTO", "LOW", "MED","HIGH"]
                cell.segment.replaceSegments(segments: item)
                cell.segment.selectedSegmentIndex = items[indexPath.row].value
            }else{
                let item = ["LOW", "MED","HIGH"]
                cell.segment.replaceSegments(segments: item)
                cell.segment.selectedSegmentIndex = items[indexPath.row].value - 1
            }
            cell.sliderChangedValueDel =
                {
                self.updateDeviceConfig(cell.title.text!, value: cell.segment.selectedSegmentIndex + 1)
                }
        }
        return cell
    }

    func updateDeviceConfig(_ configType:String,value:Int){
        
        if configType == "Speaker Volume" {
            let audio = Audio()
            audio.speaker_vol = value
            audio.mic_gain = self.micGain
            
            Device.instance.audioSettings(id: self.deviceID!, audio: audio, callback: { (device) in
                DispatchQueue.main.async {
                    self.speakerVolume = (device.settings?.audio?.speaker_vol)!
                }
            }, failure: { (error) in
                if let error =  error as? APIError{
                    guard  let des = error.error_description else{return}
                    print(des)
                }else if let error =  error as? Error{
                    print(error.localizedDescription)
                }
            })
        } else if configType == "Mic Gain" {
            let audio = Audio()
            audio.mic_gain = value
            audio.speaker_vol = self.speakerVolume
            
            Device.instance.audioSettings(id: self.deviceID!, audio: audio, callback: { (device) in
                DispatchQueue.main.async {
                    self.micGain = (device.settings?.audio?.mic_gain)!
                }
            }, failure: { (error) in
                if let error =  error as? APIError{
                    guard  let des = error.error_description else{return}
                    print(des)
                }else if let error =  error as? Error{
                    print(error.localizedDescription)
                }
            })
        } else if configType == "Video Streaming Quality" {
            let recording = Recording()
            let live = Live()
            
            live.quality = value - 1
            recording.quality = self.videoRecordingQuality
            
            Device.instance.videoSettings(id: self.deviceID!, recording: recording, live: live, callback: { (device) in
                DispatchQueue.main.async {
                    self.videoRecordingQuality = (device.settings?.video?.recording?.quality)!
                }
            }, failure: { (error) in
                if let error =  error as? APIError{
                    guard  let des = error.error_description else{return}
                    print(des)
                }else if let error =  error as? Error{
                    print(error.localizedDescription)
                }
            })
        }else if configType == "Video Recording Quality" {
            let recording = Recording()
            let live = Live()
            
            recording.quality = value - 1
            live.quality = self.videoStreamingQuality
            
            Device.instance.videoSettings(id: self.deviceID!, recording: recording, live: live, callback: { (device) in
                DispatchQueue.main.async {
                    self.videoStreamingQuality = (device.settings?.video?.live?.quality)!
                }
            }, failure: { (error) in
                if let error =  error as? APIError{
                    guard  let des = error.error_description else{return}
                    print(des)
                }else if let error =  error as? Error{
                    print(error.localizedDescription)
                }
            })
        }
    }
    
}

extension UISegmentedControl {
    func replaceSegments(segments: Array<String>) {
        self.removeAllSegments()
        for segment in segments {
            self.insertSegment(withTitle: segment, at: self.numberOfSegments, animated: false)
        }
    }
}
