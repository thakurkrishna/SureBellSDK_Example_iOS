//
//  SettingTableViewCell.swift
//  SureExample
//
//  Created by piOctave on 6/7/18.
//  Copyright © 2018 piOctave. All rights reserved.
//

import UIKit

class SettingTableViewCell: UITableViewCell {
    
    @IBOutlet weak var title: UILabel!

    @IBOutlet weak var `switch`: UISwitch!
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}

