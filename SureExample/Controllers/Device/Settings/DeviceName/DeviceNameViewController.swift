//
//  DeviceNameViewController.swift
//  SureBell_Example
//
//  Created by piOctave on 6/5/18.
//  Copyright © 2018 CocoaPods. All rights reserved.
//

import UIKit
import SureBell

class DeviceNameViewController: UIViewController {
    @IBOutlet weak var devicename: UITextField!
    
    @IBOutlet weak var done: UIButton!
    @IBAction func done(_ sender: Any) {
        if devicename.text != "" {
        done.addTarget(self, action: #selector(doneAction), for: .touchUpInside)
        }else {
            self.displayMyAlertMessage(userMessage: "Device name is empty")
        }
    }
    
    func displayMyAlertMessage(userMessage:String){
        let myAlert = UIAlertController(title: "Alert", message: userMessage, preferredStyle: UIAlertControllerStyle.alert)
        
        let okAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil)
        
        myAlert.addAction(okAction)
        self.present(myAlert, animated: true, completion: nil)
    }
    
    @objc func doneAction() {
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        let controller: LocationViewController = storyboard.instantiateViewController(withIdentifier: "LocationViewController") as! LocationViewController
        
        controller.devicename = self.devicename.text
        self.navigationController?.pushViewController(controller, animated: false)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let customBackButton = UIBarButtonItem(image: UIImage(named: "back") , style: .plain, target: self, action: #selector(backAction(sender:)))
        customBackButton.imageInsets = UIEdgeInsets(top: 2, left: -8, bottom: 0, right: 0)
        navigationItem.leftBarButtonItem = customBackButton
    }
    
    @objc func backAction(sender: UIBarButtonItem) {
        navigationController?.popViewController(animated: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        navigationController?.navigationBar.barStyle = UIBarStyle.black
        navigationController?.navigationBar.tintColor = UIColor.white
        self.navigationItem.title = "Add New Device"
    }
    
}
