//
//  EventDetailTableViewCell.swift
//  SureExample
//
//  Created by piOctave on 6/11/18.
//  Copyright © 2018 piOctave. All rights reserved.
//

import UIKit
import SureBell

class EventDetailTableViewCell: UITableViewCell {
    
    
    @IBOutlet weak var dev_img: UIImageView!
    @IBOutlet weak var dev_name: UILabel!
    @IBOutlet weak var time: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }

}
