//
//  EventListViewController.swift
//  SureExample
//
//  Created by piOctave on 6/11/18.
//  Copyright © 2018 piOctave. All rights reserved.
//

import UIKit
import SureBell
import AVKit
import AVFoundation
import UserNotifications

class EventListViewController:UIViewController,UITableViewDelegate, UITableViewDataSource {
    @IBOutlet weak var headerView: UIView!

    @IBOutlet weak var tableView: UITableView!

    var events = [Event]()
    let dateParser = GenralFunctions()
    var deviceID:String?
    var deviceName = ""
    var identifiers: [String] = []
    var geteventsbydate = false
    var eventID = ""
    var videoShare = ""
    var eventstatus = "missed"

    @IBOutlet weak var eventsView: UIView!
    @IBOutlet weak var getEvents: UIButton!
    
    @IBAction func getEventsAction(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        let controller: DeviceDataUsageViewController = storyboard.instantiateViewController(withIdentifier: "DeviceDataUsageViewController") as! DeviceDataUsageViewController
        
        if let deviceID = self.deviceID{
            controller.deviceID = deviceID
        }
        self.navigationController?.pushViewController(controller, animated: true)
    }
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let customBackButton = UIBarButtonItem(image: UIImage(named: "back") , style: .plain, target: self, action: #selector(backAction(sender:)))
        customBackButton.imageInsets = UIEdgeInsets(top: 2, left: -8, bottom: 0, right: 0)
        navigationItem.leftBarButtonItem = customBackButton
        
//        UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .badge, .sound]) { (granted, error) in
//            if granted {
//                print("Notification access granted")
//            }else{
//                print(error?.localizedDescription)
//            }
//        }
    }
    
    @objc func backAction(sender: UIBarButtonItem) {
        navigationController?.popViewController(animated: true)
//        scheduleNotification(inSeconds: 5) { (success) in
//            if success {
//                print("Success")
//            }else{
//                print("Error")
//            }
//        }
    }

//    func scheduleNotification(inSeconds: TimeInterval, completion: @escaping (_ Success: Bool) ->()){
//        let notif = UNMutableNotificationContent()
//
//        notif.title = "New Notification"
//        notif.subtitle = "Events"
//        notif.body = "Hi hello good morning"
//
//        let notifTrigger = UNTimeIntervalNotificationTrigger(timeInterval: inSeconds, repeats: false)
//
//        let request = UNNotificationRequest(identifier: "myNotification", content: notif, trigger: notifTrigger)
//
//        UNUserNotificationCenter.current().add(request) { (error) in
//            if error != nil{
//                print(error)
//                completion(false)
//            }else{
//                completion(true)
//            }
//
//        }
//    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
        navigationController?.navigationBar.barStyle = UIBarStyle.black
        navigationController?.navigationBar.tintColor = UIColor.white
        self.navigationItem.title = "Events"

        if geteventsbydate {
            if !self.events.isEmpty {
                self.tableView.dataSource = self
                self.tableView.delegate = self
                self.tableView.reloadData()
            }else {
                self.displayMyAlertMessage(userMessage: "No events found for the selected date")
            }
        }else {
            fetchList()
        }
        
//        User.instance.updateNotificationToken(token: "Notification", device: deviceID!, callback: { (user) in
//
//        }) { (error) in
//            if let error =  error as? APIError{
//                guard  let des = error.error_description else{return}
//                print(des)
//            }else if let error =  error as? Error{
//                print(error.localizedDescription)
//            }
//
//        }
    }

    func displayMyAlertMessage(userMessage:String){
        let myAlert = UIAlertController(title: "Alert", message: userMessage, preferredStyle: UIAlertControllerStyle.alert)
        
        let okAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil)
        
        myAlert.addAction(okAction)
        self.present(myAlert, animated: true, completion: nil)
    }

    func fetchList(){
        Device.instance.getEvents(id: deviceID!, callback: { (events) in
            self.events.removeAll()
            self.events = events
            DispatchQueue.main.async {
                self.tableView.dataSource = self
                self.tableView.delegate = self
                self.tableView.reloadData()
            }

        }) { (error) in
            if let error =  error as? APIError{
                guard  let des = error.error_description else{return}
                print(des)
            }else if let error =  error as? Error{
                print(error.localizedDescription)
            }
        }
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if events.count < 1{
            tableView.backgroundView = view
            tableView.separatorColor = UIColor.clear
        }else{
            tableView.backgroundView = nil
            tableView.separatorColor = UIColor.lightGray
        }
        return events.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! EventDetailTableViewCell
        let eventDate = events[indexPath.row].createdAt
        
        if self.events[indexPath.row].type == "motion"{
            if self.events[indexPath.row].status == "missed"{
                cell.dev_name.textColor = UIColor.red
                cell.dev_name.text = "Motion Alert"
                
                self.eventID = self.events[indexPath.row]._id!
                self.videoShare = self.events[indexPath.row].video!
                
                cell.dev_img.image = UIImage(named: "Events")
                cell.time.text = self.dateParser.getHumanReadbelTime(eventDate!)
                
            }else{
                cell.dev_name.textColor = UIColor.green
                cell.dev_name.text = "Motion Alert"
                
                self.eventID = self.events[indexPath.row]._id!
                self.videoShare = self.events[indexPath.row].video!

                cell.dev_img.image = UIImage(named: "Events")
                cell.time.text = dateParser.getHumanReadbelTime(eventDate!)
                
            }
        }else{
            if self.events[indexPath.row].status == "missed"{
                cell.dev_name.textColor = UIColor.red
                cell.dev_name.text = "Bell Ring"
                
                self.eventID = self.events[indexPath.row]._id!
                self.videoShare = self.events[indexPath.row].video!
                
                cell.dev_img.image = UIImage(named: "Events")
                cell.time.text = dateParser.getHumanReadbelTime(eventDate!)
                
            }else{
                cell.dev_name.textColor = UIColor.black
                cell.dev_name.text = "Bell Ring"
                
                self.eventID = self.events[indexPath.row]._id!
                self.videoShare = self.events[indexPath.row].video!
                
                cell.dev_img.image = UIImage(named: "Events")
                cell.time.text = dateParser.getHumanReadbelTime(eventDate!)
                
            }
        }
        
        if let id = events[indexPath.row]._id{
            self.identifiers.append(id)
            UNUserNotificationCenter.current().removeDeliveredNotifications(withIdentifiers: self.identifiers)
        }

        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath) as! EventDetailTableViewCell
        
        if cell.dev_name.text == "Motion Alert" || cell.dev_name.text == "Bell Ring"{
            if SharedData.UserDetails.IS_USER_ADMIN{
        let message = ""

        // Create the dialog
        let myAlert = UIAlertController(title: message, message: "", preferredStyle: UIAlertControllerStyle.actionSheet)
        
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.default, handler: nil)
        myAlert.addAction(cancelAction)
        self.present(myAlert, animated: true, completion: nil)
        
        myAlert.addAction(UIAlertAction(title: "Delete", style: UIAlertActionStyle.destructive, handler: { action in
            DispatchQueue.main.async {
                
                Device.instance.deleteEvent(device_id: self.deviceID!, event_id: self.eventID, callback: { (sr) in
                    DispatchQueue.main.async {
                        self.displayMyAlertMessage(userMessage: "Deleted event")
                        
                        self.fetchList()
                    }
                }, failure: { (error) in
                    if let error =  error as? APIError{
                        guard  let des = error.error_description else{return}
                        print(des)
                    }else if let error =  error as? Error{
                        print(error.localizedDescription)
                    }

                })
            }
        }))
        
        
        myAlert.addAction(UIAlertAction(title: "Share", style: UIAlertActionStyle.default, handler: { action in
            DispatchQueue.main.async {
                Device.instance.shareEvent(event_id: self.eventID, shared: true, callback: { (event) in
                    DispatchQueue.main.async {
                        self.fetchList()

                        let url = URL(string: self.videoShare)
                        
                        let activityController = UIActivityViewController(activityItems: [url!], applicationActivities: nil)
                        self.present(activityController, animated: true, completion: nil)
                        
                    }
                }, failure: { (error) in
                    if let error =  error as? APIError{
                        guard  let des = error.error_description else{return}
                        print(des)
                    }else if let error =  error as? Error{
                        print(error.localizedDescription)
                    }

                })
            }
        }))
            
            myAlert.addAction(UIAlertAction(title: "Play", style: UIAlertActionStyle.default, handler: { action in
                DispatchQueue.main.async {
                
                    Device.instance.getEvent(device_id: self.deviceID!, event_id: self.eventID, callback: { (event) in
                        self.eventstatus = "attended"
                        event.status = self.eventstatus
                        DispatchQueue.main.async {
                            Device.instance.changeEventStatus(event_id: self.eventID, status: event.status!, callback: { (event) in
                                DispatchQueue.main.async {
                                    
                                    let videoURL = NSURL(string: self.videoShare)
                                    let player = AVPlayer(url: videoURL! as URL)
                                    let playerViewController = AVPlayerViewController()
                                    playerViewController.player = player
                                    self.present(playerViewController, animated: true) {
                                        playerViewController.player!.play()
                                    }
                                }
                            }, failure: { (error) in
                                if let error =  error as? APIError{
                                    guard  let des = error.error_description else{return}
                                    print(des)
                                }else if let error =  error as? Error{
                                    print(error.localizedDescription)
                                }
                            })
                            
                            
                        }
                    }, failure: { (error) in
                        if let error =  error as? APIError{
                            guard  let des = error.error_description else{return}
                            print(des)
                        }else if let error =  error as? Error{
                            print(error.localizedDescription)
                        }
                    })
                }
            }))

            }else {
                let message = ""

                // Create the dialog
                let myAlert = UIAlertController(title: message, message: "", preferredStyle: UIAlertControllerStyle.actionSheet)
                
                let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.default, handler: nil)
                myAlert.addAction(cancelAction)
                self.present(myAlert, animated: true, completion: nil)
                
                myAlert.addAction(UIAlertAction(title: "Play", style: UIAlertActionStyle.default, handler: { action in
                    DispatchQueue.main.async {
                        
                        Device.instance.getEvent(device_id: self.deviceID!, event_id: self.eventID, callback: { (event) in
                            self.eventstatus = "attended"
                            event.status = self.eventstatus
                            DispatchQueue.main.async {
                                Device.instance.changeEventStatus(event_id: self.eventID, status: event.status!, callback: { (event) in
                                    DispatchQueue.main.async {
                                        let videoURL = NSURL(string: self.videoShare)
                                        let player = AVPlayer(url: videoURL! as URL)
                                        let playerViewController = AVPlayerViewController()
                                        playerViewController.player = player
                                        self.present(playerViewController, animated: true) {
                                            playerViewController.player!.play()
                                        }
                                    }
                                }, failure: { (error) in
                                    if let error =  error as? APIError{
                                        guard  let des = error.error_description else{return}
                                        print(des)
                                    }else if let error =  error as? Error{
                                        print(error.localizedDescription)
                                    }
                                })
                                
                                
                            }
                        }, failure: { (error) in
                            if let error =  error as? APIError{
                                guard  let des = error.error_description else{return}
                                print(des)
                            }else if let error =  error as? Error{
                                print(error.localizedDescription)
                            }
                        })
                    }
                }))
            }
    }else {
            
        }
    }

}

