//
//  ShareViewController.swift
//  SureExample
//
//  Created by piOctave on 6/12/18.
//  Copyright © 2018 piOctave. All rights reserved.
//

import UIKit
import SureBell

class ShareViewController:UIViewController,UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate, UIPopoverPresentationControllerDelegate, CountrySelected {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var searchbar: UITextField!
    @IBOutlet weak var searchView: UIView!
    @IBOutlet weak var userView: UIView!
    @IBOutlet weak var users: UIButton!
    @IBOutlet weak var admins: UIButton!
    @IBOutlet weak var invites: UIButton!
    
    @IBAction func usersAction(_ sender: Any) {
        fetchUser()
    }
    @IBAction func adminsAction(_ sender: Any) {
        fetchAdmin()
    }
    
    @IBAction func invitesAction(_ sender: Any) {
        fetchInvites()
    }
    
    var usersArr = [Users]()
    var adminsArr = [Admins]()
    var deviceList = [Device]()

    var sendButton = UIButton()

    var deviceID:String?
    var invitesArr = [String]()
    var email = ""
    var userId = ""
    var selectedCountryCode = "+91"
    var leftTextPhone = UILabel()
    let leftViewPhone = UIView()
    var user = ""

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let customBackButton = UIBarButtonItem(image: UIImage(named: "back") , style: .plain, target: self, action: #selector(backAction(sender:)))
        customBackButton.imageInsets = UIEdgeInsets(top: 2, left: -8, bottom: 0, right: 0)
        navigationItem.leftBarButtonItem = customBackButton
        
        self.searchbar.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        navigationController?.navigationBar.barStyle = UIBarStyle.black
        navigationController?.navigationBar.tintColor = UIColor.white
        self.navigationItem.title = "Manage Users"
        
        self.sendButton.setTitle("Send", for: .normal)
        searchbar.rightViewMode = .always
        
        sendButton.tintColor = UIColor.darkGray
        sendButton.contentMode = .center
        sendButton.addTarget(self, action: #selector(self.sendInvite(_:)), for: UIControlEvents.touchUpInside)
        sendButton.frame = CGRect(x: 0, y: 0, width: 50, height: 30)
        sendButton.titleLabel?.font =  UIFont(name: "Clan-Bold", size: 13)
        sendButton.setTitle("Send", for: .normal)
        sendButton.contentHorizontalAlignment = .right
        sendButton.setTitleColor(UIColor.black, for: .normal)
        searchbar.rightView = sendButton
        
        fetchDevices()
        
        tableView.dataSource = self
        tableView.delegate = self
        tableView.reloadData()
    }

    @objc func backAction(sender: UIBarButtonItem) {
        navigationController?.popViewController(animated: true)
    }
    
    func showCountryCodeView(){
        if selectedCountryCode.contains("+"){
            leftTextPhone.text = IsoCountryCodes.searchByCallingCode(calllingCode: selectedCountryCode).flag + selectedCountryCode
        }else{
            leftTextPhone.text = "Country Code"
        }
        leftTextPhone.isUserInteractionEnabled = true
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.tapFunction(_:)))
        leftTextPhone.addGestureRecognizer(tap)
        leftTextPhone.font = UIFont(name: "ClanOT-News", size: 17)
        
        leftViewPhone.addSubview(leftTextPhone)
        
        leftViewPhone.frame = CGRect(x: 0, y: 0, width: 80, height: 40)
        leftTextPhone.frame = CGRect(x: 5, y: 10, width: 80, height: 20)
        searchbar.leftViewMode = .always
        searchbar.leftView = leftViewPhone
    }
    
    func hideCountryCode(){
        leftTextPhone.removeFromSuperview()
        searchbar.leftViewMode = .never
        searchbar.leftView?.removeFromSuperview()
        let indentView = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: 20))
        searchbar.leftView = indentView
        searchbar.leftViewMode = .always
    }

    func dismissKeyboard() {
        view.endEditing(true)
    }

    @objc func tapFunction(_ sender:UITapGestureRecognizer) {
        self.dismissKeyboard()
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        guard let settingsViewController = storyboard.instantiateViewController(withIdentifier: "CountrySelectionTableViewController") as? CountrySelectionTableViewController else {
            return
        }
        settingsViewController.delegate = self
        let barButtonItem = UIBarButtonItem(image: UIImage(named: "close"),style: .done, target: self, action: #selector(dismissSettings))
        
        settingsViewController.navigationItem.rightBarButtonItem = barButtonItem
        settingsViewController.title = "Select a Country"
        
        let navigationController = UINavigationController(rootViewController: settingsViewController)
        navigationController.navigationBar.tintColor = UIColor.darkGray
        navigationController.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor : UIColor.darkGray];
        navigationController.modalPresentationStyle = .popover
        navigationController.popoverPresentationController?.delegate = self
        navigationController.preferredContentSize = CGSize(width: self.view.bounds.size.width - 10, height: self.view.bounds.size.height - 60)
        self.present(navigationController, animated: true, completion: nil)
        navigationController.popoverPresentationController?.sourceView = leftTextPhone
        navigationController.popoverPresentationController?.backgroundColor = UIColor.white
        navigationController.popoverPresentationController?.sourceRect = leftTextPhone.bounds
    }
    
    @objc
    func dismissSettings() {
        self.dismiss(animated: true, completion: nil)
        //TODO
    }

    func sendData(countryCode: String, counrtyName: String, countryFlag: String) {
        self.leftTextPhone.text = countryFlag + " " + countryCode
        self.selectedCountryCode = countryCode
    }

    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if let text = textField.text{
            if text.isNumeric{
                DispatchQueue.main.async {
                    self.showCountryCodeView()
                }
            }else{
                DispatchQueue.main.async {
                    self.hideCountryCode()
                }
            }
        }else{
            DispatchQueue.main.async {
                self.hideCountryCode()
            }
        }
        return true
    }

    func textFieldShouldReturn(_ userText: UITextField) -> Bool {
        if userText == self.searchbar {
        }
        return true
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var count = Int ()
        if usersArr.count != 0 {
            count = usersArr.count
        }else if adminsArr.count != 0 {
            count = adminsArr.count
        }else if invitesArr.count != 0 {
            count = invitesArr.count
        }else {
            return 0
        }
        return count
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! ShareTableViewCell
        
        if usersArr.count != 0 {
            if usersArr[indexPath.row].email! != ""{
                let userid = usersArr[indexPath.row]
                self.userId = userid._id!
                self.email = userid.email!

                cell.person_name.text = usersArr[indexPath.row].email!
            }else{
                cell.person_name.text = "No Users"
            }
        }else if adminsArr.count != 0 {
            if adminsArr[indexPath.row].email! != ""{
                let adminid = adminsArr[indexPath.row]
                self.userId = adminid._id!
                self.email = adminid.email!
                
                cell.person_name.text = adminsArr[indexPath.row].email!
            }else{
                cell.person_name.text = "No Admins"
            }
        }else if invitesArr.count != 0 {
            if invitesArr[indexPath.row] != ""{
                let useremail = invitesArr[indexPath.row]
                self.email = useremail
                
                cell.person_name.text = invitesArr[indexPath.row]
                cell.person_des.text = "User Request Sent"
            }else{
                cell.person_name.text = "No Invites"
            }
        }else {

        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! ShareTableViewCell
        
        if cell.person_name.text != ""{
            
            if adminsArr.count != 0 {
                let message = "Are you sure, you want to revoke admin access"
                
                // Create the dialog
                let myAlert = UIAlertController(title: message, message: "", preferredStyle: UIAlertControllerStyle.actionSheet)
                
                let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.default, handler: nil)
                myAlert.addAction(cancelAction)
                self.present(myAlert, animated: true, completion: nil)
                
                myAlert.addAction(UIAlertAction(title: "Make User", style: UIAlertActionStyle.destructive, handler: { action in
                    DispatchQueue.main.async {
                        Device.instance.removeAdmin(id: self.deviceID!, user: self.userId, callback: { (device) in
                            DispatchQueue.main.async {
                                self.displayMyAlertMessage(userMessage: "Revoked Admin access to \(self.email)")
                                self.fetchAdmin()
                            }
                        }, failure: { (error) in
                            if let error =  error as? APIError{
                                guard  let des = error.error_description else{return}
                                print(des)
                            }else if let error =  error as? Error{
                                print(error.localizedDescription)
                            }
                        })
                    }
                }))
            }else if usersArr.count != 0 {
                let message = "Select any action"
                
                // Create the dialog
                let myAlert = UIAlertController(title: message, message: "", preferredStyle: UIAlertControllerStyle.actionSheet)
                
                let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.default, handler: nil)
                myAlert.addAction(cancelAction)
                self.present(myAlert, animated: true, completion: nil)
                
                myAlert.addAction(UIAlertAction(title: "Make Admin", style: UIAlertActionStyle.destructive, handler: { action in
                    DispatchQueue.main.async {
                        if self.isInviteExistInAdminList(invite: self.email) {
                            self.displayMyAlertMessage(userMessage: "\(self.email) already admin for this device")
                        }else{
                            Device.instance.inviteAdmin(id: self.deviceID!, user: self.userId, callback: { (device) in
                                DispatchQueue.main.async {
                                    self.displayMyAlertMessage(userMessage: "Granted Admin access to \(self.email)")
                                    self.fetchUser()
                                }
                            }, failure: { (error) in
                                if let error =  error as? APIError{
                                    guard  let des = error.error_description else{return}
                                    print(des)
                                }else if let error =  error as? Error{
                                    print(error.localizedDescription)
                                }
                            })
                        }
                    }
                }))
                
                myAlert.addAction(UIAlertAction(title: "Remove User", style: UIAlertActionStyle.destructive, handler: { action in
                    DispatchQueue.main.async {
                        Device.instance.removeUser(id: self.deviceID!, user: self.userId, callback: { (device) in
                            DispatchQueue.main.async {
                                self.fetchUser()
                            }
                        }, failure: { (error) in
                            if let error =  error as? APIError{
                                guard  let des = error.error_description else{return}
                                print(des)
                            }else if let error =  error as? Error{
                                print(error.localizedDescription)
                            }
                        })
                    }
                }))
            } else if invitesArr.count != 0 {
                
                let message = "Are you sure, you want to remove invited user"
                
                // Create the dialog
                let myAlert = UIAlertController(title: message, message: "", preferredStyle: UIAlertControllerStyle.actionSheet)
                
                let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.default, handler: nil)
                myAlert.addAction(cancelAction)
                self.present(myAlert, animated: true, completion: nil)
                
                myAlert.addAction(UIAlertAction(title: "Remove Invite", style: UIAlertActionStyle.destructive, handler: { action in
                    DispatchQueue.main.async {
                        Device.instance.removeInvite(id: self.deviceID!, user: self.email, callback: { (device) in
                            print(self.email)
                            DispatchQueue.main.async {
                                self.fetchInvites()
                            }
                        }, failure: { (error) in
                            if let error =  error as? APIError{
                                guard  let des = error.error_description else{return}
                                print(des)
                            }else if let error =  error as? Error{
                                print(error.localizedDescription)
                            }
                        })
                    }
                }))
            }
        }
        
    }
    
    func fetchUser(){
        Device.instance.getDevice(id: deviceID!, callback: { (device) in
            self.usersArr = device.users!
            self.adminsArr.removeAll()
            self.invitesArr.removeAll()
            
            DispatchQueue.main.async {
                self.tableView.dataSource = self
                self.tableView.delegate = self
                self.tableView.reloadData()
            }
        }) { (error) in
            if let error =  error as? APIError{
                guard  let des = error.error_description else{return}
                print(des)
            }else if let error =  error as? Error{
                print(error.localizedDescription)
            }
        }
    }
    
    func fetchAdmin(){
        Device.instance.getDevice(id: deviceID!, callback: { (device) in
            self.adminsArr = device.admins!
            self.usersArr.removeAll()
            self.invitesArr.removeAll()
            
            DispatchQueue.main.async {
                self.tableView.dataSource = self
                self.tableView.delegate = self
                self.tableView.reloadData()
            }
        }) { (error) in
            if let error =  error as? APIError{
                guard  let des = error.error_description else{return}
                print(des)
            }else if let error =  error as? Error{
                print(error.localizedDescription)
            }
        }
    }
    
    func fetchInvites(){
        Device.instance.getDevice(id: deviceID!, callback: { (device) in
            self.invitesArr = device.invites!
            self.adminsArr.removeAll()
            self.usersArr.removeAll()
    
            DispatchQueue.main.async {
                self.tableView.dataSource = self
                self.tableView.delegate = self
                self.tableView.reloadData()
            }
        }) { (error) in
            if let error =  error as? APIError{
                guard  let des = error.error_description else{return}
                print(des)
            }else if let error =  error as? Error{
                print(error.localizedDescription)
            }
        }
    }
    
    func displayMyAlertMessage(userMessage:String){
        let myAlert = UIAlertController(title: "Alert", message: userMessage, preferredStyle: UIAlertControllerStyle.alert)
        
        let okAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil)
        
        myAlert.addAction(okAction)
        self.present(myAlert, animated: true, completion: nil)
    }
    
    func isValidEmail(_ testStr:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        if !emailTest.evaluate(with: testStr){
            displayMyAlertMessage(userMessage: "Invalid Email")
        }
        return emailTest.evaluate(with: testStr)
    }
    
    func isValidPhone(_ testStr:String) -> Bool {
        let PHONE_REGEX = "^[1-9]{1}[0-9]+$"
        let phoneTest = NSPredicate(format: "SELF MATCHES %@", PHONE_REGEX)
        let result =  phoneTest.evaluate(with: testStr)
        if !result{
            displayMyAlertMessage(userMessage: "Invalid Phone")
        }
        return  result
    }

    func checkEmptyValidation() -> Bool{
        var message:String = ""
        if searchbar!.text == "" || (searchbar!.text?.isEmpty)!{
            message = "Email or Mobile Number Required"
            displayMyAlertMessage(userMessage: message)
            return false
        }else{
            return true
        }
    }
    
    func fetchDevices() {
        Device.instance.getDevices(callback: { (devices) in
            DispatchQueue.main.async {
                self.deviceList = devices
            }
        }) { (error) in
            print("Failed to get devices")
            if let error =  error as? APIError{
                guard  let des = error.error_description else{return}
                print(des)
            }else if let error =  error as? Error{
                print(error.localizedDescription)
            }
        }
    }
    
    func isInviteExistInAdminList(invite:String)->Bool{
        for device in deviceList{
            if let admins = device.admins{
                for object in admins {
                    if object.email == invite{
                        return true
                    }
                    if object.phone == invite{
                        return true
                    }
                }
            }
        }
        return false
    }

    func isInviteExistInUserList(invite:String)->Bool{
        for device in deviceList{
            if let users = device.users{
                for object in users {
                    if object.email == invite{
                        return true
                    }
                    if object.phone == invite{
                        return true
                    }
                }
            }
        }
        return false
    }

    func isInviteExistInInviteList(invite:String)->Bool{
        for device in deviceList{
            if let invites = device.invites{
                for object in invites {
                    if object.contains(invite){
                        return true
                    }
                }
            }
        }
        return false
    }

    @objc func sendInvite(_ sender:UIButton){
        if  SharedData.AppSetting.IS_INTERNET_CONNECTED{
            if checkEmptyValidation(){
                if  searchbar.text!.isNumeric ? isValidPhone(searchbar.text!) :
                    isValidEmail(searchbar.text!.trimmingCharacters(in: CharacterSet.whitespaces)){
                    if searchbar.text!.isNumeric{
                        self.user = self.selectedCountryCode + searchbar.text!.trimmingCharacters(in: CharacterSet.whitespaces)
                    }else{
                        self.user = searchbar.text!.trimmingCharacters(in: CharacterSet.whitespaces)
                    }
                    if sender.titleLabel?.text == "Send"{
                        sendButton.setTitle("Send", for: .normal)
                        
                        if User.instance.email == self.user || User.instance.phone == self.user{
                            self.displayMyAlertMessage(userMessage: "\(self.user) is already having access for this device")
                        }else{
                            if isInviteExistInAdminList(invite: self.user){
                                self.displayMyAlertMessage(userMessage: "\(self.user) is already having admin access for this device")
                            }else{
                                if isInviteExistInUserList(invite: user){
                                    self.displayMyAlertMessage(userMessage: "\(self.user) is already having user access for this device")
                                }else{
                                    if isInviteExistInInviteList(invite: user){
                                        self.displayMyAlertMessage(userMessage: "\(self.user) is already invited for this device")
                                    }else{
                                        Device.instance.inviteUser(id: deviceID!, user: self.user, callback: { (device) in
                                            DispatchQueue.main.async {
                                                self.displayMyAlertMessage(userMessage: "\(self.user) is invited successfully")
                                                self.fetchInvites()
                                                self.tableView.reloadData()
                                            }
                                        }, failure: { (error) in
                                            if let error =  error as? APIError{
                                                guard  let des = error.error_description else{return}
                                                print(des)
                                            }else if let error =  error as? Error{
                                                print(error.localizedDescription)
                                            }
                                        })
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

}
