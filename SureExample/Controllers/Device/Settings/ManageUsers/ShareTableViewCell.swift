//
//  ShareTableViewCell.swift
//  SureExample
//
//  Created by piOctave on 6/12/18.
//  Copyright © 2018 piOctave. All rights reserved.
//

import UIKit

class ShareTableViewCell: UITableViewCell {
    
    
    @IBOutlet weak var person_name: UILabel!
    
    @IBOutlet weak var person_des: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}

