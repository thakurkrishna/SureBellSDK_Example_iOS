//
//  DeviceInviteTableViewCell.swift
//  SureExample
//
//  Created by piOctave on 6/13/18.
//  Copyright © 2018 piOctave. All rights reserved.
//

import UIKit

class DeviceInviteTableViewCell: UITableViewCell {
    
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var accept: UIButton!
    
    @IBOutlet weak var devicename: UILabel!
    @IBOutlet weak var reject: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
