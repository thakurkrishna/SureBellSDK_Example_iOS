//
//  SetTimeZoneViewController.swift
//  SureExample
//
//  Created by piOctave on 6/21/18.
//  Copyright © 2018 piOctave. All rights reserved.
//

import Foundation
import UIKit
import SureBell

class TimeZ {
    var timeZoneName: String
    var timeZoneAbbrivation: String
    var nsecondsFromGMT: String
    var isSelected: Bool
    init(timeZoneName: String, timeZoneAbbrivation: String, nsecondsFromGMT: String, isSelected: Bool) {
        self.timeZoneName = timeZoneName
        self.timeZoneAbbrivation = timeZoneAbbrivation
        self.nsecondsFromGMT = nsecondsFromGMT
        self.isSelected = isSelected
    }
}

class SetTimeZoneViewController: UIViewController {
    
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var timeZoneLbl: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    
    var filteredtimeZones = [TimeZ]()
    var timeZones = [TimeZ]()
    var deviceID:String?
    var oldtimeZone:String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let customBackButton = UIBarButtonItem(image: UIImage(named: "back") , style: .plain, target: self, action: #selector(backAction(sender:)))
        customBackButton.imageInsets = UIEdgeInsets(top: 2, left: -8, bottom: 0, right: 0)
        navigationItem.leftBarButtonItem = customBackButton

        if oldtimeZone != nil{
            timeZoneLbl.text = oldtimeZone
            timeZoneLbl.textColor = UIColor.red
        }else {
            timeZoneLbl.text = "Select TimeZone"
            timeZoneLbl.textColor = UIColor.brown
        }
        
        let textFieldInsideSearchBar = self.searchBar.value(forKey: "searchField") as! UITextField
        textFieldInsideSearchBar.leftViewMode = UITextFieldViewMode.always
        searchBar.showsScopeBar = true
        searchBar.delegate = self
        searchBar.returnKeyType = .search
        
        self.tableView.keyboardDismissMode = .onDrag
        self.tableView.delegate = self
        self.tableView.dataSource = self
        
        for rawTimeZone in TimeZone.knownTimeZoneIdentifiers{
            let timeZone = TimeZone(identifier: rawTimeZone)
            if let abbreviation = timeZone?.abbreviation(), let seconds = timeZone?.secondsFromGMT(){
                let time = TimeZ(timeZoneName: "\(rawTimeZone)", timeZoneAbbrivation: "\(abbreviation)", nsecondsFromGMT: "\(seconds)", isSelected: false)
                self.timeZones.append(time)
                self.filteredtimeZones.append(time)
                self.tableView.reloadData()
            }
        }
    }
    
    @objc func backAction(sender: UIBarButtonItem) {
        navigationController?.popViewController(animated: true)
    }

    func settingsAction(_ sender: UITapGestureRecognizer) {
        self.navigationController?.popViewController(animated: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        navigationController?.navigationBar.barStyle = UIBarStyle.black
        navigationController?.navigationBar.tintColor = UIColor.white
        self.navigationItem.title = "Timezone"
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}

extension SetTimeZoneViewController: UITableViewDelegate, UITableViewDataSource{
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.filteredtimeZones.count
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as UITableViewCell
        cell.textLabel?.font = UIFont(name: "ClanOT-News", size: 15)
        cell.textLabel?.sizeToFit()
        cell.textLabel?.numberOfLines = 0
        let rawTimeZone = self.filteredtimeZones[indexPath.row]
        cell.textLabel?.text = "\(rawTimeZone.timeZoneName) \n\(rawTimeZone.timeZoneAbbrivation)"
        if self.filteredtimeZones[indexPath.row].isSelected{
            cell.accessoryType = .checkmark
        }else{
            cell.accessoryType = .none
        }
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        for item in self.filteredtimeZones{
            item.isSelected = false
        }
        for item in self.timeZones{
            item.isSelected = false
        }
        
        UserDefaults.standard.set(self.filteredtimeZones[indexPath.row].timeZoneName, forKey: "SURE_SELECTED_TIME_ZONE_KEY")
        
        self.filteredtimeZones[indexPath.row].isSelected = true
        self.timeZones[indexPath.row].isSelected = true
        
        let df = DateFormatter()
        let timeZone = TimeZone(identifier: filteredtimeZones[indexPath.row].timeZoneName)
        df.timeZone = timeZone
        df.dateFormat = "Z"
        let localTimeZoneOffset: String = df.string(from: Date())
        print("\(localTimeZoneOffset)")
        self.changeTimeZone(timeZone: "\(localTimeZoneOffset)")
        self.tableView.reloadData()
    }
    
    
    func changeTimeZone(timeZone:String){
        if let deviceID = self.deviceID{
            Device.instance.changeTimeZone(id: deviceID, timeZone: timeZone, callback: { (device) in
                DispatchQueue.main.async {
                    self.timeZoneLbl.text = device.settings?.timeZone
                    self.timeZoneLbl.textColor = UIColor.red
                }
            }, failure: { (error) in
                if let error =  error as? APIError{
                    guard  let des = error.error_description else{return}
                    print(des)
                }else if let error =  error as? Error{
                    print(error.localizedDescription)
                }
            })
        }
    }
    
}

extension SetTimeZoneViewController: UISearchBarDelegate, UISearchDisplayDelegate{
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if let key = searchBar.text{
            self.filteredtimeZones = self.searchByAnyKey(searchKey: key)
            self.tableView.reloadData()
        }
    }
    
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        if let key = searchBar.text{
            self.filteredtimeZones = searchByAnyKey(searchKey: key)
            self.tableView.reloadData()
        }
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.text = ""
        searchBar.showsCancelButton = false
        searchBar.endEditing(true)
        self.view.endEditing(true)
        
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar){
        searchBar.showsCancelButton = true
    }
    
    func searchByAnyKey(searchKey:String) -> [TimeZ]{
        var filteredList = [TimeZ]()
        if searchKey.isEmpty {
            filteredList = self.timeZones
        } else {
            filteredList = self.timeZones.filter({(timeZones) in
                let str = timeZones.timeZoneName.lowercased() + timeZones.timeZoneAbbrivation.lowercased()
                let stringMatch = str.range(of: searchKey.lowercased())
                return stringMatch != nil ? true : false
            })
        }
        return filteredList
    }

}
