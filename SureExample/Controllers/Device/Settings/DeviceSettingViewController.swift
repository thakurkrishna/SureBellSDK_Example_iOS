//
//  DeviceSettingViewController.swift
//  SureExample
//
//  Created by piOctave on 6/7/18.
//  Copyright © 2018 piOctave. All rights reserved.
//

import UIKit
import SureBell

class DeviceSettingViewController:UIViewController, UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate {
    @IBOutlet weak var deviceNameView: UIView!
    @IBOutlet weak var devicenameedit: UITextField!
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var devicenamelbl: UILabel!
    @IBOutlet weak var headerView: UIView!
    
    typealias SettingInfo = (title: String, status:Bool, isSettingEnabled:Bool)
    fileprivate var items = [SettingInfo]()
    var deviceList = [Device]()

    var index:IndexPath?
    var deviceID:String?
    var timezone:String?
    let rightViewDevicename = UIView()
    var btnRightMenu: UIButton?
    var deviceeditclick = true

    var oldDeviceName = ""
    var isChimeEnabled = false
    var isCloudUploadEnabled = false
    var userid = ""
    var motionSensitivity = 2

    func displayMyAlertMessage(userMessage:String){
        let myAlert = UIAlertController(title: "Alert", message: userMessage, preferredStyle: UIAlertControllerStyle.alert)
        
        let okAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil)
        
        myAlert.addAction(okAction)
        self.present(myAlert, animated: true, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let customBackButton = UIBarButtonItem(image: UIImage(named: "back") , style: .plain, target: self, action: #selector(backAction(sender:)))
        customBackButton.imageInsets = UIEdgeInsets(top: 2, left: -8, bottom: 0, right: 0)
        navigationItem.leftBarButtonItem = customBackButton
        
        if SharedData.UserDetails.IS_USER_ADMIN{
            self.devicenameedit.isHidden = false
            deviceNameView.isUserInteractionEnabled = true
            
            btnRightMenu = UIButton()
            
            devicenameedit.rightViewMode = .always
            btnRightMenu?.tintColor = UIColor.darkGray
            btnRightMenu?.contentMode = .center
            btnRightMenu?.addTarget(self, action: #selector(self.changeDeviceName(_:)), for: UIControlEvents.touchUpInside)
            btnRightMenu?.frame = CGRect(x: 0, y: 0, width: 50, height: 30)
            btnRightMenu?.titleLabel?.font =  UIFont(name: "Clan-Bold", size: 13)
            btnRightMenu?.setTitle("Edit", for: .normal)
            btnRightMenu?.contentHorizontalAlignment = .right
            btnRightMenu?.setTitleColor(UIColor.black, for: .normal)
            devicenameedit.rightView = btnRightMenu
                        
            items.append(("Motion Settings",false,false))
            items.append(("Chime Config",false,self.isChimeEnabled))
            items.append(("Cloud Upload",false,self.isCloudUploadEnabled))
            items.append(("Change Device Wifi", false,false))
            items.append(("AV Settings",false,false))
            items.append(("Manage Users",false,false))
            items.append(("Remove Device",false,false))
            items.append(("Device Data Usage",false,false))
            items.append(("TimeZone",false,false))
        }else{
            self.devicenameedit.isHidden = false
            deviceNameView.isUserInteractionEnabled = false
         
            items.append(("Remove Device",false,false))
            items.append(("Device Data Usage",false,false))
            items.append(("TimeZone",false,false))
        }
        tableView.dataSource = self
        tableView.delegate = self
    }
    
    @objc func changeDeviceName(_ sender:UIButton){
        if sender.titleLabel?.text == "Edit"{
            self.btnRightMenu?.setTitle("Save", for: .normal)
            self.devicenameedit.text = self.oldDeviceName
            }else{
                self.btnRightMenu?.setTitle("Edit", for: .normal)
                if self.devicenameedit.text != self.oldDeviceName{
                    if !(self.devicenameedit.text?.trimmingCharacters(in: .whitespaces).isEmpty)!{                                Device.instance.changeDeviceName(id: deviceID!, name: (self.devicenameedit.text)!, callback: { (device) in                                    DispatchQueue.main.async {
                            self.oldDeviceName = device.name!
                            self.displayMyAlertMessage(userMessage: "Device Name updated")
                        }
                                    
                    }, failure: { (error) in
                        if let error =  error as? APIError{
                            guard  let des = error.error_description else{return}
                            print(des)
                        }else if let error =  error as? Error{
                            print(error.localizedDescription)
                        }
                    })
                    }else{
                        let message = "Device name can't be empty"
                        self.displayMyAlertMessage(userMessage: message)
                        self.devicenameedit.text = self.oldDeviceName
                    }
            }
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        navigationController?.navigationBar.barStyle = UIBarStyle.black
        navigationController?.navigationBar.tintColor = UIColor.white
        self.navigationItem.title = "Device Settings"
        
        Device.instance.getDevice(id: deviceID!, callback: { (device) in
            DispatchQueue.main.async {
                let device_name = (device.name)
                self.devicenameedit.text = device_name
                
                self.userid = User.instance._id!
                self.deviceID = (device._id)
                self.timezone = device.settings?.timeZone
                                        
                self.oldDeviceName =  device_name!
                self.isChimeEnabled = (device.settings?.chime)!
                    
                if let cloudupload = device.settings?.cloudUpload {
                    self.isCloudUploadEnabled = cloudupload
                }
                self.tableView.reloadData()

            }
        }, failure: { (error) in
            if let error =  error as? APIError{
                guard  let des = error.error_description else{return}
                print(des)
            }else if let error =  error as? Error{
                print(error.localizedDescription)
            }
        })
    }
    
    @objc func backAction(sender: UIBarButtonItem) {
        navigationController?.popViewController(animated: true)
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! SettingTableViewCell
        cell.title.text =  items[indexPath.row].title

        if items[indexPath.row].title == "Motion Settings"{
            cell.`switch`.isHidden = true
        }else if items[indexPath.row].title == "Chime Config"{
            cell.`switch`.isHidden = false
            if self.isChimeEnabled{
                cell.`switch`.isOn = true
            }else{
                cell.`switch`.isOn = false
            }
            cell.`switch`.addTarget(self, action: #selector(DeviceSettingViewController.switchIsChangedChime(_:)), for: UIControlEvents.valueChanged)
        }else if items[indexPath.row].title == "Cloud Upload"{
            cell.`switch`.isHidden = false
            if self.isCloudUploadEnabled{
                cell.`switch`.isOn = true
            }else{
                cell.`switch`.isOn = false
            }
            cell.`switch`.addTarget(self, action: #selector(DeviceSettingViewController.switchIsChangedCloud(_:)), for: UIControlEvents.valueChanged)
        }else if items[indexPath.row].title == "Change Device Wifi"{
            cell.`switch`.isHidden = true
        }else if items[indexPath.row].title == "AV Settings"{
            cell.`switch`.isHidden = true
        }else if items[indexPath.row].title == "Manage Users"{
            cell.`switch`.isHidden = true
        }else if items[indexPath.row].title == "Remove Device"{
            cell.`switch`.isHidden = true
        }else if items[indexPath.row].title == "Device Data Usage"{
            cell.`switch`.isHidden = true
        }else if items[indexPath.row].title == "TimeZone"{
            cell.`switch`.isHidden = true
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        _ = tableView.cellForRow(at: indexPath) as! SettingTableViewCell

        if items[indexPath.row].title == "Motion Settings"{
            Device.instance.getDevice(id: deviceID!, callback: { (device) in
                DispatchQueue.main.async {
                    let controller =  MotionSettingViewController(device)
                    self.present(controller, animated: false, completion: nil)
                }
            }, failure: { (error) in
                if let error =  error as? APIError{
                    guard  let des = error.error_description else{return}
                    print(des)
                }else if let error =  error as? Error{
                    print(error.localizedDescription)
                }
            })
        }else if items[indexPath.row].title == "AV Settings"{
            if let deviceID = self.deviceID{
                let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
                let controller: MoreDeviceSettingsViewController = storyboard.instantiateViewController(withIdentifier: "MoreDeviceSettingsViewController") as! MoreDeviceSettingsViewController
                controller.deviceID = deviceID
                self.navigationController?.pushViewController(controller, animated: true)
            }
        } else if items[indexPath.row].title == "Chime Config"{
            
        }else if items[indexPath.row].title == "Cloud Upload"{

        }else if items[indexPath.row].title == "Change Device Wifi"{
            Device.instance.getDevice(id: self.deviceID!, callback: { (device) in
                DispatchQueue.main.async {
                    let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
                    let appHome: SelectDeviceController = storyboard.instantiateViewController(withIdentifier: "SelectDeviceController") as! SelectDeviceController
                    appHome.location = device.location!
                    appHome.devicename = device.name
                    self.navigationController?.pushViewController(appHome, animated: true)
                }
            }, failure: { (error) in
                if let error =  error as? APIError{
                    guard  let des = error.error_description else{return}
                    print(des)
                }else if let error =  error as? Error{
                    print(error.localizedDescription)
                }
            })
        }else if items[indexPath.row].title == "Remove Device"{
            let message = "Are you sure. You Want to Remove Device"
            
            // Create the dialog
            let myAlert = UIAlertController(title: message, message: "", preferredStyle: UIAlertControllerStyle.alert)
            
            let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.default, handler: nil)
            myAlert.addAction(cancelAction)
            self.present(myAlert, animated: true, completion: nil)
            
            myAlert.addAction(UIAlertAction(title: "Remove", style: UIAlertActionStyle.destructive, handler: { action in
                DispatchQueue.main.async {
                    Device.instance.removeMe(id: self.deviceID!, user: self.userid, callback: { (device) in
                       self.displayMyAlertMessage(userMessage: "Successfully removed device")
                        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
                        let appHome: AppHomeViewController = storyboard.instantiateViewController(withIdentifier: "AppHomeViewController") as! AppHomeViewController
                        self.navigationController?.pushViewController(appHome, animated: false)

                    }, failure: { (error) in
                        if let error =  error as? APIError{
                            guard  let des = error.error_description else{return}
                            print(des)
                        }else if let error =  error as? Error{
                            print(error.localizedDescription)
                        }
                    })
                }
            }))
        }else if items[indexPath.row].title == "Device Data Usage"{
            let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
            let controller: DeviceDataUsageViewController = storyboard.instantiateViewController(withIdentifier: "DeviceDataUsageViewController") as! DeviceDataUsageViewController
            
            if let deviceID = self.deviceID{
                controller.deviceID = deviceID
                controller.dataUsage = true
                controller.titlelbl = true
            }
            self.navigationController?.pushViewController(controller, animated: true)

        }else if items[indexPath.row].title == "Manage Users"{
            let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
            let controller: ShareViewController = storyboard.instantiateViewController(withIdentifier: "ShareViewController") as! ShareViewController
            
            if let deviceID = self.deviceID{
                controller.deviceID = deviceID
            }
            self.navigationController?.pushViewController(controller, animated: true)

        }else if items[indexPath.row].title == "TimeZone"{
            let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
            let controller: SetTimeZoneViewController = storyboard.instantiateViewController(withIdentifier: "SetTimeZoneViewController") as! SetTimeZoneViewController
            
            if let deviceID = self.deviceID{
                controller.deviceID = deviceID
                controller.oldtimeZone = timezone
            }
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }

    @objc func switchIsChangedChime(_ mySwitch: UISwitch) {

        if mySwitch.isOn {
            Device.instance.changeChimeStatus(id: deviceID!, type: self.isChimeEnabled, callback: { (device) in
                DispatchQueue.main.async {
                        self.isChimeEnabled = (device.settings?.chime)!
                        self.displayMyAlertMessage(userMessage: "Chime Enabled")
                }
            }, failure: { (error) in
                if let error =  error as? APIError{
                    guard  let des = error.error_description else{return}
                    print(des)
                }else if let error =  error as? Error{
                    print(error.localizedDescription)
                }
            })
        } else {
            Device.instance.changeChimeStatus(id: deviceID!, type: self.isChimeEnabled, callback: { (device) in
                DispatchQueue.main.async {
                    self.isChimeEnabled = (device.settings?.chime)!
                    self.displayMyAlertMessage(userMessage: "Chime Disabled")
                }
            }, failure: { (error) in
                if let error =  error as? APIError{
                    guard  let des = error.error_description else{return}
                    print(des)
                }else if let error =  error as? Error{
                    print(error.localizedDescription)
                }
            })
        }
    }
    
    @objc func switchIsChangedCloud(_ mySwitch: UISwitch) {
        
        if mySwitch.isOn {
            Device.instance.changeCloudUploadStatus(id: deviceID!, type: self.isCloudUploadEnabled, callback: { (device) in
                DispatchQueue.main.async {
                    self.isCloudUploadEnabled = (device.settings?.cloudUpload)!
                    self.displayMyAlertMessage(userMessage: "Cloud Enabled Motion Upload")
                }
            }, failure: { (error) in
                if let error =  error as? APIError{
                    guard  let des = error.error_description else{return}
                    print(des)
                }else if let error =  error as? Error{
                    print(error.localizedDescription)
                }
            })
        } else {
            Device.instance.changeCloudUploadStatus(id: deviceID!, type: self.isCloudUploadEnabled, callback: { (device) in
                DispatchQueue.main.async {
                    self.isCloudUploadEnabled = (device.settings?.cloudUpload)!
                    self.displayMyAlertMessage(userMessage: "Cloud Disabled Motion Upload")
                }
            }, failure: { (error) in
                if let error =  error as? APIError{
                    guard  let des = error.error_description else{return}
                    print(des)
                }else if let error =  error as? Error{
                    print(error.localizedDescription)
                }
            })
        }
    }

}
