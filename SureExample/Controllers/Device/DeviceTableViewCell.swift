//
//  DeviceTableViewCell.swift
//  SureExample
//
//  Created by piOctave on 6/6/18.
//  Copyright © 2018 piOctave. All rights reserved.
//

import UIKit

class DeviceTableViewCell: UITableViewCell {
    
    @IBOutlet weak var settingcontainer: UIView!
    @IBOutlet weak var settingear: UIImageView!
    @IBOutlet weak var liveaction: UIButton!
    @IBOutlet weak var live: UIButton!
    @IBOutlet weak var liveView: UIView!
    
    @IBOutlet weak var devicestatus: UIButton!
    @IBOutlet weak var devicename: UILabel!
    @IBOutlet weak var events: UIButton!
    
    @IBOutlet weak var img: UIImageView!
    @IBAction func eventsaction(_ sender: Any) {
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    
    }

    
}
