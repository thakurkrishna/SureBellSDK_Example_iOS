//
//  AppHomeViewController.swift
//  SureBell_Example
//
//  Created by piOctave on 5/16/18.
//  Copyright © 2018 CocoaPods. All rights reserved.
//

import UIKit
import SureBell

class AppHomeViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    @IBOutlet weak var image: UIImage!
    @IBOutlet weak var headerView: UIView!
    
    @IBOutlet weak var profilepic: UIButton!
    @IBAction func profilepicAction(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        let controller: ProfileViewController = storyboard.instantiateViewController(withIdentifier: "ProfileViewController") as! ProfileViewController
        self.navigationController?.pushViewController(controller, animated: false)
    }
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var settingear: UIImageView!
    @IBOutlet weak var settingcontainer: UIView!
    
    var deviceList = [Device]()
    var deviceAvailable = [DeviceAvailable]()
    var deviceId:String?
    
    var lastImage = UIImage()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.settingsAction))
        settingcontainer.addGestureRecognizer(tapGestureRecognizer)
                
        self.tableView.dataSource = self
        self.tableView.delegate = self
    }
    
    @objc func settingsAction(_ sender: UITapGestureRecognizer) {
        UIView.animate(withDuration: 0.2, animations: {
            self.settingear.transform = CGAffineTransform(rotationAngle: CGFloat.pi / 1)
            }, completion: {
            (value: Bool) in
            let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
            let controller: DrawerViewController = storyboard.instantiateViewController(withIdentifier: "DrawerViewController") as! DrawerViewController
            self.navigationController?.pushViewController(controller, animated: false)
        })
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.userName.text = User.instance.firstname! + " " + User.instance.lastname!
        self.navigationController?.setNavigationBarHidden(true, animated: false)

        tableView.frame = CGRect(0, 70, self.view.frame.size.width, self.view.frame.size.height)

        if User.instance.profilePicture != nil {
            self.image = self.getImageFromUrl(sourceUrl: User.instance.profilePicture!)
        }else{
            self.image = UIImage(named: "user")
        }

        fetchDevices()
    }
    
    func fetchDevices() {
        Device.instance.getDevices(callback: { (devices) in
            DispatchQueue.main.async {
                self.deviceList = devices
                self.tableView.reloadData()
            }
        }) { (error) in
            print("Failed to get devices")
            if let error =  error as? APIError{
                guard  let des = error.error_description else{return}
                print(des)
            }else if let error =  error as? Error{
                print(error.localizedDescription)
            }
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if deviceList.count < 1{
            return 1
        }else{
            return deviceList.count
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if deviceList.count < 1{
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! DeviceTableViewCell
            cell.devicename.isHidden = true
            cell.events.isHidden = true
            cell.liveView.isHidden = true
            cell.live.isHidden = true
            cell.settingear.isHidden = true
            cell.settingcontainer.isHidden = true
            cell.img.isHidden = true
            cell.devicestatus.isHidden = true
            
            return cell
        }else{
            if self.isInvited(deviceId: deviceList[indexPath.row]._id!){
                let cell = tableView.dequeueReusableCell(withIdentifier: "invite") as! DeviceInviteTableViewCell
                let dInfo = deviceList[indexPath.row]
                let device_name = (dInfo.name!)

                self.deviceId = dInfo._id
                
                cell.reject.addTarget(self,action:#selector(self.rejectAction(sender:)), for: .touchUpInside)
                cell.accept.addTarget(self,action:#selector(acceptAction(sender:)), for: .touchUpInside)
                
                if !(dInfo.lastImage?.isEmpty)! {
                    self.lastImage = self.getImageFromUrl(sourceUrl: dInfo.lastImage!)!
                    cell.devicename.text = device_name
                    cell.img.image = self.lastImage
                }else{
                    cell.devicename.text = device_name
                }

                return cell
            }else{
                let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! DeviceTableViewCell
                let dInfo = deviceList[indexPath.row]
                let device_name = (dInfo.name!)
                let deviceID = (dInfo._id)
                
                Device.instance.deviceAvailable(callback: { (deviceAvble) in
                    DispatchQueue.main.async {
                        self.deviceAvailable = deviceAvble
                        let dAvailableInfo = self.deviceAvailable[indexPath.row]
                        let dAvailable = dAvailableInfo.isAvailable
                        
                        if !(dInfo.lastImage?.isEmpty)! {
                            self.lastImage = self.getImageFromUrl(sourceUrl: dInfo.lastImage!)!
                            cell.devicename.text = device_name
                            cell.img.image = self.lastImage
                        }else{
                            cell.devicename.text = device_name
                        }
                        
                        Device.instance.deviceAvailable(deviceID!, callback: { (deviceAvbleId) in
                            DispatchQueue.main.async {
                                if  dAvailable == true {
                                    cell.devicestatus.setTitle("Active", for: .normal)
                                    cell.devicestatus.titleLabel?.font =  UIFont(name: "Clanbook", size: 12)
                                    cell.devicestatus.setTitleColor(UIColor.green, for: .normal)
                                } else {
                                    cell.devicestatus.setTitle("InActive", for: .normal)
                                    cell.devicestatus.titleLabel?.font =  UIFont(name: "Clanbook", size: 12)
                                    cell.devicestatus.setTitleColor(UIColor.red, for: .normal)
                                }
                            }
                        }, failure: { (error) in
                            if let error =  error as? APIError{
                                guard  let des = error.error_description else{return}
                                print(des)
                            }else if let error =  error as? Error{
                                print(error.localizedDescription)
                            }
                        })
                    }
                }, failure: { (error) in
                    if let error =  error as? APIError{
                        guard  let des = error.error_description else{return}
                        print(des)
                    }else if let error =  error as? Error{
                        print(error.localizedDescription)
                    }
                    
                })
                
                cell.events.tag = indexPath.row
                cell.events.addTarget(self,action:#selector(eventsClicked(sender:)), for: .touchUpInside)
                
                cell.live.tag = indexPath.row
                cell.live.addTarget(self,action:#selector(getTurnAddress(sender:)), for: .touchUpInside)
                
                cell.settingear.transform = CGAffineTransform(rotationAngle: 0.0)
                cell.settingear.tag = indexPath.row
                cell.settingear.isUserInteractionEnabled = true
                let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.buttonSettingsClicked(recognizer:)))
                cell.settingear.addGestureRecognizer(tapGestureRecognizer)
                
                cell.devicename.isHidden = false
                cell.events.isHidden = false
                cell.liveView.isHidden = false
                cell.live.isHidden = false
                cell.settingear.isHidden = false
                cell.settingcontainer.isHidden = false
                cell.img.isHidden = false
                cell.devicestatus.isHidden = false
                
                return cell
            }
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

    }

    @objc func buttonSettingsClicked(recognizer : UITapGestureRecognizer){
        let data = deviceList[recognizer.view!.tag]

        if isAdmin(deviceId: data._id!) {
            SharedData.UserDetails.IS_USER_ADMIN = true
        }else{
            SharedData.UserDetails.IS_USER_ADMIN = false
        }

            let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
            let controller: DeviceSettingViewController = storyboard.instantiateViewController(withIdentifier: "DeviceSettingViewController") as! DeviceSettingViewController
       
        if let deviceID = data._id{
            controller.deviceID = deviceID
        }

        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    @objc func acceptAction(sender:UIButton){
        Device.instance.acceptInvite(id: deviceId!, callback: { (device) in
            DispatchQueue.main.async {
                self.fetchDevices()
            }
        }) { (error) in
            if let error =  error as? APIError{
                guard  let des = error.error_description else{return}
                print(des)
            }else if let error =  error as? Error{
                print(error.localizedDescription)
            }
        }
    }
    
    @objc func rejectAction(sender:UIButton){
        Device.instance.rejectInvite(id: deviceId!, callback: { (device) in
            DispatchQueue.main.async {
                self.fetchDevices()
            }
        }) { (error) in
            if let error =  error as? APIError{
                guard  let des = error.error_description else{return}
                print(des)
            }else if let error =  error as? Error{
                print(error.localizedDescription)
            }
        }
    }

    @objc func eventsClicked(sender:UIButton){
        let data = deviceList[sender.tag]
        
        if isAdmin(deviceId: data._id!) {
            SharedData.UserDetails.IS_USER_ADMIN = true
        }else{
            SharedData.UserDetails.IS_USER_ADMIN = false
        }
        
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        let controller: EventListViewController = storyboard.instantiateViewController(withIdentifier: "EventListViewController") as! EventListViewController
        
        if let deviceID = data._id{
            controller.deviceID = deviceID
        }
        
        self.navigationController?.pushViewController(controller, animated: true)
    }

    @objc func getTurnAddress(sender:UIButton){
        let data = deviceList[sender.tag]
        Device.instance.getTurnServerValues(callback: { (turnList) in
            let liveViewController = LiveViewController(turnList, device: data, sessionKey: Utils.randomStringWithLength(15), callFrom
                : "Live")
            self.present(liveViewController, animated: false, completion: nil)

        }) { (error) in
            if let error =  error as? APIError{
                guard  let des = error.error_description else{return}
                print(des)
            }else if let error =  error as? Error{
                print(error.localizedDescription)
            }
        }
    }

    func getImageFromUrl(sourceUrl: String) -> UIImage? {
        if let imageData = try? Data(contentsOf:URL(string: sourceUrl)!) {
            return UIImage(data: imageData)
        }
        return nil
    }
    
    func isAdmin(deviceId:String)-> Bool{
        for device in deviceList{
            if let id = device._id{
                if id == deviceId{
                    if let admins = device.admins{
                        for admin in admins{
                            if let email = admin.email{
                                if email == User.instance.email{
                                    return true
                                }
                            }
                            if let phone = admin.phone{
                                if phone == User.instance.phone{
                                    return true
                                }
                            }
                        }
                    }
                }
            }
        }
        return false
    }
    
    func isInvited(deviceId:String)-> Bool{
        for device in deviceList{
            if let id = device._id{
                if id == deviceId{
                    if let invites = device.invites{
                        for invite in invites{
                            if invite == User.instance.email {
                                return true
                            }
                        }
                    }
                }
            }
        }
        return false
    }

}

extension CGRect{
    init(_ x:CGFloat,_ y:CGFloat,_ width:CGFloat,_ height:CGFloat) {
        self.init(x:x,y:y,width:width,height:height)
    }
    
}
