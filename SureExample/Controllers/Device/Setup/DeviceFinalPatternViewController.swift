//
//  DeviceFinalPatternViewController.swift
//  SureExample
//
//  Created by piOctave on 6/21/18.
//  Copyright © 2018 piOctave. All rights reserved.
//

import UIKit
import SureBell

class DeviceFinalPatternViewController: UIViewController {
    
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var titleLbl: UILabel!
    
    @IBOutlet weak var lownetworkLbl: UILabel!
    @IBOutlet weak var wrongpwdLbl: UILabel!
    @IBOutlet weak var correctpwdLbl: UILabel!
    
    @IBOutlet weak var clickbtn: UIButton!
    
    @IBAction func clickAction(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        let appHome: AppHomeViewController = storyboard.instantiateViewController(withIdentifier: "AppHomeViewController") as! AppHomeViewController
        self.navigationController?.pushViewController(appHome, animated: false)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let customBackButton = UIBarButtonItem(image: UIImage(named: "back") , style: .plain, target: self, action: #selector(backAction(sender:)))
        customBackButton.imageInsets = UIEdgeInsets(top: 2, left: -8, bottom: 0, right: 0)
        navigationItem.leftBarButtonItem = customBackButton
    }
    
    @objc func backAction(sender: UIBarButtonItem) {
        navigationController?.popViewController(animated: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        navigationController?.navigationBar.barStyle = UIBarStyle.black
        navigationController?.navigationBar.tintColor = UIColor.white
        self.navigationItem.title = "Device Pairing Pattern"
        
        self.wrongpwdLbl.blink()
    }

}

extension UIView{
    func blink() {
        self.alpha = 0.2
        
        UIView.animate(withDuration: 1,
                       delay: 0.0,
                       options: [.curveLinear,
                                 .repeat,
                                 .autoreverse],
                       animations: { self.alpha = 1.0 },
                       completion: nil)
    }
}
