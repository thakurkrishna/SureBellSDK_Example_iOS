//
//  LocationViewController.swift
//  SureBell_Example
//
//  Created by piOctave on 6/5/18.
//  Copyright © 2018 CocoaPods. All rights reserved.
//

import UIKit
import SureBell
import MapKit
import CoreLocation

class LocationViewController: UIViewController, CLLocationManagerDelegate {
    
    @IBOutlet weak var map: MKMapView!
    var devicename:String?
    
    var latitudeCurrentLocation: CLLocationDegrees = 0.0
    var longitudeCurrentLocation: CLLocationDegrees = 0.0

    let manager = CLLocationManager()
    
    @IBAction func done(_ sender: Any) {
        if map.showsUserLocation {
            done.addTarget(self, action: #selector(doneAction), for: .touchUpInside)
        }else {
            self.displayMyAlertMessage(userMessage: "Location is empty")
        }
    }
    
    @IBOutlet weak var done: UIButton!
    
    func displayMyAlertMessage(userMessage:String){
        let myAlert = UIAlertController(title: "Alert", message: userMessage, preferredStyle: UIAlertControllerStyle.alert)
        
        let okAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil)
        
        myAlert.addAction(okAction)
        self.present(myAlert, animated: true, completion: nil)
    }
    
    @objc func doneAction() {
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        let controller: SelectDeviceController = storyboard.instantiateViewController(withIdentifier: "SelectDeviceController") as! SelectDeviceController
        controller.devicename = self.devicename
        controller.location = [latitudeCurrentLocation, longitudeCurrentLocation]
        self.navigationController?.pushViewController(controller, animated: false)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let customBackButton = UIBarButtonItem(image: UIImage(named: "back") , style: .plain, target: self, action: #selector(backAction(sender:)))
        customBackButton.imageInsets = UIEdgeInsets(top: 2, left: -8, bottom: 0, right: 0)
        navigationItem.leftBarButtonItem = customBackButton
        
        manager.delegate = self
        manager.desiredAccuracy = kCLLocationAccuracyBest
        manager.requestWhenInUseAuthorization()
        manager.startUpdatingLocation()
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let location = locations[0]
        
        let span:MKCoordinateSpan = MKCoordinateSpanMake(0.01, 0.01)
        let myLocation:CLLocationCoordinate2D = CLLocationCoordinate2DMake(location.coordinate.latitude, location.coordinate.longitude)
        let region:MKCoordinateRegion = MKCoordinateRegionMake(myLocation, span)
        map.setRegion(region, animated: true)
        
        self.latitudeCurrentLocation = location.coordinate.latitude
        self.longitudeCurrentLocation = location.coordinate.longitude
        
        self.map.showsUserLocation = true
    }
    
    @objc func backAction(sender: UIBarButtonItem) {
        navigationController?.popViewController(animated: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        navigationController?.navigationBar.barStyle = UIBarStyle.black
        navigationController?.navigationBar.tintColor = UIColor.white
        self.navigationItem.title = "Location"
    }


}
