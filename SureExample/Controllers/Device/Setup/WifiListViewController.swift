//
//  WifiListViewController.swift
//  SureBell_Example
//
//  Created by piOctave on 6/5/18.
//  Copyright © 2018 CocoaPods. All rights reserved.
//

import UIKit
import SureBell

class WifiListViewController:UIViewController,UITableViewDelegate,UITableViewDataSource {
    @IBOutlet weak var tableView: UITableView!
    
    var cell:WifiListTableViewCell!
    var ssids = [SSID]()
    var refreshControl:UIRefreshControl?
    var selected_channel = ""
    var selected_ssid = ""
    var selected_security = ""
    var selected_encryption = ""
    var localAddress = "192.168.101.1"
    var devicename:String?
    var location = [0.01, 0.01]

    @IBAction func backAction(_ sender: AnyObject) {
        navigationController?.popToRootViewController(animated: false)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let customBackButton = UIBarButtonItem(image: UIImage(named: "back") , style: .plain, target: self, action: #selector(backAction(sender:)))
        customBackButton.imageInsets = UIEdgeInsets(top: 2, left: -8, bottom: 0, right: 0)
        navigationItem.leftBarButtonItem = customBackButton
    }
    
    @objc func backAction(sender: UIBarButtonItem) {
        navigationController?.popViewController(animated: true)
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(false)
    }

    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        navigationController?.navigationBar.barStyle = UIBarStyle.black
        navigationController?.navigationBar.tintColor = UIColor.white
        self.navigationItem.title = "WifiList"
        fetchList()
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return ssids.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! WifiListTableViewCell
        if ssids.count > 0{
            print(ssids[indexPath.row].ssid!)
            cell.wifiName.text = ssids[indexPath.row].ssid!
        } else {
            cell.wifiName.text = "No Wifi Found"
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if ssids[indexPath.row].security != "OPEN"{
            Device.instance.pairNewDevice(accessToken: Token.instance.access_token!, ipAddress: localAddress, channel: ssids[indexPath.row].channel!, ssid: ssids[indexPath.row].ssid!, encryption: self.ssids[indexPath.row].encryption!, password: "", security: self.ssids[indexPath.row].security!, location: self.location, chime: true, name: devicename!, status: { (Int) in
                print("Naviagting to wifi password view controller")
                
                DispatchQueue.main.async {
                    let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
                    let appHome: WifiPasswordViewController = storyboard.instantiateViewController(withIdentifier: "WifiPasswordViewController") as! WifiPasswordViewController
                    self.navigationController?.pushViewController(appHome, animated: true)                }
            })
        }else{
            print(self.ssids)
            self.selected_ssid = self.ssids[indexPath.row].ssid!
            self.selected_channel = self.ssids[indexPath.row].channel!
            self.selected_security = self.ssids[indexPath.row].security!
            self.selected_encryption = self.ssids[indexPath.row].encryption!
        }
        
    }
    
    func fetchList(){
        Device.instance.getSSIDList(ipAddress: localAddress, callback: { (ssids) in
            print(ssids)
            self.ssids.removeAll()
            self.refreshControl?.endRefreshing()
            self.ssids = ssids
            DispatchQueue.main.async {
                self.tableView.dataSource = self
                self.tableView.delegate = self
                self.tableView.reloadData()
            }
        }, { (error) in
            print("Failed to get SSIDS")
            if let error =  error as? APIError{
                guard  let des = error.error_description else{return}
                print(des)
            }else if let error =  error as? Error{
                print(error.localizedDescription)
            }
        })
    }
    
}
