//
//  SelectDeviceController.swift
//  SureBell_Example
//
//  Created by piOctave on 6/5/18.
//  Copyright © 2018 CocoaPods. All rights reserved.
//

import UIKit
import SureBell


class SelectDeviceController: UIViewController {
    
    @IBOutlet weak var selectdevicelbl: UILabel!

    var timer:Timer?
    var devicename:String?
    var isSecond = false
    var location = [0.01, 0.01]
    
    @IBAction func next(_ sender: Any) {
        pingToAp()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let customBackButton = UIBarButtonItem(image: UIImage(named: "back") , style: .plain, target: self, action: #selector(backAction(sender:)))
        customBackButton.imageInsets = UIEdgeInsets(top: 2, left: -8, bottom: 0, right: 0)
        navigationItem.leftBarButtonItem = customBackButton
    }
    
    @objc func backAction(sender: UIBarButtonItem) {
        navigationController?.popViewController(animated: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        navigationController?.navigationBar.barStyle = UIBarStyle.black
        navigationController?.navigationBar.tintColor = UIColor.white
        self.navigationItem.title = "Select Device AP"
    }

    
    func pingToAp(){
        let wifiInfo = GenralFunctions.fetchSSIDInfo()
        if wifiInfo.count > 0{
            if wifiInfo[0].contains("SureBell"){
                self.timer?.invalidate()
                Device.instance.getPingResponse(ipAddress: "192.168.101.1", callback: { (ping) in
                    print(ping)
                    DispatchQueue.main.async{
                        self.timer?.invalidate()
                        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
                        let appHome: WifiListViewController = storyboard.instantiateViewController(withIdentifier: "WifiListViewController") as! WifiListViewController
                        appHome.devicename = self.devicename
                        appHome.location = self.location
                        self.navigationController?.pushViewController(appHome, animated: true)
                    }
                }, { (error) in
                    print("Failed to ping")
                    if let error =  error as? APIError{
                        guard  let des = error.error_description else{return}
                        print(des)
                    }else if let error =  error as? Error{
                        print(error.localizedDescription)
                    }
                })
            }
        }
    }
}
