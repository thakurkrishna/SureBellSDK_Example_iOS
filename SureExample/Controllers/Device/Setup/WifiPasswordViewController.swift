//
//  WifiPasswordViewController.swift
//  SureBell_Example
//
//  Created by piOctave on 6/5/18.
//  Copyright © 2018 CocoaPods. All rights reserved.
//

import UIKit

@objc protocol ViewControllerDelegate{
    func keyboardWillShowWithSize(_ size:CGSize, andDuration duration:TimeInterval)
    func keyboardWillHideWithSize(_ size:CGSize,andDuration duration:TimeInterval)
}

class WifiPasswordViewController: UIViewController,UITextFieldDelegate {
    
    @IBOutlet weak var headingMesage: UILabel!
    
    @IBOutlet weak var done_btn: UIButton!
    @IBOutlet weak var password_input: UITextField!
    
    var keyboardDelegate:ViewControllerDelegate?
    var selected_channel = ""
    var selected_ssid = ""
    var selected_security = ""
    var selected_encryption = ""
    var btnLeftMenu: UIButton?
    var passwordiconClick = true
    var deviceAddress = "http://192.168.101.1"
    
    @IBAction func doneAction(_ sender: Any) {
        if password_input.text != "" {
            let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
            let appHome: DeviceFinalPatternViewController = storyboard.instantiateViewController(withIdentifier: "DeviceFinalPatternViewController") as! DeviceFinalPatternViewController
            self.navigationController?.pushViewController(appHome, animated: false)
        }else{
            displayMyAlertMessage(userMessage: "Enter Wifi Password")
        }
    }
    
    func displayMyAlertMessage(userMessage:String){
        let myAlert = UIAlertController(title: "Alert", message: userMessage, preferredStyle: UIAlertControllerStyle.alert)
        
        let okAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil)
        
        myAlert.addAction(okAction)
        self.present(myAlert, animated: true, completion: nil)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let customBackButton = UIBarButtonItem(image: UIImage(named: "back") , style: .plain, target: self, action: #selector(backAction(sender:)))
        customBackButton.imageInsets = UIEdgeInsets(top: 2, left: -8, bottom: 0, right: 0)
        navigationItem.leftBarButtonItem = customBackButton

        btnLeftMenu = UIButton()
        let origImage = UIImage(named: "ic_eye_closed");
        let tintedImage = origImage?.withRenderingMode(UIImageRenderingMode.alwaysTemplate)
        btnLeftMenu?.setImage(tintedImage, for: UIControlState())
        btnLeftMenu?.tintColor = UIColor.darkGray
        btnLeftMenu?.contentMode = .center
        btnLeftMenu!.addTarget(self, action: #selector(self.togglePasswordVisible(_:)), for: UIControlEvents.touchUpInside)
        btnLeftMenu!.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        password_input.rightView = btnLeftMenu
        password_input.rightViewMode = .always
        
        self.headingMesage.text = "WIFI PASSWORD FOR \(self.selected_ssid)"
        self.password_input.placeholder = "Enter WiFi Password for \(self.selected_ssid)"
        done_btn.backgroundColor = UIColor.darkGray
        self.password_input!.delegate = self
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool{
        if textField == self.password_input {
        }
        return true
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    
    @objc func backAction(sender: UIBarButtonItem) {
        navigationController?.popViewController(animated: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        navigationController?.navigationBar.barStyle = UIBarStyle.black
        navigationController?.navigationBar.tintColor = UIColor.white
        self.navigationItem.title = "Wifi Password"
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(false)
    }
    
    @IBAction func backAction(_ sender: AnyObject) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let maxLength = 35
        let currentString: NSString =  NSString(string: textField.text!)
        let newString: NSString =
            currentString.replacingCharacters(in: range, with: string) as NSString
        if (string == "\n") {}
        return newString.length <= maxLength
    }
    
    @objc func togglePasswordVisible(_ sender:AnyObject){
        if(passwordiconClick == true) {
            password_input!.isSecureTextEntry = false
            let origImage = UIImage(named: "ic_eye_open");
            let tintedImage = origImage?.withRenderingMode(UIImageRenderingMode.alwaysTemplate)
            btnLeftMenu!.setImage(tintedImage, for: UIControlState())
            btnLeftMenu!.tintColor = UIColor.darkGray
            passwordiconClick = false
        } else {
            password_input!.isSecureTextEntry = true
            let origImage = UIImage(named: "ic_eye_closed");
            let tintedImage = origImage?.withRenderingMode(UIImageRenderingMode.alwaysTemplate)
            btnLeftMenu!.setImage(tintedImage, for: UIControlState())
            btnLeftMenu!.tintColor = UIColor.darkGray
            passwordiconClick = true
        }
    }
    
    func doYourStuff(){
        NotificationCenter.default.removeObserver(self)
    }
    
}

