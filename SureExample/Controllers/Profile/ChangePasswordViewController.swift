//
//  ChangePasswordViewController.swift
//  SureBell_Example
//
//  Created by piOctave on 5/31/18.
//  Copyright © 2018 CocoaPods. All rights reserved.
//

import Foundation
import UIKit
import SureBell

class ChangePasswordViewController:UIViewController, UITextFieldDelegate {
    
    var currentpasswordTextField = UITextField()
    var passwordTextField = UITextField()
    var cfnpasswordTextField = UITextField()
    
    var btnLeftMenu: UIButton?
    var btnLeftMenu1: UIButton?
    var btnLeftMenu2: UIButton?
    var currentpasswordiconClick = true
    var newpasswordiconClick = true
    var confirmpasswordiconClick = true


    override func awakeFromNib() {
        self.changepwdViewAndConstraints()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let customBackButton = UIBarButtonItem(image: UIImage(named: "back") , style: .plain, target: self, action: #selector(backAction(sender:)))
        customBackButton.imageInsets = UIEdgeInsets(top: 2, left: -8, bottom: 0, right: 0)
        navigationItem.leftBarButtonItem = customBackButton
    }

    @objc func backAction(sender: UIBarButtonItem) {
        navigationController?.popViewController(animated: true)
    }

    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        navigationController?.navigationBar.barStyle = UIBarStyle.black
        navigationController?.navigationBar.tintColor = UIColor.white
        self.navigationItem.title = "Change Password"
    }
    
    func changepwdViewAndConstraints() {
        currentpasswordTextField.placeholder = "Current Password"
        currentpasswordTextField.textColor = UIColor.darkGray
        currentpasswordTextField.font = UIFont(name: "Clanbook", size: 20)
        currentpasswordTextField.tintColor = UIColor.darkGray
        
        self.view.addSubview(currentpasswordTextField)
        
        currentpasswordTextField.translatesAutoresizingMaskIntoConstraints = false
        let leftConstraint = NSLayoutConstraint(item: currentpasswordTextField, attribute: .leading, relatedBy: .equal, toItem: view, attribute: .leading, multiplier: 1.0, constant: 40)
        let rightConstraint = NSLayoutConstraint(item: currentpasswordTextField, attribute: .trailing, relatedBy: .equal, toItem: view, attribute: .trailing, multiplier: 1.0, constant: -40)
        let heightConstraint = NSLayoutConstraint(item: currentpasswordTextField, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 40)
        let widthConstraint = NSLayoutConstraint(item: currentpasswordTextField, attribute: .width, relatedBy: .greaterThanOrEqual, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 200)
        let topConstraint = NSLayoutConstraint(item: currentpasswordTextField, attribute: .top, relatedBy: .equal, toItem: view, attribute: .top, multiplier: 1.0, constant: 130)
        view.addConstraints([leftConstraint, rightConstraint, heightConstraint, widthConstraint, topConstraint])
        
        btnLeftMenu = UIButton()
        let origImage = UIImage(named: "ic_eye_closed");
        let tintedImage = origImage?.withRenderingMode(UIImageRenderingMode.alwaysTemplate)
        btnLeftMenu?.setImage(tintedImage, for: UIControlState())
        btnLeftMenu?.tintColor = UIColor.darkGray
        btnLeftMenu?.contentMode = .center
        btnLeftMenu?.isAccessibilityElement = true
        btnLeftMenu!.addTarget(self, action: #selector(self.togglecpPasswordVisible(_:)), for: UIControlEvents.touchUpInside)
        btnLeftMenu!.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        currentpasswordTextField.rightView = btnLeftMenu
        currentpasswordTextField.rightViewMode = .always
        
        passwordTextField.placeholder = "New Password"
        passwordTextField.textColor = UIColor.darkGray
        passwordTextField.font = UIFont(name: "Clanbook", size: 20)
        passwordTextField.tintColor = UIColor.darkGray

        self.view.addSubview(passwordTextField)
        
        passwordTextField.translatesAutoresizingMaskIntoConstraints = false
        let leftConstraint1 = NSLayoutConstraint(item: passwordTextField, attribute: .leading, relatedBy: .equal, toItem: view, attribute: .leading, multiplier: 1.0, constant: 40)
        let rightConstraint1 = NSLayoutConstraint(item: passwordTextField, attribute: .trailing, relatedBy: .equal, toItem: view, attribute: .trailing, multiplier: 1.0, constant: -40)
        let heightConstraint1 = NSLayoutConstraint(item: passwordTextField, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 40)
        let widthConstraint1 = NSLayoutConstraint(item: passwordTextField, attribute: .width, relatedBy: .greaterThanOrEqual, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 200)
        let topConstraint1 = NSLayoutConstraint(item: passwordTextField, attribute: .top, relatedBy: .equal, toItem: view, attribute: .top, multiplier: 1.0, constant: 180)
        view.addConstraints([leftConstraint1, rightConstraint1, heightConstraint1, widthConstraint1, topConstraint1])
        
        btnLeftMenu1 = UIButton()
        let origImage1 = UIImage(named: "ic_eye_closed");
        let tintedImage1 = origImage1?.withRenderingMode(UIImageRenderingMode.alwaysTemplate)
        btnLeftMenu1?.setImage(tintedImage1, for: UIControlState())
        btnLeftMenu1?.tintColor = UIColor.darkGray
        btnLeftMenu1?.contentMode = .center
        btnLeftMenu1?.isAccessibilityElement = true
        btnLeftMenu1!.addTarget(self, action: #selector(self.togglePasswordVisible(_:)), for: UIControlEvents.touchUpInside)
        btnLeftMenu1!.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        passwordTextField.rightView = btnLeftMenu1
        passwordTextField.rightViewMode = .always
        
        cfnpasswordTextField.placeholder = "Confirm Password"
        cfnpasswordTextField.textColor = UIColor.darkGray
        cfnpasswordTextField.font = UIFont(name: "Clanbook", size: 20)
        cfnpasswordTextField.tintColor = UIColor.darkGray
        self.view.addSubview(cfnpasswordTextField)
        
        cfnpasswordTextField.translatesAutoresizingMaskIntoConstraints = false
        let leftConstraint2 = NSLayoutConstraint(item: cfnpasswordTextField, attribute: .leading, relatedBy: .equal, toItem: view, attribute: .leading, multiplier: 1.0, constant: 40)
        let rightConstraint2 = NSLayoutConstraint(item: cfnpasswordTextField, attribute: .trailing, relatedBy: .equal, toItem: view, attribute: .trailing, multiplier: 1.0, constant: -40)
        let heightConstraint2 = NSLayoutConstraint(item: cfnpasswordTextField, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 40)
        let widthConstraint2 = NSLayoutConstraint(item: cfnpasswordTextField, attribute: .width, relatedBy: .greaterThanOrEqual, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 200)
        let topConstraint2 = NSLayoutConstraint(item: cfnpasswordTextField, attribute: .top, relatedBy: .equal, toItem: view, attribute: .top, multiplier: 1.0, constant: 230)
        view.addConstraints([leftConstraint2, rightConstraint2, heightConstraint2, widthConstraint2, topConstraint2])
        
        btnLeftMenu2 = UIButton()
        let origImage2 = UIImage(named: "ic_eye_closed");
        let tintedImage2 = origImage2?.withRenderingMode(UIImageRenderingMode.alwaysTemplate)
        btnLeftMenu2?.setImage(tintedImage2, for: UIControlState())
        btnLeftMenu2?.tintColor = UIColor.darkGray
        btnLeftMenu2?.contentMode = .center
        btnLeftMenu2?.isAccessibilityElement = true
        btnLeftMenu2!.addTarget(self, action: #selector(self.togglecfnPasswordVisible(_:)), for: UIControlEvents.touchUpInside)
        btnLeftMenu2!.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        cfnpasswordTextField.rightView = btnLeftMenu2
        cfnpasswordTextField.rightViewMode = .always
        
        let changePwdBtn = UIButton()
        changePwdBtn.backgroundColor = UIColor.black
        changePwdBtn.setTitle("Change Password", for: .normal)
        changePwdBtn.titleLabel?.font =  UIFont(name: "Clanbook", size: 20)
        changePwdBtn.setTitleColor(UIColor.white, for: .normal)
        changePwdBtn.addTarget(self, action: #selector(changepwdAction), for: .touchUpInside)
        self.view.addSubview(changePwdBtn)
        
        changePwdBtn.translatesAutoresizingMaskIntoConstraints = false
        let leftConstraint3 = NSLayoutConstraint(item: changePwdBtn, attribute: .leading, relatedBy: .equal, toItem: view, attribute: .leading, multiplier: 1.0, constant: 40)
        let rightConstraint3 = NSLayoutConstraint(item: changePwdBtn, attribute: .trailing, relatedBy: .equal, toItem: view, attribute: .trailing, multiplier: 1.0, constant: -40)
        let heightConstraint3 = NSLayoutConstraint(item: changePwdBtn, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 40)
        let widthConstraint3 = NSLayoutConstraint(item: changePwdBtn, attribute: .width, relatedBy: .greaterThanOrEqual, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 200)
        let topConstraint3 = NSLayoutConstraint(item: changePwdBtn, attribute: .top, relatedBy: .equal, toItem: view, attribute: .top, multiplier: 1.0, constant: 330)
        view.addConstraints([leftConstraint3, rightConstraint3, heightConstraint3, widthConstraint3, topConstraint3])

        self.currentpasswordTextField.delegate = self
        self.passwordTextField.delegate = self
        self.cfnpasswordTextField.delegate = self
    }
    
    @objc func togglecpPasswordVisible(_ sender:AnyObject){
        if(currentpasswordiconClick == true) {
            currentpasswordTextField.isSecureTextEntry = false
            let origImage = UIImage(named: "ic_eye_open");
            let tintedImage = origImage?.withRenderingMode(UIImageRenderingMode.alwaysTemplate)
            btnLeftMenu!.setImage(tintedImage, for: UIControlState())
            btnLeftMenu!.tintColor = UIColor.darkGray
            currentpasswordiconClick = false
        } else {
            currentpasswordTextField.isSecureTextEntry = true
            let origImage = UIImage(named: "ic_eye_closed");
            let tintedImage = origImage?.withRenderingMode(UIImageRenderingMode.alwaysTemplate)
            btnLeftMenu!.setImage(tintedImage, for: UIControlState())
            btnLeftMenu!.tintColor = UIColor.darkGray
            
            currentpasswordiconClick = true
        }
    }

    @objc func togglePasswordVisible(_ sender:AnyObject){
        if(newpasswordiconClick == true) {
            passwordTextField.isSecureTextEntry = false
            let origImage = UIImage(named: "ic_eye_open");
            let tintedImage = origImage?.withRenderingMode(UIImageRenderingMode.alwaysTemplate)
            btnLeftMenu1!.setImage(tintedImage, for: UIControlState())
            btnLeftMenu1!.tintColor = UIColor.darkGray
            newpasswordiconClick = false
        } else {
            passwordTextField.isSecureTextEntry = true
            let origImage = UIImage(named: "ic_eye_closed");
            let tintedImage = origImage?.withRenderingMode(UIImageRenderingMode.alwaysTemplate)
            btnLeftMenu1!.setImage(tintedImage, for: UIControlState())
            btnLeftMenu1!.tintColor = UIColor.darkGray
            
            newpasswordiconClick = true
        }
    }

    @objc func togglecfnPasswordVisible(_ sender:AnyObject){
        if(confirmpasswordiconClick == true) {
            cfnpasswordTextField.isSecureTextEntry = false
            let origImage = UIImage(named: "ic_eye_open");
            let tintedImage = origImage?.withRenderingMode(UIImageRenderingMode.alwaysTemplate)
            btnLeftMenu2!.setImage(tintedImage, for: UIControlState())
            btnLeftMenu2!.tintColor = UIColor.darkGray
            confirmpasswordiconClick = false
        } else {
            cfnpasswordTextField.isSecureTextEntry = true
            let origImage = UIImage(named: "ic_eye_closed");
            let tintedImage = origImage?.withRenderingMode(UIImageRenderingMode.alwaysTemplate)
            btnLeftMenu2!.setImage(tintedImage, for: UIControlState())
            btnLeftMenu2!.tintColor = UIColor.darkGray
            
            confirmpasswordiconClick = true
        }
    }

    
    func displayMyAlertMessage(userMessage:String){
        let myAlert = UIAlertController(title: "Alert", message: userMessage, preferredStyle: UIAlertControllerStyle.alert)
        
        let okAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil)
        
        myAlert.addAction(okAction)
        self.present(myAlert, animated: true, completion: nil)
    }
    
    func checkEmptyValidation() -> Bool{
        var message:String = ""
        if currentpasswordTextField.text == "" || (currentpasswordTextField.text?.isEmpty)!{
            message = "Current password required"
            displayMyAlertMessage(userMessage: message)
            return false
        }else if passwordTextField.text == "" || (passwordTextField.text?.isEmpty)!{
            message = "Old password required"
            displayMyAlertMessage(userMessage: message)
            return false
        }else if cfnpasswordTextField.text == "" || (cfnpasswordTextField.text?.isEmpty)!{
            message = "Confirm password required"
            displayMyAlertMessage(userMessage: message)
            return false
        }else{
            return true
        }
    }
    
    func checkPasswordCharLimit()-> Bool{
        var message:String = ""
        if let password = passwordTextField.text{
            if password.count >= 6{
                return true
            }else{
                message = "Password Should be Greater than 5 Characters"
                displayMyAlertMessage(userMessage: message)
                return false
            }
        }else{
            return false
        }
    }

    func checkPasswordMatch() -> Bool{
        if passwordTextField.text != cfnpasswordTextField.text{
            let message = "Password and Confirm Password Should be Same"
            displayMyAlertMessage(userMessage: message)
            return  false
        }else{
            return  true
        }
    }

    @objc func changepwdAction() {
        if checkEmptyValidation(){
            if checkPasswordMatch(){
                if checkPasswordCharLimit(){
                    if currentpasswordTextField.text != passwordTextField.text{
                        let param = ["currentPassword":"\(String(describing: currentpasswordTextField.text!))","newPassword":"\(String(describing: passwordTextField.text!))"]

                        User.instance.updatePassword(parameters: param, callback: { (user) in
                            self.displayMyAlertMessage(userMessage: "Password reset done successfully")
                            self.navigationController?.popViewController(animated: true)
                        }, failure: { (error) in
                            if let error =  error as? APIError{
                                guard  let des = error.error_description else{return}
                                print(des)
                            }else if let error =  error as? Error{
                                print(error.localizedDescription)
                            }
                        })
                    }else {
                        self.displayMyAlertMessage(userMessage: "Current password and new password are same")
                    }
                }
            }
        }
        
    }

}
