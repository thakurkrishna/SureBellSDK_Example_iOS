//
//  ProfileViewController.swift
//  SureBell_Example
//
//  Created by piOctave on 5/29/18.
//  Copyright © 2018 CocoaPods. All rights reserved.
//

import UIKit
import SureBell

class ProfileViewController: UIViewController, UITextFieldDelegate, UIPopoverPresentationControllerDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, CountrySelected {
    
    var firstnameTextField = UITextField()
    var lastnameTextField = UITextField()
    var emailTextField = UITextField()
    var phnumTextField = UITextField()
    var changepwdButton = UIButton()
    var securityButton = UIButton()
    
    var selectedCountryCode = "+91"
    let phnumViewTextField = UIView()
    let leftphnum = UILabel()
    
    var firstnameEditButton = UIButton()
    var lastnameEditButton = UIButton()
    var phnumEditButton = UIButton()
    var emailEditButton = UIButton()

    var imageView = UIImageView()

    var oldFirstName = ""
    var oldEmail = ""
    var oldLastName = ""
    var oldPhone = ""
    let imagePickerController = UIImagePickerController()

    override func viewDidLoad() {
        super.viewDidLoad()
        let customBackButton = UIBarButtonItem(image: UIImage(named: "back") , style: .plain, target: self, action: #selector(backAction(sender:)))
        customBackButton.imageInsets = UIEdgeInsets(top: 2, left: -8, bottom: 0, right: 0)
        navigationItem.leftBarButtonItem = customBackButton
        imagePickerController.delegate = self

        self.profileViewAndConstraints()
        self.rightAndleftView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        navigationController?.navigationBar.barStyle = UIBarStyle.black
        navigationController?.navigationBar.tintColor = UIColor.white
        self.navigationItem.title = "Profile"
        
        self.firstnameTextField.text = User.instance.firstname!
        self.lastnameTextField.text = User.instance.lastname!
        self.emailTextField.text = User.instance.email!
        self.phnumTextField.text = User.instance.phone!
        self.selectedCountryCode = User.instance.countryDialCode!
        
        self.oldEmail = User.instance.email!
        self.oldFirstName =  User.instance.firstname!
        self.oldLastName =  User.instance.lastname!
        self.oldPhone =  User.instance.phone!
        
        self.firstnameEditButton.setTitle("Edit", for: .normal)
        self.lastnameEditButton.setTitle("Edit", for: .normal)
        self.phnumEditButton.setTitle("Edit", for: .normal)
        self.emailEditButton.setTitle("Edit", for: .normal)
        
    }
    
    @objc func backAction(sender: UIBarButtonItem) {
        navigationController?.popViewController(animated: true)
    }
    
    func profileViewAndConstraints() {
        let image: UIImage = UIImage(named: "user")!
        imageView = UIImageView(image: image)
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.isUserInteractionEnabled = true
       
        self.view.addSubview(imageView)
        
        let leftConstraint = NSLayoutConstraint(item: imageView, attribute: .leading, relatedBy: .equal, toItem: view, attribute: .leading, multiplier: 1.0, constant: 50)
        let rightConstraint = NSLayoutConstraint(item: imageView, attribute: .trailing, relatedBy: .equal, toItem: view, attribute: .trailing, multiplier: 1.0, constant: -50)
        let heightConstraint = NSLayoutConstraint(item: imageView, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 100)
        let widthConstraint = NSLayoutConstraint(item: imageView, attribute: .width, relatedBy: .greaterThanOrEqual, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 50)
        let topConstraint = NSLayoutConstraint(item: imageView, attribute: .top, relatedBy: .equal, toItem: view, attribute: .top, multiplier: 1.0, constant: 100)
        view.addConstraints([leftConstraint, rightConstraint, heightConstraint, widthConstraint, topConstraint])
        
        firstnameTextField.tintColor = UIColor.darkGray
        firstnameTextField.textColor = UIColor.black
        firstnameTextField.placeholder = "First Name"
        self.view.addSubview(firstnameTextField)

        firstnameTextField.translatesAutoresizingMaskIntoConstraints = false
        let leftConstraint2 = NSLayoutConstraint(item: firstnameTextField, attribute: .leading, relatedBy: .equal, toItem: view, attribute: .leading, multiplier: 1.0, constant: 40)
        let rightConstraint2 = NSLayoutConstraint(item: firstnameTextField, attribute: .trailing, relatedBy: .equal, toItem: view, attribute: .trailing, multiplier: 1.0, constant: -40)
        let heightConstraint2 = NSLayoutConstraint(item: firstnameTextField, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 40)
        let widthConstraint2 = NSLayoutConstraint(item: firstnameTextField, attribute: .width, relatedBy: .greaterThanOrEqual, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 200)
        let topConstraint2 = NSLayoutConstraint(item: firstnameTextField, attribute: .top, relatedBy: .equal, toItem: view, attribute: .top, multiplier: 1.0, constant: 250)
        view.addConstraints([leftConstraint2, rightConstraint2, heightConstraint2, widthConstraint2, topConstraint2])
        
        lastnameTextField.tintColor = UIColor.darkGray
        lastnameTextField.textColor = UIColor.black
        lastnameTextField.placeholder = "Last Name"
        self.view.addSubview(lastnameTextField)

        
        lastnameTextField.translatesAutoresizingMaskIntoConstraints = false
        let leftConstraint3 = NSLayoutConstraint(item: lastnameTextField, attribute: .leading, relatedBy: .equal, toItem: view, attribute: .leading, multiplier: 1.0, constant: 40)
        let rightConstraint3 = NSLayoutConstraint(item: lastnameTextField, attribute: .trailing, relatedBy: .equal, toItem: view, attribute: .trailing, multiplier: 1.0, constant: -40)
        let heightConstraint3 = NSLayoutConstraint(item: lastnameTextField, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 40)
        let widthConstraint3 = NSLayoutConstraint(item: lastnameTextField, attribute: .width, relatedBy: .greaterThanOrEqual, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 200)
        let topConstraint3 = NSLayoutConstraint(item: lastnameTextField, attribute: .top, relatedBy: .equal, toItem: view, attribute: .top, multiplier: 1.0, constant: 300)
        view.addConstraints([leftConstraint3, rightConstraint3, heightConstraint3, widthConstraint3, topConstraint3])
        
        emailTextField.tintColor = UIColor.darkGray
        emailTextField.textColor = UIColor.black
        emailTextField.placeholder = "Email"
        self.view.addSubview(emailTextField)

        
        emailTextField.translatesAutoresizingMaskIntoConstraints = false
        let leftConstraint4 = NSLayoutConstraint(item: emailTextField, attribute: .leading, relatedBy: .equal, toItem: view, attribute: .leading, multiplier: 1.0, constant: 40)
        let rightConstraint4 = NSLayoutConstraint(item: emailTextField, attribute: .trailing, relatedBy: .equal, toItem: view, attribute: .trailing, multiplier: 1.0, constant: -40)
        let heightConstraint4 = NSLayoutConstraint(item: emailTextField, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 40)
        let widthConstraint4 = NSLayoutConstraint(item: emailTextField, attribute: .width, relatedBy: .greaterThanOrEqual, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 200)
        let topConstraint4 = NSLayoutConstraint(item: emailTextField, attribute: .top, relatedBy: .equal, toItem: view, attribute: .top, multiplier: 1.0, constant: 350)
        view.addConstraints([leftConstraint4, rightConstraint4, heightConstraint4, widthConstraint4, topConstraint4])
        
        phnumTextField.tintColor = UIColor.darkGray
        phnumTextField.textColor = UIColor.black
        phnumTextField.placeholder = "Phone Number"
        self.view.addSubview(phnumTextField)


        phnumTextField.translatesAutoresizingMaskIntoConstraints = false
        let leftConstraint5 = NSLayoutConstraint(item: phnumTextField, attribute: .leading, relatedBy: .equal, toItem: view, attribute: .leading, multiplier: 1.0, constant: 40)
        let rightConstraint5 = NSLayoutConstraint(item: phnumTextField, attribute: .trailing, relatedBy: .equal, toItem: view, attribute: .trailing, multiplier: 1.0, constant: -40)
        let heightConstraint5 = NSLayoutConstraint(item: phnumTextField, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 40)
        let widthConstraint5 = NSLayoutConstraint(item: phnumTextField, attribute: .width, relatedBy: .greaterThanOrEqual, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 200)
        let topConstraint5 = NSLayoutConstraint(item: phnumTextField, attribute: .top, relatedBy: .equal, toItem: view, attribute: .top, multiplier: 1.0, constant: 400)
        view.addConstraints([leftConstraint5, rightConstraint5, heightConstraint5, widthConstraint5, topConstraint5])
        
        changepwdButton.backgroundColor = UIColor.black
        changepwdButton.setTitle("Change Password", for: .normal)
        changepwdButton.titleLabel?.font =  UIFont(name: "Clanbook", size: 10)
        changepwdButton.setTitleColor(UIColor.white, for: .normal)
        changepwdButton.addTarget(self, action: #selector(changepwdAction), for: .touchUpInside)
        self.view.addSubview(changepwdButton)

        
        changepwdButton.translatesAutoresizingMaskIntoConstraints = false
        let leftConstraint6 = NSLayoutConstraint(item: changepwdButton, attribute: .leading, relatedBy: .equal, toItem: view, attribute: .leading, multiplier: 1.0, constant: 40)
        let heightConstraint6 = NSLayoutConstraint(item: changepwdButton, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 30)
        let widthConstraint6 = NSLayoutConstraint(item: changepwdButton, attribute: .width, relatedBy: .greaterThanOrEqual, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 80)
        let topConstraint6 = NSLayoutConstraint(item: changepwdButton, attribute: .top, relatedBy: .equal, toItem: view, attribute: .top, multiplier: 1.0, constant: 450)
        view.addConstraints([leftConstraint6, heightConstraint6, widthConstraint6, topConstraint6])
        
        securityButton.backgroundColor = UIColor.black
        securityButton.setTitle("Security", for: .normal)
        securityButton.titleLabel?.font =  UIFont(name: "Clanbook", size: 10)
        securityButton.setTitleColor(UIColor.white, for: .normal)
        securityButton.addTarget(self, action: #selector(securityAction), for: .touchUpInside)
        self.view.addSubview(securityButton)
        
        securityButton.translatesAutoresizingMaskIntoConstraints = false

        let rightConstraint7 = NSLayoutConstraint(item: securityButton, attribute: .trailing, relatedBy: .equal, toItem: view, attribute: .trailing, multiplier: 1.0, constant: -40)
        let heightConstraint7 = NSLayoutConstraint(item: securityButton, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 30)
        let widthConstraint7 = NSLayoutConstraint(item: securityButton, attribute: .width, relatedBy: .greaterThanOrEqual, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 80)
        let topConstraint7 = NSLayoutConstraint(item: securityButton, attribute: .top, relatedBy: .equal, toItem: view, attribute: .top, multiplier: 1.0, constant: 450)
        view.addConstraints([rightConstraint7, heightConstraint7, widthConstraint7, topConstraint7])
        
        self.firstnameTextField.delegate = self
        self.lastnameTextField.delegate = self
        self.emailTextField.delegate = self
        self.phnumTextField.delegate = self
    }
    
    func rightAndleftView() {
        firstnameTextField.rightViewMode = .always

        firstnameEditButton.tintColor = UIColor.darkGray
        firstnameEditButton.contentMode = .center
        firstnameEditButton.addTarget(self, action: #selector(self.changeFirstName(_:)), for: UIControlEvents.touchUpInside)
        firstnameEditButton.frame = CGRect(x: 0, y: 0, width: 50, height: 30)
        firstnameEditButton.titleLabel?.font =  UIFont(name: "Clan-Bold", size: 13)
        firstnameEditButton.setTitle("Edit", for: .normal)
        firstnameEditButton.contentHorizontalAlignment = .right
        firstnameEditButton.setTitleColor(UIColor.black, for: .normal)
        firstnameTextField.rightView = firstnameEditButton
        
        lastnameTextField.rightViewMode = .always
        
        lastnameEditButton.tintColor = UIColor.darkGray
        lastnameEditButton.contentMode = .center
        lastnameEditButton.addTarget(self, action: #selector(self.changeLastName(_:)), for: UIControlEvents.touchUpInside)
        lastnameEditButton.frame = CGRect(x: 0, y: 0, width: 50, height: 30)
        lastnameEditButton.titleLabel?.font =  UIFont(name: "Clan-Bold", size: 13)
        lastnameEditButton.setTitle("Edit", for: .normal)
        lastnameEditButton.contentHorizontalAlignment = .right
        lastnameEditButton.setTitleColor(UIColor.black, for: .normal)
        lastnameTextField.rightView = lastnameEditButton

        emailTextField.rightViewMode = .always
        
        emailEditButton.tintColor = UIColor.darkGray
        emailEditButton.contentMode = .center
        emailEditButton.frame = CGRect(x: 0, y: 0, width: 50, height: 30)
        emailEditButton.isUserInteractionEnabled = false
            
        phnumTextField.rightViewMode = .always
        
        phnumEditButton.tintColor = UIColor.darkGray
        phnumEditButton.contentMode = .center
        phnumEditButton.frame = CGRect(x: 0, y: 0, width: 50, height: 30)
        phnumEditButton.isUserInteractionEnabled = false

        self.showCountryCodeView()
    }
    
    func showCountryCodeView() {
        leftphnum.font = UIFont(name: "ClanOT-News", size: 10)
        leftphnum.textColor = UIColor.black
        leftphnum.textAlignment = .center
        phnumViewTextField.addSubview(leftphnum)
        
        if self.selectedCountryCode.contains("+"){
            leftphnum.text = IsoCountryCodes.searchByCallingCode(calllingCode: self.selectedCountryCode).flag + self.selectedCountryCode
            }else{
                leftphnum.text = "Country Code"
            }
    
        phnumViewTextField.frame = CGRect(x: 0, y: 1, width: 80, height: 40)
        leftphnum.frame = CGRect(x: 0, y: 1, width: 80, height: 40)
        phnumTextField.leftViewMode = .always
        phnumTextField.leftView = phnumViewTextField
        leftphnum.isUserInteractionEnabled = true
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.tapFunction(_:)))
        leftphnum.addGestureRecognizer(tap)
    }
    
    @objc func tapFunction(_ sender:UITapGestureRecognizer) {
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        guard let settingsViewController = storyboard.instantiateViewController(withIdentifier: "CountrySelectionTableViewController") as? CountrySelectionTableViewController else {
            return
        }
        settingsViewController.delegate = self
        let barButtonItem = UIBarButtonItem(image: UIImage(named: "close"),style: .done, target: self, action: #selector(dismissSettings))
        
        settingsViewController.navigationItem.rightBarButtonItem = barButtonItem
        settingsViewController.title = "Select a Country"
        
        let navigationController = UINavigationController(rootViewController: settingsViewController)
        navigationController.navigationBar.tintColor = UIColor.darkGray
        navigationController.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor : UIColor.darkGray];
        navigationController.modalPresentationStyle = .popover
        navigationController.popoverPresentationController?.delegate = self
        navigationController.preferredContentSize = CGSize(width: self.view.bounds.size.width - 10, height: self.view.bounds.size.height - 60)
        self.present(navigationController, animated: true, completion: nil)
        navigationController.popoverPresentationController?.sourceView = leftphnum
        navigationController.popoverPresentationController?.backgroundColor = UIColor.white
        navigationController.popoverPresentationController?.sourceRect = leftphnum.bounds
    }
    
    func sendData(countryCode: String, counrtyName: String, countryFlag: String) {
        DispatchQueue.main.async {
            self.leftphnum.text = countryFlag + " " + countryCode
            self.selectedCountryCode = countryCode
        }
    }
    
    @objc func dismissSettings() {
        self.dismiss(animated: true, completion: nil)
    }

    func displayMyAlertMessage(userMessage:String){
        let myAlert = UIAlertController(title: "Alert", message: userMessage, preferredStyle: UIAlertControllerStyle.alert)
        
        let okAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil)
        
        myAlert.addAction(okAction)
        self.present(myAlert, animated: true, completion: nil)
    }

    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == firstnameTextField {
            if firstnameEditButton.titleLabel?.text == "Save"{
                return true
            }else{
                return false
            }
        }else if textField == lastnameTextField {
            if lastnameEditButton.titleLabel?.text == "Save"{
                return true
            }else{
                return false
            }
        }else if textField == phnumTextField {
            
        }else if textField == emailTextField {

        }
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.textColor = UIColor.darkGray
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        textField.textColor = UIColor.darkGray
    }
    
    func textFieldShouldReturn(_ userText: UITextField) -> Bool {
        if userText == self.firstnameTextField {
            self.lastnameTextField.becomeFirstResponder()
        }else if userText == self.lastnameTextField{
            self.phnumTextField.becomeFirstResponder()
        }else if userText == phnumTextField{

        }
        return true
    }

    @objc func changeFirstName(_ sender:UIButton){
        if sender.titleLabel?.text == "Edit"{
            firstnameEditButton.setTitle("Save", for: .normal)
            firstnameTextField.text = self.oldFirstName
            emailTextField.text = self.oldEmail
            lastnameTextField.text = self.oldLastName
            phnumTextField.text = self.oldPhone
            firstnameTextField.becomeFirstResponder()
        }else{
            firstnameEditButton.setTitle("Edit", for: .normal)
            if firstnameTextField.text != self.oldFirstName{
                if !(firstnameTextField.text?.trimmingCharacters(in: .whitespaces).isEmpty)!{
                    let params = User()
                    params.firstname = firstnameTextField.text
                    User.instance.updateMe(userUpdate: params, callback: { (user) in
                        DispatchQueue.main.async {
                            self.oldFirstName = User.instance.firstname!
                            self.displayMyAlertMessage(userMessage: "Profile updated")
                        }

                    }, failure: { (error) in
                        if let error =  error as? APIError{
                            guard  let des = error.error_description else{return}
                            print(des)
                        }else if let error =  error as? Error{
                            print(error.localizedDescription)
                        }
                    })
                }else{
                    let message = "First name can't be empty"
                    self.displayMyAlertMessage(userMessage: message)
                    self.firstnameTextField.text = self.oldFirstName
                }
            }
        }
    }
    
    
    @objc func changeLastName(_ sender:UIButton){
        if sender.titleLabel?.text == "Edit"{
            lastnameEditButton.setTitle("Save", for: .normal)
            firstnameTextField.text = self.oldFirstName
            emailTextField.text = self.oldEmail
            lastnameTextField.text = self.oldLastName
            phnumTextField.text = self.oldPhone
            lastnameTextField.becomeFirstResponder()
        }else{
            lastnameEditButton.setTitle("Edit", for: .normal)
            if lastnameTextField.text != self.oldLastName
            {
                if !(lastnameTextField.text?.trimmingCharacters(in: .whitespaces).isEmpty)!{
                    let params = User()
                    params.lastname = lastnameTextField.text
                    User.instance.updateMe(userUpdate: params, callback: { (user) in
                        DispatchQueue.main.async {
                            self.oldLastName = User.instance.lastname!
                            self.displayMyAlertMessage(userMessage: "Profile updated")
                        }
                        
                    }, failure: { (error) in
                        if let error =  error as? APIError{
                            guard  let des = error.error_description else{return}
                            print(des)
                        }else if let error =  error as? Error{
                            print(error.localizedDescription)
                        }
                    })
                }else{
                    let message = "Last name can't be empty"
                    self.displayMyAlertMessage(userMessage: message)
                    self.lastnameTextField.text = self.oldLastName
                }
            }
        }
    }
    
    @objc func changepwdAction() {
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        let controller: ChangePasswordViewController = storyboard.instantiateViewController(withIdentifier: "ChangePasswordViewController") as! ChangePasswordViewController
        self.navigationController?.pushViewController(controller, animated: false)

    }

    @objc func securityAction() {
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        let controller: SecurityViewController = storyboard.instantiateViewController(withIdentifier: "SecurityViewController") as! SecurityViewController
        self.navigationController?.pushViewController(controller, animated: false)
    }

    @objc func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        let selectedImage = info[UIImagePickerControllerOriginalImage] as! UIImage
        
        let rep = UIImagePNGRepresentation(selectedImage)
        imageView.image = selectedImage
        
        dismiss(animated: true, completion: nil)
        
        User.instance.uploadProfileImage(imageData: rep!, callback: { (bool) in
            DispatchQueue.main.async {
                print("Upload Success")
            }
        }) { (error) in
            if let error =  error as? APIError{
                guard  let des = error.error_description else{return}
                print(des)
            }else if let error =  error as? Error{
                print(error.localizedDescription)
            }
            
        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        let controller = UIImagePickerController()
        controller.delegate = self
        controller.sourceType = .photoLibrary
        
        self.present(controller, animated: true, completion: nil)
    }
}
