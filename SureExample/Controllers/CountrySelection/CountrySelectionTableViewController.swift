//
//  CountrySelectionTableViewController.swift
//  Sure
//
//  Created by Krishna Thakur on 8/10/17.
//  Copyright © 2017 PiOctave. All rights reserved.
//

import UIKit

protocol CountrySelected{
    func sendData(countryCode: String, counrtyName: String, countryFlag:String)
}

class CountrySelectionTableViewController: UITableViewController,UISearchBarDelegate, UISearchDisplayDelegate {

    @IBOutlet weak var searchBar: UISearchBar!
    
    var filteredList = [IsoCountryInfo]()
    var delegate: CountrySelected?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let textFieldInsideSearchBar = self.searchBar.value(forKey: "searchField") as! UITextField
        textFieldInsideSearchBar.leftViewMode = UITextFieldViewMode.always
        searchBar.showsScopeBar = true
        searchBar.delegate = self
        searchBar.returnKeyType = .search
        filteredList = IsoCountries.allCountries
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    // This method updates filteredData based on the text in the Search Box
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if let key = searchBar.text{
            filteredList = IsoCountryCodes.searchByAnyKey(searchKey: key)
            self.tableView.reloadData()
        }
    }
    
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        if let key = searchBar.text{
            filteredList = IsoCountryCodes.searchByAnyKey(searchKey: key)
            self.tableView.reloadData()
        }
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.text = ""
        searchBar.showsCancelButton = false
        searchBar.endEditing(true)
        self.view.endEditing(true)
        
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar){
        searchBar.showsCancelButton = true
    }
    
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return filteredList.count
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! CountryTableViewCell
        cell.countryName.text = filteredList[indexPath.row].flag + "  " + filteredList[indexPath.row].calling + "  " + filteredList[indexPath.row].name
        return cell
    }
    
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if (self.delegate) != nil
        {
            delegate?.sendData(countryCode: filteredList[indexPath.row].calling, counrtyName: filteredList[indexPath.row].name, countryFlag: filteredList[indexPath.row].flag)
            self.dismiss(animated: true, completion: nil)
        }
        
    }
    
}

