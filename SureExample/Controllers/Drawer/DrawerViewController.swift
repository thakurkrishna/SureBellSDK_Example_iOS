//
//  DrawerViewController.swift
//  SureBell_Example
//
//  Created by piOctave on 5/16/18.
//  Copyright © 2018 CocoaPods. All rights reserved.
//

import UIKit
import SureBell


class DrawerViewController:UIViewController,UITableViewDataSource,UITableViewDelegate {
    
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var tableView: UITableView!
    typealias SettingInfo = (title: String, titleImage:String)
    fileprivate var items = [SettingInfo]()
    var index:IndexPath?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let customBackButton = UIBarButtonItem(image: UIImage(named: "back") , style: .plain, target: self, action: #selector(backAction(sender:)))
        customBackButton.imageInsets = UIEdgeInsets(top: 2, left: -8, bottom: 0, right: 0)
        navigationItem.leftBarButtonItem = customBackButton

        
        items.append(("Add New Device","drawer_sure"))
        items.append(("Logout","drawer_logout"))
        
        tableView.dataSource = self
        tableView.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        navigationController?.navigationBar.barStyle = UIBarStyle.black
        navigationController?.navigationBar.tintColor = UIColor.white
        self.navigationItem.title = "Settings"
    }
    
    @objc func backAction(sender: UIBarButtonItem) {
        navigationController?.popViewController(animated: true)
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! DrawerTableViewCell
        
        if items.count > 0{
            cell.drawerTitle.text =  items[indexPath.row].title
            cell.itemImage.image = UIImage(named:items[indexPath.row].titleImage)
        }else {
            
        }
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        _ = tableView.cellForRow(at: indexPath) as! DrawerTableViewCell
        if items[indexPath.row].title == "Logout"{
            let message = "Are You Sure, You Want To Logout"
            
            // Create the dialog
            let myAlert = UIAlertController(title: message, message: "", preferredStyle: UIAlertControllerStyle.actionSheet)
            
            let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.default, handler: nil)
            myAlert.addAction(cancelAction)
            self.present(myAlert, animated: true, completion: nil)
            
            myAlert.addAction(UIAlertAction(title: "Logout", style: UIAlertActionStyle.destructive, handler: { action in
                DispatchQueue.main.async {
                    User.instance.deleteNotificationToken(token: "token", callback: { (user) in
                        Token.instance.deleteToken()
                        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
                        let login: LoginViewController = storyboard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
                        self.navigationController?.pushViewController(login, animated: false)

                    }, failure: { (error) in
                        if let error =  error as? APIError{
                            guard  let des = error.error_description else{return}
                            print(des)
                        }else if let error =  error as? Error{
                            print(error.localizedDescription)
                        }
                    })
                }

            }))
        }else if items[indexPath.row].title == "Add New Device"{
            
            let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
            let deviceadd: DeviceNameViewController = storyboard.instantiateViewController(withIdentifier: "DeviceNameViewController") as! DeviceNameViewController
            self.navigationController?.pushViewController(deviceadd, animated: true)
        }
    }
}

