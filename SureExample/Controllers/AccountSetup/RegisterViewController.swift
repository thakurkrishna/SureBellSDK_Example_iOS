//
//  ViewController.swift
//  SureBell
//
//  Created by Krishna Thakur on 04/18/2018.
//  Copyright (c) 2018 Krishna Thakur. All rights reserved.
//

import UIKit
import SureBell

class RegisterViewController: UIViewController, UITextFieldDelegate, UIPopoverPresentationControllerDelegate, CountrySelected {
    
    @IBOutlet weak var firstName: UITextField!
    @IBOutlet weak var lastName: UITextField!
    @IBOutlet weak var email: UITextField!
    @IBOutlet weak var phone: UITextField!
    @IBOutlet weak var password: UITextField!
    @IBOutlet weak var con_password: UITextField!
    
    @IBAction func login(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        let login: LoginViewController = storyboard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
        self.navigationController?.pushViewController(login, animated: true)
    }
    @IBAction func register(_ sender: Any) {
        registerUser()
    }
    @IBOutlet weak var register: UIButton!
    @IBOutlet weak var login: UIButton!
    
    var btnLeftMenu: UIButton?
    var btnLeftMenu2: UIButton?
    var passwordiconClick = true
    var conpasswordClick = true
    var selectedCountryCode = "+91"
    let leftTextEmail = UILabel()
    let leftViewEmail = UIView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.viewSetup()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == self.phone{
            if let text = phone.text{
                if text.count >= 2{
                    self.showCountryCodeView()
                }else{
                    self.hideCountryCode()
                }
            }
            
            let newString = (textField.text! as NSString).replacingCharacters(in: range, with: string) as NSString
            let nsString = NSString(string: textField.text!)
            let newText = nsString.replacingCharacters(in: range, with: string)
            guard range.location == 0 else {
                if newText.count <= 20{
                    return true
                }else{
                    return  newText.count <= 20 && newString.rangeOfCharacter(from: NSCharacterSet.whitespacesAndNewlines).location != 0
                }
            }
            return  newText.count <= 20 && newString.rangeOfCharacter(from: NSCharacterSet.whitespacesAndNewlines).location != 0
            
        }else{
            let newString = (textField.text! as NSString).replacingCharacters(in: range, with: string) as NSString
            let nsString = NSString(string: textField.text!)
            let newText = nsString.replacingCharacters(in: range, with: string)
            guard range.location == 0 else {
                if newText.count <= 50{
                    return true
                }else{
                    return  newText.count <= 50 && newString.rangeOfCharacter(from: NSCharacterSet.whitespacesAndNewlines).location != 0
                }
            }
            return  newText.count <= 50 && newString.rangeOfCharacter(from: NSCharacterSet.whitespacesAndNewlines).location != 0
        }
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == self.phone{
            if let text = phone.text{
                if text.count >= 1{
                    self.showCountryCodeView()
                }else{
                    self.hideCountryCode()
                }
            }
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField, reason: UITextFieldDidEndEditingReason) {
        if let text = phone.text{
            if text.count >= 1{
                self.showCountryCodeView()
            }else{
                self.hideCountryCode()
            }
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool{
        if textField == self.firstName {
            self.lastName.becomeFirstResponder()
        }else if textField == self.lastName {
            self.email.becomeFirstResponder()
        }else if textField == self.email{
            self.phone!.becomeFirstResponder()
        }else if textField == self.phone{
            self.password!.becomeFirstResponder()
        }else if textField == self.password{
            self.con_password.becomeFirstResponder()
        }else if textField == self.con_password{
            registerUser()
        }
        return true
    }
    
    func showCountryCodeView(){
        leftTextEmail.font = UIFont(name: "ClanOT-News", size: 15)
        leftTextEmail.textColor = UIColor.gray
        leftViewEmail.addSubview(leftTextEmail)
        
        leftViewEmail.frame = CGRect(x: 0, y: 1, width: 80, height: phone.frame.height)
        leftTextEmail.frame = CGRect(x: 0, y: 1, width: 80, height: phone.frame.height)
        phone!.leftViewMode = .always
        phone!.leftView = leftViewEmail
        if selectedCountryCode.contains("+"){
            
            leftTextEmail.text = IsoCountryCodes.searchByCallingCode(calllingCode: selectedCountryCode).flag + selectedCountryCode
        }else{
            leftTextEmail.text = "Country Code"
        }
        leftTextEmail.isUserInteractionEnabled = true
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.tapFunction(_:)))
        leftTextEmail.addGestureRecognizer(tap)
    }
    
    @objc func tapFunction(_ sender:UITapGestureRecognizer) {
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        guard let settingsViewController = storyboard.instantiateViewController(withIdentifier: "CountrySelectionTableViewController") as? CountrySelectionTableViewController else {
            return
        }
        settingsViewController.delegate = self
        let barButtonItem = UIBarButtonItem(image: UIImage(named: "close"),style: .done, target: self, action: #selector(dismissSettings))
        barButtonItem.isAccessibilityElement = true
        settingsViewController.navigationItem.rightBarButtonItem = barButtonItem
        settingsViewController.title = "Select a Country"

        let navigationController = UINavigationController(rootViewController: settingsViewController)
        navigationController.navigationBar.tintColor = UIColor.darkGray
        navigationController.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor : UIColor.darkGray];
        navigationController.modalPresentationStyle = .popover
        navigationController.popoverPresentationController?.delegate = self
        navigationController.preferredContentSize = CGSize(width: self.view.bounds.size.width - 10, height: self.view.bounds.size.height - 60)
        self.present(navigationController, animated: true, completion: nil)
        navigationController.popoverPresentationController?.sourceView = leftTextEmail
        navigationController.popoverPresentationController?.backgroundColor = UIColor.white
        navigationController.popoverPresentationController?.sourceRect = leftTextEmail.bounds
    }

    @objc func dismissSettings() {
        self.dismiss(animated: true, completion: nil)
    }

    func hideCountryCode(){
        leftTextEmail.text = ""
        self.leftViewEmail.removeFromSuperview()
        self.leftTextEmail.removeFromSuperview()
        phone!.leftViewMode = .never
        leftTextEmail.isUserInteractionEnabled = false
    }

    func sendData(countryCode: String, counrtyName: String, countryFlag: String) {
        DispatchQueue.main.async {
            self.leftTextEmail.text = countryFlag + " " + countryCode
            self.selectedCountryCode = countryCode
        }
    }
    
    func viewSetup(){
        register.setTitleColor(UIColor.white, for: UIControlState())
        login.setTitleColor(UIColor.darkGray, for: UIControlState())
        register.backgroundColor = UIColor.black
        login.backgroundColor = UIColor.white
        
        password!.rightViewMode = .always
        
        btnLeftMenu = UIButton()
        let origImage = UIImage(named: "ic_eye_closed");
        let tintedImage = origImage?.withRenderingMode(UIImageRenderingMode.alwaysTemplate)
        btnLeftMenu?.setImage(tintedImage, for: UIControlState())
        btnLeftMenu?.tintColor = UIColor.darkGray
        btnLeftMenu?.contentMode = .center
        btnLeftMenu?.isAccessibilityElement = true
        btnLeftMenu!.addTarget(self, action: #selector(self.togglePasswordVisible(_:)), for: UIControlEvents.touchUpInside)
        btnLeftMenu!.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        password!.rightView = btnLeftMenu
        
        con_password!.rightViewMode = .always
        
        btnLeftMenu2 = UIButton()
        let origImage2 = UIImage(named: "ic_eye_closed");
        let tintedImage2 = origImage2?.withRenderingMode(UIImageRenderingMode.alwaysTemplate)
        btnLeftMenu2?.setImage(tintedImage2, for: UIControlState())
        btnLeftMenu2?.tintColor = UIColor.darkGray
        btnLeftMenu2?.contentMode = .center
        btnLeftMenu2?.isAccessibilityElement = true
        btnLeftMenu2!.addTarget(self, action: #selector(self.toggleconPasswordVisible(_:)), for: UIControlEvents.touchUpInside)
        btnLeftMenu2!.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        con_password.rightView = btnLeftMenu2
        
        self.firstName!.delegate = self
        firstName!.placeholder = "First Name"
        self.lastName!.delegate = self
        lastName!.placeholder = "Last Name"
        self.email!.delegate = self
        email!.placeholder = "Email"
        self.phone!.delegate = self
        phone!.placeholder = "Phone"
        self.password!.delegate = self
        password!.placeholder = "Password"
        self.con_password!.delegate = self
        con_password!.placeholder = "Confirm Password"
    }
    
    @objc func togglePasswordVisible(_ sender:AnyObject){
        if(passwordiconClick == true) {
            password!.isSecureTextEntry = false
            let origImage = UIImage(named: "ic_eye_open");
            let tintedImage = origImage?.withRenderingMode(UIImageRenderingMode.alwaysTemplate)
            btnLeftMenu!.setImage(tintedImage, for: UIControlState())
            btnLeftMenu!.tintColor = UIColor.darkGray
            password?.keyboardType = .asciiCapable
            passwordiconClick = false
        } else {
            password!.isSecureTextEntry = true
            let origImage = UIImage(named: "ic_eye_closed");
            let tintedImage = origImage?.withRenderingMode(UIImageRenderingMode.alwaysTemplate)
            btnLeftMenu!.setImage(tintedImage, for: UIControlState())
            btnLeftMenu!.tintColor = UIColor.darkGray
            password?.keyboardType = .asciiCapable
            passwordiconClick = true
        }
    }
    
    @objc func toggleconPasswordVisible(_ sender:AnyObject){
        if(conpasswordClick == true) {
            con_password!.isSecureTextEntry = false
            let origImage = UIImage(named: "ic_eye_open");
            let tintedImage = origImage?.withRenderingMode(UIImageRenderingMode.alwaysTemplate)
            btnLeftMenu2!.setImage(tintedImage, for: UIControlState())
            btnLeftMenu2!.tintColor = UIColor.darkGray
            con_password?.keyboardType = .asciiCapable
            conpasswordClick = false
        } else {
            con_password!.isSecureTextEntry = true
            let origImage = UIImage(named: "ic_eye_closed");
            let tintedImage = origImage?.withRenderingMode(UIImageRenderingMode.alwaysTemplate)
            btnLeftMenu2!.setImage(tintedImage, for: UIControlState())
            btnLeftMenu2!.tintColor = UIColor.darkGray
            con_password?.keyboardType = .asciiCapable
            conpasswordClick = true
        }
    }
    
    func displayMyAlertMessage(userMessage:String){
        let myAlert = UIAlertController(title: "Alert", message: userMessage, preferredStyle: UIAlertControllerStyle.alert)
        
        let okAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil)
        
        myAlert.addAction(okAction)
        self.present(myAlert, animated: true, completion: nil)
    }
    
    func checkEmptyValidation() -> Bool{
        var message:String = ""
        if firstName!.text == "" || (firstName!.text?.isEmpty)!{
            message = "Firstname Required"
            displayMyAlertMessage(userMessage: message)
            return false
        }else if lastName!.text == "" || (lastName!.text?.isEmpty)!{
            message = "Lastname Required"
            displayMyAlertMessage(userMessage: message)
            return false
        }else if email.text == ""{
            message = "Email Required"
            displayMyAlertMessage(userMessage: message)
            return false
        }else if phone.text == ""{
            message = "Phone Number Required"
            displayMyAlertMessage(userMessage: message)
            return false
        }else if password!.text == ""{
            message = "Password Required"
            displayMyAlertMessage(userMessage: message)
            return false
        }else if con_password.text == ""{
            message = "Confirm Password Required"
            displayMyAlertMessage(userMessage: message)
            return false
        }else{
            return true
        }
    }
    
    func checkPasswordCharLimit()-> Bool{
        var message:String = ""
        if let password = password?.text{
            if password.count >= 6{
                return true
            }else{
                message = "Password Should be Greater than 5 Characters"
                displayMyAlertMessage(userMessage: message)
                return false
            }
        }else{
            return false
        }
    }
    
    func checkPasswordMatch() -> Bool{
        if password?.text != con_password.text{
            let message = "Password and Confirm Password Should be Same"
            displayMyAlertMessage(userMessage: message)
            return  false
        }else{
            return  true
        }
    }
    
    func isValidEmail(_ testStr:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        if !emailTest.evaluate(with: testStr){
            displayMyAlertMessage(userMessage: "Invalid Email")
        }
        return emailTest.evaluate(with: testStr)
    }
    
    func isValidPhone(_ testStr:String) -> Bool {
        let PHONE_REGEX = "^[1-9]{1}[0-9]+$"
        let phoneTest = NSPredicate(format: "SELF MATCHES %@", PHONE_REGEX)
        let result =  phoneTest.evaluate(with: testStr)
        if !result{
            displayMyAlertMessage(userMessage: "Invalid Phone")
        }
        return  result
    }
    
    func registerUser() {
        if  SharedData.AppSetting.IS_INTERNET_CONNECTED{
            if checkEmptyValidation(){
                if isValidEmail(email.text!.trimmingCharacters(in: CharacterSet.whitespaces)) && isValidPhone(phone.text!){
                    if checkPasswordCharLimit(){
                        if checkPasswordMatch(){
                            User.instance.create(firstName: firstName.text!, lastName: lastName.text!, email: email.text!.trimmingCharacters(in: CharacterSet.whitespaces), phone: self.selectedCountryCode + phone.text!, password: password!.text!.trimmingCharacters(in: CharacterSet.whitespaces), countryDialCode: self.selectedCountryCode, callback: { (user) in
                            self.displayMyAlertMessage(userMessage: "User registered successfully, go back and login")
                            }, failure: { (error) in
                                if let error =  error as? APIError{
                                    guard  let des = error.error_description else{return}
                                    print(des)
                                }else if let error =  error as? Error{
                                    print(error.localizedDescription)
                                }
                            })
                        }
                    }
                }
            }
        }else{
        self.displayMyAlertMessage(userMessage: "No Internet Connection")
        }
    }
}


