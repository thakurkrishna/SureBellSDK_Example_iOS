//
//  ResetPasswordViewController.swift
//  SureBell_Example
//
//  Created by piOctave on 5/14/18.
//  Copyright © 2018 CocoaPods. All rights reserved.
//

import UIKit
import SureBell

class ResetPasswordViewController: UIViewController {
    
    @IBOutlet weak var resetCode: UITextField!
    @IBOutlet weak var login: UIButton!
    @IBOutlet weak var password: UITextField!
    @IBOutlet weak var con_password: UITextField!
    @IBOutlet weak var reset: UIButton!
    @IBAction func login(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        let login: LoginViewController = storyboard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
        self.navigationController?.pushViewController(login, animated: true)
    }
    @IBAction func reset(_ sender: Any) {
        resetAction()
    }
    
    var email:String?
    var btnLeftMenu: UIButton?
    var btnLeftMenu2: UIButton?
    var passwordiconClick = true
    var conpasswordClick = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.viewSetup()
        self.title = "Reset Password"

        login.setTitleColor(UIColor.darkGray, for: UIControlState())
        reset.setTitleColor(UIColor.white, for: UIControlState())
        
        login.backgroundColor = UIColor.white
        reset.backgroundColor = UIColor.black
        
        resetCode.placeholder = "Reset Code"
        password.placeholder = "Password"
        con_password.placeholder = "Confirm Password"
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }

    func viewSetup(){
        
        password!.rightViewMode = .always
        
        btnLeftMenu = UIButton()
        let origImage = UIImage(named: "ic_eye_closed");
        let tintedImage = origImage?.withRenderingMode(UIImageRenderingMode.alwaysTemplate)
        btnLeftMenu?.setImage(tintedImage, for: UIControlState())
        btnLeftMenu?.tintColor = UIColor.darkGray
        btnLeftMenu?.contentMode = .center
        btnLeftMenu?.isAccessibilityElement = true
        btnLeftMenu!.addTarget(self, action: #selector(self.togglePasswordVisible(_:)), for: UIControlEvents.touchUpInside)
        btnLeftMenu!.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        password!.rightView = btnLeftMenu
        
        con_password!.rightViewMode = .always
        
        btnLeftMenu2 = UIButton()
        let origImage2 = UIImage(named: "ic_eye_closed");
        let tintedImage2 = origImage2?.withRenderingMode(UIImageRenderingMode.alwaysTemplate)
        btnLeftMenu2?.setImage(tintedImage2, for: UIControlState())
        btnLeftMenu2?.tintColor = UIColor.darkGray
        btnLeftMenu2?.contentMode = .center
        btnLeftMenu2?.isAccessibilityElement = true
        btnLeftMenu2!.addTarget(self, action: #selector(self.toggleconPasswordVisible(_:)), for: UIControlEvents.touchUpInside)
        btnLeftMenu2!.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        con_password.rightView = btnLeftMenu2
    }
    
    @objc func togglePasswordVisible(_ sender:AnyObject){
        if(passwordiconClick == true) {
            password!.isSecureTextEntry = false
            let origImage = UIImage(named: "ic_eye_open");
            let tintedImage = origImage?.withRenderingMode(UIImageRenderingMode.alwaysTemplate)
            btnLeftMenu!.setImage(tintedImage, for: UIControlState())
            btnLeftMenu!.tintColor = UIColor.darkGray
            password?.keyboardType = .asciiCapable
            passwordiconClick = false
        } else {
            password!.isSecureTextEntry = true
            let origImage = UIImage(named: "ic_eye_closed");
            let tintedImage = origImage?.withRenderingMode(UIImageRenderingMode.alwaysTemplate)
            btnLeftMenu!.setImage(tintedImage, for: UIControlState())
            btnLeftMenu!.tintColor = UIColor.darkGray
            password?.keyboardType = .asciiCapable
            passwordiconClick = true
        }
    }
    
    @objc func toggleconPasswordVisible(_ sender:AnyObject){
        if(conpasswordClick == true) {
            con_password!.isSecureTextEntry = false
            let origImage = UIImage(named: "ic_eye_open");
            let tintedImage = origImage?.withRenderingMode(UIImageRenderingMode.alwaysTemplate)
            btnLeftMenu2!.setImage(tintedImage, for: UIControlState())
            btnLeftMenu2!.tintColor = UIColor.darkGray
            con_password?.keyboardType = .asciiCapable
            conpasswordClick = false
        } else {
            con_password!.isSecureTextEntry = true
            let origImage = UIImage(named: "ic_eye_closed");
            let tintedImage = origImage?.withRenderingMode(UIImageRenderingMode.alwaysTemplate)
            btnLeftMenu2!.setImage(tintedImage, for: UIControlState())
            btnLeftMenu2!.tintColor = UIColor.darkGray
            con_password?.keyboardType = .asciiCapable
            conpasswordClick = true
        }
    }
    
    func displayMyAlertMessage(userMessage:String){
        let myAlert = UIAlertController(title: "Alert", message: userMessage, preferredStyle: UIAlertControllerStyle.alert)
        
        let okAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil)
        
        myAlert.addAction(okAction)
        self.present(myAlert, animated: true, completion: nil)
    }
    
    func checkEmptyValidation() -> Bool{
        var message:String = ""
        if resetCode!.text == "" || (resetCode!.text?.isEmpty)!{
            message = "Reset Code Required"
            displayMyAlertMessage(userMessage: message)
            return false
        }else if password!.text == "" || (password!.text?.isEmpty)!{
            message = "Password Required"
            displayMyAlertMessage(userMessage: message)
            return false
        }else if con_password!.text == "" || (con_password!.text?.isEmpty)!{
            message = "Confirm Password Required"
            displayMyAlertMessage(userMessage: message)
            return false
        }else{
            return true
        }
    }
    
    func checkPasswordCharLimit()-> Bool{
        var message:String = ""
        if let password = password?.text{
            if password.count >= 6{
                return true
            }else{
                message = "Password Should be Greater than 5 Characters"
                displayMyAlertMessage(userMessage: message)
                return false
            }
        }else{
            return false
        }
    }
    
    func checkPasswordMatch() -> Bool{
        if password?.text != con_password.text{
            let message = "Password and Confirm Password Should be Same"
            displayMyAlertMessage(userMessage: message)
            return  false
        }else{
            return  true
        }
    }

    func resetAction(){
        if  SharedData.AppSetting.IS_INTERNET_CONNECTED{
        if checkEmptyValidation(){
            if checkPasswordMatch(){
                    if checkPasswordCharLimit(){
                        User.instance.resetPassword(parameters: ["password_reset_code": resetCode!.text!.trimmingCharacters(in: CharacterSet.whitespaces), "email": User.instance.email!.trimmingCharacters(in: CharacterSet.whitespaces), "password": password!.text!.trimmingCharacters(in: CharacterSet.whitespaces)], callback: { (StatusResponse) in
                            self.displayMyAlertMessage(userMessage: "Password Reset Success")
                        }, failure: { (error) in
                            if let error =  error as? APIError{
                                guard  let des = error.error_description else{return}
                                print(des)
                            }else if let error =  error as? Error{
                                print(error.localizedDescription)
                            }
                        })
                }
            }
        }
        }else{
                self.displayMyAlertMessage(userMessage: "No Internet Connection")
        }
    }
    
}
