//
//  LoginViewController.swift
//  SureBell_Example
//
//  Created by piOctave on 5/11/18.
//  Copyright © 2018 CocoaPods. All rights reserved.
//

import UIKit
import SureBell

class LoginViewController: UIViewController, UITextFieldDelegate, UIPopoverPresentationControllerDelegate, CountrySelected {
    
    
    @IBOutlet weak var userName: UITextField!
    @IBOutlet weak var password: UITextField!
    
    @IBOutlet weak var loginwithOTP: UIButton!
    
    var selectedCountryCode = "+91"
    var loginWithOtpPhone = ""
    let leftTextUsername = UILabel()
    let leftViewUsername = UIView()
    var isNewUser = false
    var loginWithOTP = false
    var phverify = false
    var device = [Device]()
    let appDelegate = UIApplication.shared.delegate as! AppDelegate

    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        self.userName.backgroundColor = UIColor.white
        self.password.backgroundColor = UIColor.white
        
        if textField == self.userName{
            self.password.text = ""
            if let text = textField.text{
                if text.isNumeric{
                    if text.count >= 5{
                        self.showCountryCodeView()
                    }else{
                        self.hideCountryCode()
                    }
                }else{
                    self.hideCountryCode()
                }
            }else{
                self.hideCountryCode()
            }
        }
        
        let newString = (textField.text! as NSString).replacingCharacters(in: range, with: string) as NSString
        let nsString = NSString(string: textField.text!)
        let newText = nsString.replacingCharacters(in: range, with: string)
        guard range.location == 0 else {
            if newText.count <= 50{
                return true
            }else{
                return  newText.count <= 50 && newString.rangeOfCharacter(from: NSCharacterSet.whitespacesAndNewlines).location != 0
            }
        }
        return  newText.count <= 50 && newString.rangeOfCharacter(from: NSCharacterSet.whitespacesAndNewlines).location != 0
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        self.userName.backgroundColor = UIColor.white
        self.password.backgroundColor = UIColor.white
        self.isNewUser = true
        if textField == self.userName{
        }
    }
    
    func showCountryCodeView(){
        
        leftTextUsername.font = UIFont(name: "ClanOT-News", size: 15)
        leftTextUsername.textColor = UIColor.gray
        leftTextUsername.textAlignment = .center
        leftViewUsername.addSubview(leftTextUsername)
        
        
        leftViewUsername.frame = CGRect(x: 0, y: 1, width: 80, height: userName.frame.height)
        leftTextUsername.frame = CGRect(x: 0, y: 1, width: 80, height: userName.frame.height)
        userName.leftViewMode = .always
        userName.leftView = leftViewUsername
        if selectedCountryCode.contains("+"){
            
            leftTextUsername.text = IsoCountryCodes.searchByCallingCode(calllingCode: selectedCountryCode).flag + selectedCountryCode
        }else{
            leftTextUsername.text = "Country Code"
        }
        leftTextUsername.isUserInteractionEnabled = true
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.tapFunction(_:)))
        leftTextUsername.addGestureRecognizer(tap)
        self.loginwithOTP.isHidden = false
    }
    
    func hideCountryCode(){
        leftTextUsername.text = ""
        self.leftTextUsername.removeFromSuperview()
        self.leftTextUsername.removeFromSuperview()
        userName.leftViewMode = .never
        leftTextUsername.isUserInteractionEnabled = false
        self.loginwithOTP.isHidden = true
    }
    
    @objc func tapFunction(_ sender:UITapGestureRecognizer) {
        self.dismissKeyboard()
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        guard let settingsViewController = storyboard.instantiateViewController(withIdentifier: "CountrySelectionTableViewController") as? CountrySelectionTableViewController else {
            return
        }
        settingsViewController.delegate = self
        let barButtonItem = UIBarButtonItem(image: UIImage(named: "close"),style: .done, target: self, action: #selector(dismissSettings))
        
        settingsViewController.navigationItem.rightBarButtonItem = barButtonItem
        settingsViewController.title = "Select a Country"
        
        let navigationController = UINavigationController(rootViewController: settingsViewController)
        navigationController.navigationBar.tintColor = UIColor.darkGray
        navigationController.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor : UIColor.darkGray];
        navigationController.modalPresentationStyle = .popover
        navigationController.popoverPresentationController?.delegate = self
        navigationController.preferredContentSize = CGSize(width: self.view.bounds.size.width - 10, height: self.view.bounds.size.height - 60)
        self.present(navigationController, animated: true, completion: nil)
        navigationController.popoverPresentationController?.sourceView = leftTextUsername
        navigationController.popoverPresentationController?.backgroundColor = UIColor.white
        navigationController.popoverPresentationController?.sourceRect = leftTextUsername.bounds
    }
    
    
    func sendData(countryCode: String, counrtyName: String, countryFlag: String) {
        self.leftTextUsername.text = countryFlag + " " + countryCode
        self.selectedCountryCode = countryCode
    }
    
    @objc
    func dismissSettings() {
        self.dismiss(animated: true, completion: nil)
    }

    func dismissKeyboard() {
        view.endEditing(true)
    }
    
    
    func isValidEmail(_ testStr:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        if !emailTest.evaluate(with: testStr){
            displayMyAlertMessage(userMessage: "Invalid Email")
        }
        return emailTest.evaluate(with: testStr)
    }
    
    func isValidPhone(_ testStr:String) -> Bool {
        let PHONE_REGEX = "^[1-9]{1}[0-9]+$"
        let phoneTest = NSPredicate(format: "SELF MATCHES %@", PHONE_REGEX)
        let result =  phoneTest.evaluate(with: testStr)
        if !result{
            displayMyAlertMessage(userMessage: "Invalid Phone")
        }
        return  result
    }
    
    @IBAction func loginwithOTP(_ sender: Any) {
        let loginWithOtpPhone = self.selectedCountryCode + userName.text!
        if loginWithOtpPhone != "" {
            if isValidPhone(userName.text!){
                    var user = ""
                    if userName.text!.isNumeric{
                        user = self.selectedCountryCode + userName.text!.trimmingCharacters(in: CharacterSet.whitespaces)
                    }else{
                        user = userName.text!.trimmingCharacters(in: CharacterSet.whitespaces)
                    }

                User.instance.generateLoginOtp(phone: user, callback: { (StatusRespone) in
                print(StatusRespone)
                let userMessage = "Check your SMS for OTP"
                
                let alert = UIAlertController(title: "Alert", message: userMessage, preferredStyle: UIAlertControllerStyle.alert)
                // add the actions (buttons)
                alert.addAction(UIAlertAction(title: "Verify", style: UIAlertActionStyle.destructive, handler: { action in
                    DispatchQueue.main.async {
                        User.instance.phone = user
                        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
                        let login: LoginOTPViewController = storyboard.instantiateViewController(withIdentifier: "LoginOTPViewController") as! LoginOTPViewController
                        login.loginWithOTP = true
                        self.navigationController?.pushViewController(login, animated: false)
                        }
                    }))
                    alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel, handler: nil))
                    self.present(alert, animated: true, completion: nil)
            },  failure: { (error) in
                if let error =  error as? APIError{
                    guard  let des = error.error_description else{return}
                    print(des)
                }else if let error =  error as? Error{
                    print(error.localizedDescription)
                }
                })
            }
        }
    }

    @IBAction func login(_ sender: Any) {
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.loginUser(_:)))
        login.addGestureRecognizer(tap)
    }
    
    @IBOutlet weak var forgotPassword: UIButton!
    @IBOutlet weak var register: UIButton!
    @IBOutlet weak var login: UIButton!
    
    @IBAction func forgotPasswordview(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        let forgotPassword: ForgotPasswordViewController = storyboard.instantiateViewController(withIdentifier: "ForgotPasswordViewController") as! ForgotPasswordViewController
        self.navigationController?.pushViewController(forgotPassword, animated: true)
    }
    
    @IBAction func registerview(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        let register: RegisterViewController = storyboard.instantiateViewController(withIdentifier: "RegisterViewController") as! RegisterViewController
        self.navigationController?.pushViewController(register, animated: true)
    }
    
    var btnLeftMenu: UIButton?
    var passwordiconClick = true

    override func viewDidLoad() {
        super.viewDidLoad()
        
        register.setTitleColor(UIColor.darkGray, for: UIControlState())
        login.setTitleColor(UIColor.white, for: UIControlState())
        register.backgroundColor = UIColor.white
        login.backgroundColor = UIColor.black

        loginwithOTP.setTitleColor(UIColor.darkGray, for: UIControlState())
        forgotPassword.setTitleColor(UIColor.darkGray, for: UIControlState())
        register.setTitleColor(UIColor.darkGray, for: UIControlState())
        login.setTitleColor(UIColor.white, for: UIControlState())

        btnLeftMenu = UIButton()
        let origImage = UIImage(named: "ic_eye_closed");
        let tintedImage = origImage?.withRenderingMode(UIImageRenderingMode.alwaysTemplate)
        btnLeftMenu?.setImage(tintedImage, for: UIControlState())
        btnLeftMenu?.tintColor = UIColor.darkGray
        btnLeftMenu?.contentMode = .center
        btnLeftMenu?.isAccessibilityElement = true
        btnLeftMenu!.addTarget(self, action: #selector(self.togglePasswordVisible(_:)), for: UIControlEvents.touchUpInside)
        btnLeftMenu!.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        password.rightView = btnLeftMenu
        password.rightViewMode = .always
        
        self.loginwithOTP.isHidden = true
        
        self.userName.delegate = self
        
        self.password.delegate = self
    }

    @objc func togglePasswordVisible(_ sender:AnyObject){
        if(passwordiconClick == true) {
            password!.isSecureTextEntry = false
            let origImage = UIImage(named: "ic_eye_open");
            let tintedImage = origImage?.withRenderingMode(UIImageRenderingMode.alwaysTemplate)
            btnLeftMenu!.setImage(tintedImage, for: UIControlState())
            btnLeftMenu!.tintColor = UIColor.darkGray
            password?.keyboardType = .asciiCapable
            passwordiconClick = false
        } else {
            password!.isSecureTextEntry = true
            let origImage = UIImage(named: "ic_eye_closed");
            let tintedImage = origImage?.withRenderingMode(UIImageRenderingMode.alwaysTemplate)
            btnLeftMenu!.setImage(tintedImage, for: UIControlState())
            btnLeftMenu!.tintColor = UIColor.darkGray
            password?.keyboardType = .asciiCapable
            passwordiconClick = true
        }
    }
    
    func displayMyAlertMessage(userMessage:String){
        let myAlert = UIAlertController(title: "Alert", message: userMessage, preferredStyle: UIAlertControllerStyle.alert)
        
        let okAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil)
        
        myAlert.addAction(okAction)
        self.present(myAlert, animated: true, completion: nil)
    }
    
    func checkEmptyValidation() -> Bool{
        var message:String = ""
        if userName!.text == "" || (userName!.text?.isEmpty)!{
            message = "Username Required"
            displayMyAlertMessage(userMessage: message)
            return false
        }else if password!.text == "" || (password!.text?.isEmpty)!{
            message = "Password Required"
            displayMyAlertMessage(userMessage: message)
            return false
        }else{
            return true
        }
    }
    
    func checkPasswordCharLimit()-> Bool{
        var message:String = ""
        if let password = password?.text{
            if password.count >= 6{
                return true
            }else{
                message = "Password Should be Greater than 5 Characters"
                displayMyAlertMessage(userMessage: message)
                return false
            }
        }else{
            return false
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    @objc func loginUser(_ sender:UITapGestureRecognizer) {
        if  SharedData.AppSetting.IS_INTERNET_CONNECTED{
        if checkEmptyValidation(){
            if  userName.text!.isNumeric ? isValidPhone(userName.text!) :
                isValidEmail(userName.text!.trimmingCharacters(in: CharacterSet.whitespaces)){
                var user = ""
                if userName.text!.isNumeric{
                    user = self.selectedCountryCode + userName.text!.trimmingCharacters(in: CharacterSet.whitespaces)
                }else{
                    user = userName.text!.trimmingCharacters(in: CharacterSet.whitespaces)
                }
                if checkPasswordCharLimit(){
                    Token.instance.getToken(userName: user, password: password!.text!, callback: { (token) in
                        DispatchQueue.main.async {
                        Token.instance.access_token = token.access_token
                        Token.instance.refresh_token = token.refresh_token
                        Token.instance.token_type = token.token_type
                        Token.instance.expires_in = token.expires_in
                        User.instance.getMe(callback: { (user) in
                            DispatchQueue.main.async {
                                if user.phoneVerified == true {
                                    if user.emailVerified == true {
                                    User.instance.firstname = user.firstname
                                    User.instance.lastname = user.lastname
                                    User.instance.email = user.email
                                    User.instance.phone = user.phone
                                    User.instance.profilePicture = user.profilePicture
                                    User.instance.countryDialCode = user.countryDialCode
                                    User.instance._id = user._id
                        
                                    let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
                                    let appHome: AppHomeViewController = storyboard.instantiateViewController(withIdentifier: "AppHomeViewController") as! AppHomeViewController
                                        let nav: NavigationController = storyboard.instantiateViewController(withIdentifier: "NavigationController") as! NavigationController
                                        nav.viewControllers = [appHome]
                                        self.appDelegate.window?.rootViewController = nav

                                    }else {
                                        User.instance.firstname = user.firstname
                                        User.instance.lastname = user.lastname
                                        User.instance.email = user.email
                                        User.instance.phone = user.phone
                                        User.instance.profilePicture = user.profilePicture
                                        User.instance.countryDialCode = user.countryDialCode
                                        User.instance._id = user._id
                                        
                                        self.displayMyAlertMessage(userMessage: "Email is not verified")
                                        User.instance.resendEmailVerification(id: user._id!, callback: { (sr) in
                                            DispatchQueue.main.async {
                                                let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
                                                let appHome: LoginOTPViewController = storyboard.instantiateViewController(withIdentifier: "LoginOTPViewController") as! LoginOTPViewController
                                                self.navigationController?.pushViewController(appHome, animated: true)
                                            }
                                        }, failure: { (error) in
                                            print("Failed to verify email")
                                        })
                                    }
                                }else {
                                    User.instance.firstname = user.firstname
                                    User.instance.lastname = user.lastname
                                    User.instance.email = user.email
                                    User.instance.phone = user.phone
                                    User.instance.profilePicture = user.profilePicture
                                    User.instance.countryDialCode = user.countryDialCode
                                    User.instance._id = user._id

                                    self.displayMyAlertMessage(userMessage: "Phone number is not verified. Please Check SMS and verify phone number")
                                    User.instance.resendPhoneVerificationCode(id: user._id!, callback: { (StatusResponse) in
                                            DispatchQueue.main.async {
                                        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
                                        let appHome: LoginOTPViewController = storyboard.instantiateViewController(withIdentifier: "LoginOTPViewController") as! LoginOTPViewController
                                        appHome.phverify = true
                                        self.navigationController?.pushViewController(appHome, animated: true)
                                        }

                                    }, failure: { (error) in
                                        if let error =  error as? APIError{
                                            guard  let des = error.error_description else{return}
                                            print(des)
                                        }else if let error =  error as? Error{
                                            print(error.localizedDescription)
                                        }
                                    })
                                    }
                            }
                        },  failure: { (error) in
                            if let error =  error as? APIError{
                                guard  let des = error.error_description else{return}
                                print(des)
                            }else if let error =  error as? Error{
                                print(error.localizedDescription)
                            }
                        })
                        }
                    }, failure: { (error) in
                        if let error =  error as? APIError{
                            guard  let des = error.error_description else{return}
                            print(des)
                        }else if let error =  error as? Error{
                            print(error.localizedDescription)
                        }
                    })
                }
            }
        }
        }else{
            self.displayMyAlertMessage(userMessage: "No Internet Connection")
        }
    }
    
}

